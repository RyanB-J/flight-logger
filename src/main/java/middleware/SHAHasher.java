//*******************************************************************
// SHAHasher
//
// Hashes a string into an SHA-256 hash string.
//*******************************************************************

package main.java.middleware;

import main.java.util.Helper;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHAHasher {

    private static String byteToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (byte b : hash) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static String hash(String input) {

        String hashedString = "";

        try {
            // SHA hash instance
            MessageDigest md = MessageDigest.getInstance("SHA-256");

            // Calculate message digest and store in byte array
            byte[] mdByteArray = md.digest(input.getBytes(StandardCharsets.UTF_8));

            // Convert byte array into hex value
            hashedString = byteToHex(mdByteArray);

        } catch (NoSuchAlgorithmException e) {
            System.out.println(Helper.changeColor("NoSuchAlgorithmException: " + e.getMessage(), Helper.Color.RED));
        }

        return hashedString;

    }

}
