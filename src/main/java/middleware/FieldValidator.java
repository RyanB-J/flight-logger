//*******************************************************************
// FieldValidator
//
// Checks fields for validity after submission is attempted.
//*******************************************************************

package main.java.middleware;

import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import main.java.models.Field;
import main.java.models.PassField;
import main.java.models.User;
import main.java.util.AppAlerts;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class FieldValidator {

    // RegEx Patterns
    private static final Pattern namePattern = Pattern.compile("^[\\w\\-]*$");
    private static final Pattern generalPattern = Pattern.compile("^[\\w\\-_/()+&* ]*$");
    private static final Pattern alphaPattern = Pattern.compile("^[A-Za-z ]*$");
    private static final Pattern characterPattern = Pattern.compile("^[\\w\\-]*$");
    private static final Pattern integerPattern = Pattern.compile("^\\d*$");
    private static final Pattern decimalPattern = Pattern.compile("^[\\d]*?.[\\d]*$");
    private static final Pattern airportPattern = Pattern.compile("^[\\w\\-]{3}[\\w\\-]? - [\\w\\-/()+&* ]+$");
    private static final Pattern latitudePattern = Pattern.compile("^\\d{3}-\\d{2}-\\d{2}.\\d{4}N$");
    private static final Pattern longitudePattern = Pattern.compile("^\\d{3}-\\d{2}-\\d{2}.\\d{4}W$");
    private static final Pattern frequencyPattern = Pattern.compile("^[123]\\d{2}\\.\\d(00|25|50|75)$");
    private static final Pattern headingPattern = Pattern.compile("^(0?0?[0-9]|0?[0-9][0-9]|[0-2][0-9][0-9]|3[0-5][0-9])$");
    private static final Pattern passwordPattern = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?^&+=():;'`~,./\\- ])[A-Za-z\\d@$!%*#?^&+=():;'`~,./\\- ]{8,}$");
    private static final Pattern emailPattern = Pattern.compile("^[\\w\\-_.]+@[\\w-.]+\\.[\\w]+$");
    private static final Pattern timePattern = Pattern.compile("(1[0-2]|0[1-9]):([0-5][0-9]) ([AP]M)");

    // General Field validation Routine to check Field Validity
    public static boolean validate(ArrayList<Field> fields) {

        boolean validated = true;

        // Update objects from the text fields
        for (Field field : fields) {
            field.setValue(field.getTextField().getText());
        }

        StringBuilder errorMessage = new StringBuilder();

        // If text fields are validated
        errorMessage.append(validateText(fields));

        // If the errorMessage is not empty
        if (!errorMessage.toString().isEmpty()) {
            validated = false;
            AppAlerts.throwFieldValidationAlert(errorMessage.toString());
        }

        return validated;
    }

    // Field Validation with option to Bypass
    public static ButtonType validateWithBypass(ArrayList<Field> fields, StringBuilder localErrorMessage) {

        ButtonType result = ButtonType.OK;

        // Update objects from the text fields
        for (Field field : fields) {
            field.setValue(field.getTextField().getText());
        }

        StringBuilder errorMessage = new StringBuilder();
        String errorText = validateText(fields);

        // If text fields are validated
        errorMessage.append(errorText);
        localErrorMessage.append(errorMessage);

        // If the errorMessage is not empty
        if (!errorMessage.toString().isEmpty()) {
            result = AppAlerts.throwBypassableFieldValidationAlert(errorMessage.toString());
        } else {
            localErrorMessage = new StringBuilder();
        }

        return result;
    }

    // Return an error message for all invalid fields
    public static String validateText(ArrayList<Field> fields) {

        StringBuilder errorMessage = new StringBuilder();

        // Loop through the objects and test each type of field
        for (Field object : fields) {

            // If the field is required
            if (object.isRequired()) {
                if (object.getTextField().getLength() < 1) {
                    errorMessage.append("- ").append(object.getName()).append(" is required to have a value.\n");
                }
            }

            switch(object.getType()) {

                // General text type (exclude all symbols except &, -, _, +, (, ), and / -- spaces allowed)
                case "general":
                    if (!generalPattern.matcher(object.getValue()).find()) {
                        errorMessage.append("- ").append(object.getName()).append(" has invalid characters.\n");
                    }
                    break;

                // Alphabetic text type (exclude all numbers and symbols -- spaces allowed)
                case "alphabetic":
                    if (!alphaPattern.matcher(object.getValue()).find()) {
                        errorMessage.append("- ").append(object.getName()).append(" has invalid characters.\n");
                    }

                // Character type (exclude all symbols except for - -- no spaces)
                case "character":
                    if (!characterPattern.matcher(object.getValue()).find()) {
                        errorMessage.append("- ").append(object.getName()).append(" has invalid characters.\n");
                    }
                    break;

                // Integer type (only whole numbers)
                case "integer":
                    if (!integerPattern.matcher(object.getValue()).find()) {
                        errorMessage.append("- ").append(object.getName()).append(" may only contain whole numbers.\n");
                    }
                    break;

                // Decimal type (only numbers and decimals points -- including decimals)
                case "decimal":
                    if (!decimalPattern.matcher(object.getValue()).find()) {
                        errorMessage.append("- ").append(object.getName()).append(" may only contain numbers.\n");
                    }
                    break;

                // Name type (exclude all symbols except hyphens and underscores -- no spaces)
                case "name":
                    if (!namePattern.matcher(object.getValue()).find()) {
                        errorMessage.append("- ").append(object.getName()).append(" has invalid characters.\n");
                    }
                    break;

                // Name type (exclude all symbols except hyphens and underscores -- no spaces)
                case "airport":
                    if (!airportPattern.matcher(object.getValue()).find()) {
                        errorMessage.append("- ").append(object.getName()).append(" needs to be in the format: \"CODE - Airport Name\"\n");
                    }
                    break;

                // Latitude Format
                case "latitude":
                    if (!latitudePattern.matcher(object.getValue()).find()) {
                        errorMessage.append("- ").append(object.getName()).append(" is in an invalid format.\n");
                    }
                    break;

                // Longitude Format
                case "longitude":
                    if (!longitudePattern.matcher(object.getValue()).find()) {
                        errorMessage.append("\n- ").append(object.getName()).append(" is in an invalid format.\n");
                    }
                    break;

                // Frequency Format
                case "frequency":
                    if (!frequencyPattern.matcher(object.getValue()).find()) {
                        errorMessage.append("- ").append(object.getName()).append(" is in an invalid format.\n");
                    }
                    break;

                // Heading Format
                case "heading":
                    object.setValue(("000" + object.getValue()).substring(object.getValue().length()));
                    if (!headingPattern.matcher(object.getValue()).find()) {
                        errorMessage.append("- ").append(object.getName()).append(" is invalid. Heading should be between 0 and 359.\n");
                    }
                    break;

                // Email type (exclude all symbols except hyphens, underscores, and periods)
                case "email":
                    if (!emailPattern.matcher(object.getValue()).find()) {
                        errorMessage.append("- ").append(object.getName()).append(" is in an invalid format.\n");
                    }
                    break;

                // Time type (##:## AM/PM)
                case "time":
                    if (!timePattern.matcher(object.getValue()).find()) {
                        errorMessage.append("- ").append(object.getName()).append(" is in an invalid format.\n");
                    }
                    break;
            }
        }

        return errorMessage.toString();
    }

    // Password Validator to ensure Password Strength
    public static String validatePasswords(ArrayList<PassField> passFields) {

        StringBuilder errorMessage = new StringBuilder();

        // Loop through the objects and test each type of field
        for (PassField object : passFields) {

            // Password type (minimum eight characters, at least one letter, one number and one special character)
            if (!passwordPattern.matcher(object.getValue()).find()) {
                errorMessage.append("- ").append(object.getName()).append(" must contain eight characters, at least " +
                        "one letter, one number, and one special character.\n");
            }
        }

        return errorMessage.toString();
    }

    // ComboBox Requirement Validation
    public static String requireComboBoxes(ArrayList<ComboBox<String>> comboBoxes) {

        boolean hasValue = true;
        StringBuilder errorMessage = new StringBuilder();

        for (ComboBox<String> comboBox : comboBoxes) {
            if (comboBox.getValue() == null || comboBox.getValue().equals("")) {
                hasValue = false;
            }
        }

        if (!hasValue) {
            errorMessage.append("- ").append("One or more required(*) selections have not been made.\n");
        }

        return errorMessage.toString();
    }

    // Date Requirement Validation
    public static String requireDates(ArrayList<DatePicker> datePickers) {

        boolean hasValue = true;
        StringBuilder errorMessage = new StringBuilder();

        for (DatePicker datePicker : datePickers) {
            if (datePicker.getValue() == null) {
                hasValue = false;
            }
        }

        if (!hasValue) {
            errorMessage.append("- ").append("One or more required(*) dates have not been selected.\n");
        }

        return errorMessage.toString();
    }

    // Password Match Validator
    public static String passwordMatch(String password1, String password2) {

        StringBuilder errorMessage = new StringBuilder();

        if (!password1.equals(password2)) {
            errorMessage.append("- ").append("The two passwords entered do not match.\n");
        }

        return errorMessage.toString();
    }

    // Authenticate User
    public static String authenticateUser(String username, String password) {

        StringBuilder errorMessage = new StringBuilder();

        if (!User.authenticate(username, password)) {
            errorMessage.append("- ").append("The password is incorrect.\n");
        }

        return errorMessage.toString();
    }

    // Check User Existence
    public static String uniqueUsername(String username) {

        StringBuilder errorMessage = new StringBuilder();

        if (User.exists(username)) {
            errorMessage.append("- ").append("That username already exists.\n");
        }
        return errorMessage.toString();
    }
}
