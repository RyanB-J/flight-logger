//*******************************************************************
// FieldFormatter
//
// Adds Listeners to fields to restrict invalid input.
//*******************************************************************

package main.java.middleware;

import main.java.models.Field;
import main.java.models.PassField;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class FieldFormatter {

    // RegEx Patterns
    private static final Pattern characterPattern = Pattern.compile("^[A-Za-z0-9\\-]*$");
    private static final Pattern alphaPattern = Pattern.compile("^[A-Za-z ]*$");
    private static final Pattern integerPattern = Pattern.compile("^\\d*$");
    private static final Pattern decimalPattern = Pattern.compile("^[\\d]*?.[\\d]*$");

    // Add text-field listeners to restrict input length
    public static void restrictFields(ArrayList<Field> fields) {

        // Loop through the fields and add a listener for each field
        for (Field object:fields) {
            object.getTextField().textProperty().addListener((ov, oldValue, newValue) -> {
                String objectText = object.getTextField().getText();

                if (!objectText.isEmpty()) {
                    if (objectText.length() > object.getLength()) {
                        object.getTextField().setText(objectText.substring(0, object.getLength()));
                    }
                    switch (object.getType()) {
                        case "character":
                            if (!characterPattern.matcher(objectText).find()) {
                                object.getTextField().setText(oldValue);
                            }
                            break;
                        case "alpha":
                            if (!alphaPattern.matcher(objectText).find()) {
                                object.getTextField().setText(oldValue);
                            }
                            break;
                        case "heading":
                        case "integer":
                            if (!integerPattern.matcher(objectText).find()) {
                                object.getTextField().setText(oldValue);
                            }
                            break;
                        case "decimal":
                            if (!decimalPattern.matcher(objectText).find()) {
                                object.getTextField().setText(oldValue);
                            }
                            break;
                    }
                }
            });
        }
    }

    // Add password-field listeners to restrict input length
    public static void restrictPasswords(ArrayList<PassField> fields) {

        // Loop through the fields and add a listener for each field
        for (PassField object:fields) {
            object.getPassField().textProperty().addListener((ov, oldValue, newValue) -> {
                if (object.getPassField().getText().length() > object.getLength()) {
                    object.getPassField().setText(object.getPassField().getText().substring(0, object.getLength()));
                }
            });
        }
    }
}
