//*******************************************************************
// Aircraft
//
// Constructs an Aircraft Object and controls Aircraft related operations from the database.
//*******************************************************************

package main.java.models;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.java.util.Database;
import main.java.util.Helper;

import java.sql.*;

public class Aircraft {

    private final SimpleIntegerProperty
            id = new SimpleIntegerProperty(),
            engineNumber = new SimpleIntegerProperty();

    private final SimpleDoubleProperty
            wingspan = new SimpleDoubleProperty(),
            length = new SimpleDoubleProperty(),
            tailHeight = new SimpleDoubleProperty(),
            wheelbase = new SimpleDoubleProperty();

    private final SimpleStringProperty
            make = new SimpleStringProperty(),
            model = new SimpleStringProperty(),
            engineClass = new SimpleStringProperty(),
            wakeCategory = new SimpleStringProperty(),
            icaoCode = new SimpleStringProperty();

    private final SimpleBooleanProperty custom = new SimpleBooleanProperty();

    public Aircraft() { }

    public Aircraft(int id, String make, String model, String icaoCode, int engineNumber, String engineClass,
                    double wingspan, double length, double tailHeight, double wheelbase, String wakeCategory,
                    boolean custom) {
        setId(id);
        setMake(make);
        setModel(model);
        setIcaoCode(icaoCode);
        setEngineNumber(engineNumber);
        setEngineClass(engineClass);
        setWingspan(wingspan);
        setLength(length);
        setTailHeight(tailHeight);
        setWheelbase(wheelbase);
        setWakeCategory(wakeCategory);
        setCustom(custom);
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public int getEngineNumber() {
        return engineNumber.get();
    }

    public void setEngineNumber(int engineNumber) {
        this.engineNumber.set(engineNumber);
    }

    public double getWingspan() {
        return wingspan.get();
    }

    public void setWingspan(double wingspan) {
        this.wingspan.set(wingspan);
    }

    public double getLength() {
        return length.get();
    }

    public void setLength(double length) {
        this.length.set(length);
    }

    public double getTailHeight() {
        return tailHeight.get();
    }

    public void setTailHeight(double tailHeight) {
        this.tailHeight.set(tailHeight);
    }

    public double getWheelbase() {
        return wheelbase.get();
    }

    public void setWheelbase(double wheelbase) {
        this.wheelbase.set(wheelbase);
    }

    public String getMake() {
        return make.get();
    }

    public void setMake(String manufacturer) {
        this.make.set(manufacturer);
    }

    public String getModel() {
        return model.get();
    }

    public void setModel(String model) {
        this.model.set(model);
    }

    public String getEngineClass() {
        return engineClass.get();
    }

    public void setEngineClass(String engineClass) {
        this.engineClass.set(engineClass);
    }

    public String getWakeCategory() {
        return wakeCategory.get();
    }

    public void setWakeCategory(String wakeCategory) {
        this.wakeCategory.set(wakeCategory);
    }

    public String getIcaoCode() {
        return icaoCode.get();
    }

    public void setIcaoCode(String icaoCode) {
        this.icaoCode.set(icaoCode);
    }

    public boolean isCustom() {
        return custom.get();
    }

    public void setCustom(boolean custom) {
        this.custom.set(custom);
    }

    // Engine Classes
    public static final ObservableList<String> engineClasses = FXCollections.observableArrayList(
            "", "Piston", "Turboprop", "Jet", "Helicopter"
    );

    // Wake Categories
    public static final ObservableList<String> wakeCategories = FXCollections.observableArrayList(
            "", "L", "M", "H", "S"
    );

    // Helper method to populate an aircraft list from a result set
    private static void buildAircraftListFromResultSet(ObservableList<Aircraft> aircraftList, PreparedStatement ps) throws SQLException {
        try (ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                aircraftList.add(new Aircraft(
                        rs.getInt("id"),
                        rs.getString("make"),
                        rs.getString("model"),
                        rs.getString("icao"),
                        rs.getInt("engine_num"),
                        rs.getString("engine_class"),
                        rs.getDouble("wingspan"),
                        rs.getDouble("length"),
                        rs.getDouble("tail_height"),
                        rs.getDouble("wheel_base"),
                        rs.getString("wake"),
                        rs.getInt("custom") == 1
                ));
            }
        }
    }

    // Create a Custom Aircraft
    public static boolean createAircraft(String make, String model, String icaoCode, String engineNumber,
                                           String engineClass, String wingspan, String length, String tailHeight,
                                           String wheelBase, String wakeCategory) {

        System.out.println(Helper.changeColor("Creating Aircraft...", Helper.Color.YELLOW));

        // Add the aircraft to the database
        String query = "INSERT INTO aircraft " +
                "(make, model, icao, engine_num, engine_class, wingspan, length, tail_height, wheel_base, wake, custom) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1)";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query,
                Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, make.toUpperCase());
            ps.setString(2, model.toUpperCase());
            if (!icaoCode.isEmpty()) ps.setString(3, icaoCode.toUpperCase()); else ps.setNull(3, Types.VARCHAR);
            if (!engineNumber.isEmpty()) ps.setInt(4, Integer.parseInt(engineNumber)); else ps.setNull(4, Types.INTEGER);
            if (!engineClass.isEmpty()) ps.setString(5, engineClass.toUpperCase()); else ps.setNull(5, Types.VARCHAR);
            if (!wingspan.isEmpty()) ps.setDouble(6, Double.parseDouble(wingspan)); else ps.setNull(6, Types.DOUBLE);
            if (!length.isEmpty()) ps.setDouble(7, Double.parseDouble(length)); else ps.setNull(7, Types.DOUBLE);
            if (!tailHeight.isEmpty()) ps.setDouble(8, Double.parseDouble(tailHeight)); else ps.setNull(8, Types.DOUBLE);
            if (!wheelBase.isEmpty()) ps.setDouble(9, Double.parseDouble(wheelBase)); else ps.setNull(9, Types.DOUBLE);
            if (!wakeCategory.isEmpty()) ps.setString(10, wakeCategory); else ps.setNull(10, Types.CHAR);

            int rowsAffected = ps.executeUpdate();
            System.out.println("\tCreated Aircraft: " + rowsAffected + " row affected.");
            if (rowsAffected == 0) {
                System.out.println("\tAircraft Creation Failed");
                return false;
            }

            // Get the key of the insert
            try (ResultSet generatedKey = ps.getGeneratedKeys()) {
                if (generatedKey.next()) {
                    System.out.println("\tAircraft ID: " + generatedKey.getInt(1));
                }
            }

            System.out.println(Helper.changeColor("Aircraft Created", Helper.Color.GREEN) + "\n");

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return true;
    }

    // Get Aircraft
    public static ObservableList<Aircraft> getAircraft(String searchString, int numberOfResults, boolean customOnly) {
        ObservableList<Aircraft> aircraftList = FXCollections.observableArrayList();
        boolean searchQuery = (searchString != null && !searchString.isEmpty());

        String query;
        if (searchQuery) {
            if (customOnly) {
                query = "SELECT * FROM aircraft WHERE (make LIKE ? OR model LIKE ? OR icao LIKE ?) AND custom = 1 LIMIT ?";
            } else {
                query = "SELECT * FROM aircraft WHERE make LIKE ? OR model LIKE ? OR icao LIKE ? LIMIT ?";
            }
        } else {
            if (customOnly) {
                query = "SELECT * FROM aircraft WHERE custom = 1";
            } else {
                query = "SELECT * FROM aircraft LIMIT 100";
            }
        }
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            if (searchQuery) {
                ps.setString(1, "%" + searchString + "%");
                ps.setString(2, "%" + searchString + "%");
                ps.setString(3, "%" + searchString + "%");
                ps.setInt(4, numberOfResults);
            }

            buildAircraftListFromResultSet((ObservableList<Aircraft>) aircraftList, ps);

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return aircraftList;
    }

    // Get a single Aircraft
    public static Aircraft getSingleAircraft(int aircraftID) {
        Aircraft aircraft = new Aircraft();

        String query = "SELECT * FROM aircraft WHERE id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setInt(1, aircraftID);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    aircraft.setId(rs.getInt("id"));
                    aircraft.setMake(rs.getString("make"));
                    aircraft.setModel(rs.getString("model"));
                    aircraft.setIcaoCode(rs.getString("icao"));
                    aircraft.setEngineNumber(rs.getInt("engine_num"));
                    aircraft.setEngineClass(rs.getString("engine_class"));
                    aircraft.setWingspan(rs.getDouble("wingspan"));
                    aircraft.setLength(rs.getDouble("length"));
                    aircraft.setTailHeight(rs.getDouble("tail_height"));
                    aircraft.setWheelbase(rs.getDouble("wheel_base"));
                    aircraft.setWakeCategory(rs.getString("wake"));
                    aircraft.setCustom(rs.getInt("custom") == 1);
                }
            }
        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return aircraft;
    }

    // Get makes
    public static ObservableList<String> getMakes(String searchString, int numberOfResults) {
        ObservableList<String> aircraftList = FXCollections.observableArrayList();

        String query = "SELECT DISTINCT make FROM aircraft WHERE make LIKE ? LIMIT ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setString(1, "%" + searchString + "%");
            ps.setInt(2, numberOfResults);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    aircraftList.add(rs.getString("make"));
                }
            }

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return aircraftList;
    }

    // Get all main.java.models from a single make
    public static ObservableList<Aircraft> getModels(String make) {
        ObservableList<Aircraft> aircraftList = FXCollections.observableArrayList();

        String query = "SELECT * FROM aircraft WHERE make = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setString(1, make);

            buildAircraftListFromResultSet((ObservableList<Aircraft>) aircraftList, ps);

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return aircraftList;
    }

    // Update a Custom Aircraft
    public static boolean updateAircraft(int id, String make, String model, String icaoCode, String engineNumber,
                                         String engineClass, String wingspan, String length, String tailHeight,
                                         String wheelBase, String wakeCategory) {

        System.out.println(Helper.changeColor("Updating Aircraft...", Helper.Color.YELLOW));

        // Update the aircraft in the database
        String query = "UPDATE aircraft " +
                "SET make = ?, model = ?, icao = ?, engine_num = ?, engine_class = ?, wingspan = ?, length = ?, " +
                "tail_height = ?, wheel_base = ?, wake = ? " +
                "WHERE id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setString(1, make.toUpperCase());
            ps.setString(2, model.toUpperCase());
            if (!icaoCode.isEmpty()) ps.setString(3, icaoCode.toUpperCase()); else ps.setNull(3, Types.VARCHAR);
            if (!engineNumber.isEmpty()) ps.setInt(4, Integer.parseInt(engineNumber)); else ps.setNull(4, Types.INTEGER);
            if (!engineClass.isEmpty()) ps.setString(5, engineClass.toUpperCase()); else ps.setNull(5, Types.VARCHAR);
            if (!wingspan.isEmpty()) ps.setDouble(6, Double.parseDouble(wingspan)); else ps.setNull(6, Types.DOUBLE);
            if (!length.isEmpty()) ps.setDouble(7, Double.parseDouble(length)); else ps.setNull(7, Types.DOUBLE);
            if (!tailHeight.isEmpty()) ps.setDouble(8, Double.parseDouble(tailHeight)); else ps.setNull(8, Types.DOUBLE);
            if (!wheelBase.isEmpty()) ps.setDouble(9, Double.parseDouble(wheelBase)); else ps.setNull(9, Types.DOUBLE);
            if (!wakeCategory.isEmpty()) ps.setString(10, wakeCategory); else ps.setNull(10, Types.CHAR);
            ps.setInt(11, id);

            int rowsAffected = ps.executeUpdate();
            System.out.println("\tUpdated Aircraft: " + rowsAffected + " row affected.");
            if (rowsAffected == 0) {
                System.out.println("\tAircraft Update Failed");
                return false;
            }

            System.out.println("\tAircraft ID: " + id);

            System.out.println(Helper.changeColor("Aircraft Updated", Helper.Color.GREEN) + "\n");

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return true;
    }

    // Delete an Aircraft
    public static void deleteAircraft(int id) {

        System.out.println(Helper.changeColor("Deleting Aircraft...", Helper.Color.YELLOW));

        // Delete the aircraft from the database
        String query = "DELETE FROM aircraft WHERE id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setInt(1, id);

            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 0) {
                System.out.println("\tAircraft Deletion Failed");
            } else {
                System.out.println("\tAircraft Deleted with ID: " + id);
                System.out.println(Helper.changeColor("Aircraft Deleted\n", Helper.Color.GREEN));
            }

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }
    }
}
