//*******************************************************************
// Airport
//
// Constructs an Airport Object and controls Airport related operations from the database.
//*******************************************************************

package main.java.models;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.java.util.Database;
import main.java.util.Helper;

import java.sql.*;

public class Airport {

    private SimpleIntegerProperty id = new SimpleIntegerProperty();

    private final SimpleDoubleProperty
            elevation = new SimpleDoubleProperty();

    private final SimpleStringProperty
            relationalId = new SimpleStringProperty(),
            name = new SimpleStringProperty(),
            type = new SimpleStringProperty(),
            locId = new SimpleStringProperty(),
            state = new SimpleStringProperty(),
            city = new SimpleStringProperty(),
            latitude = new SimpleStringProperty(),
            longitude = new SimpleStringProperty(),
            ctaf = new SimpleStringProperty(),
            unicom = new SimpleStringProperty();

    private final SimpleBooleanProperty
            tower = new SimpleBooleanProperty(),
            custom = new SimpleBooleanProperty();

    public Airport() { }

    public Airport(int id, String relationalId, String name, String locId, String type, String city, String state,
                   String latitude, String longitude, double elevation, String ctaf, String unicom, boolean tower,
                   boolean custom) {
        setId(id);
        setRelationalId(relationalId);
        setName(name);
        setLocId(locId);
        setType(type);
        setCity(city);
        setState(state);
        setLatitude(latitude);
        setLongitude(longitude);
        setElevation(elevation);
        setCtaf(ctaf);
        setUnicom(unicom);
        setTower(tower);
        setCustom(custom);
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public double getElevation() {
        return elevation.get();
    }

    public void setElevation(double elevation) {
        this.elevation.set(elevation);
    }

    public String getCtaf() {
        return ctaf.get();
    }

    public void setCtaf(String ctaf) {
        this.ctaf.set(ctaf);
    }

    public String getUnicom() {
        return unicom.get();
    }

    public void setUnicom(String unicom) {
        this.unicom.set(unicom);
    }

    public String getRelationalId() {
        return relationalId.get();
    }

    public void setRelationalId(String relationalId) {
        this.relationalId.set(relationalId);
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getType() {
        return type.get();
    }

    public void setType(String type) {
        this.type.set(type);
    }

    public String getLocId() {
        return locId.get();
    }

    public void setLocId(String locId) {
        this.locId.set(locId);
    }

    public String getState() {
        return state.get();
    }

    public void setState(String state) {
        this.state.set(state);
    }

    public String getCity() {
        return city.get();
    }

    public void setCity(String city) {
        this.city.set(city);
    }

    public String getLatitude() {
        return latitude.get();
    }

    public void setLatitude(String latitude) {
        this.latitude.set(latitude);
    }

    public String getLongitude() {
        return longitude.get();
    }

    public void setLongitude(String longitude) {
        this.longitude.set(longitude);
    }

    public boolean isTower() {
        return tower.get();
    }

    public void setTower(boolean tower) {
        this.tower.set(tower);
    }

    public boolean isCustom() {
        return custom.get();
    }

    public void setCustom(boolean custom) {
        this.custom.set(custom);
    }

    // Airport Type
    public static final ObservableList<String> airportTypes = FXCollections.observableArrayList(
            "", "Airport", "Heliport", "Seaplane Base", "Ultralight"
    );

    // States
    public static final ObservableList<String> states = FXCollections.observableArrayList(
            "Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware",
            "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana",
            "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana",
            "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina",
            "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina",
            "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia",
            "Wisconsin", "Wyoming"
    );

    // UNICOM Frequencies
    public static final ObservableList<String> unicomFrequencies = FXCollections.observableArrayList(
            "", "122.700", "122.725", "122.800", "122.950", "122.975", "123.000", "123.050", "123.075"
    );

    // Query to keep count or relationalIDs
    public static int getLastRelationalId() {
        int lastId = 0;
        String query = "SELECT relational_id FROM airports WHERE relational_id LIKE 'CUST*_%' " +
                "ORDER BY id DESC LIMIT 1";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    lastId = Integer.parseInt(rs.getString(1).substring(5));
                }
            }
        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }
        return lastId;
    }

    // Create a Custom Airport
    public static boolean createAirport(String relationalID, String name, String locId, String type, String city,
                                        String state, String latitude, String longitude, String elevation, String ctaf,
                                        String unicom, boolean tower) {

        System.out.println(Helper.changeColor("Creating Airport...", Helper.Color.YELLOW));

        // Add the airport to the database
        String query = "INSERT INTO airports " +
                "(relational_id, name, loc_id, type, city, state, latitude, longitude, elevation, ctaf, unicom, tower, custom) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1)";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query,
                Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, relationalID);
            ps.setString(2, name.toUpperCase());
            ps.setString(3, locId.toUpperCase());
            ps.setString(4, type.toUpperCase());
            ps.setString(5, city.toUpperCase());
            ps.setString(6, state.toUpperCase());
            ps.setString(7, latitude.toUpperCase());
            ps.setString(8, longitude.toUpperCase());
            if (!elevation.isEmpty()) ps.setDouble(9, Double.parseDouble(elevation)); else ps.setNull(9, Types.DOUBLE);
            if (!ctaf.isEmpty()) ps.setString(10, ctaf); else ps.setNull(10, Types.CHAR);
            if (!unicom.isEmpty()) ps.setString(11, unicom); else ps.setNull(11, Types.CHAR);
            ps.setInt(12, (tower ? 1 : 0));

            int rowsAffected = ps.executeUpdate();
            System.out.println("\tCreated Airport: " + rowsAffected + " row affected");
            if (rowsAffected == 0) {
                System.out.println("\tAircraft Creation Failed");
                return false;
            }

            // Get the key of the insert
            try (ResultSet generatedKey = ps.getGeneratedKeys()) {
                if (generatedKey.next()) {
                    System.out.println("\tAirport ID: " + generatedKey.getInt(1));
                    System.out.println("\tRelational ID: " + relationalID);
                }
            }

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return true;
    }

    // Get Airports
    public static ObservableList<Airport> getAirports(String searchString, int numberOfResults, boolean customOnly) {
        ObservableList<Airport> airportList = FXCollections.observableArrayList();
        boolean searchQuery = (searchString != null && !searchString.isEmpty());

        String query;
        if (searchQuery) {
            if (customOnly) {
                query = "SELECT * FROM airports WHERE (loc_id LIKE ? OR name LIKE ? OR city LIKE ?) AND custom = 1 LIMIT ?";
            } else {
                query = "SELECT * FROM airports WHERE loc_id LIKE ? OR name LIKE ? OR city LIKE ? LIMIT ?";
            }
        } else {
            if (customOnly) {
                query = "SELECT * FROM airports WHERE custom = 1";
            } else {
                query = "SELECT * FROM airports LIMIT 100";
            }
        }
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            if (searchQuery) {
                ps.setString(1, "%" + searchString + "%");
                ps.setString(2, "%" + searchString + "%");
                ps.setString(3, "%" + searchString + "%");
                ps.setInt(4, numberOfResults);
            }

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    airportList.add(new Airport(
                            rs.getInt("id"),
                            rs.getString("relational_id"),
                            rs.getString("name"),
                            rs.getString("loc_id"),
                            rs.getString("type"),
                            rs.getString("city"),
                            rs.getString("state"),
                            rs.getString("latitude"),
                            rs.getString("longitude"),
                            rs.getDouble("elevation"),
                            rs.getString("ctaf"),
                            rs.getString("unicom"),
                            rs.getInt("tower") == 1,
                            rs.getInt("custom") == 1
                    ));
                }
            }

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return airportList;
    }

    // Get a single Airport
    public static Airport getSingleAirport(int airportID) {
        Airport airport = new Airport();

        String query = "SELECT * FROM airports WHERE id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setInt(1, airportID);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    airport.setId(rs.getInt("id"));
                    airport.setRelationalId(rs.getString("relational_id"));
                    airport.setName(rs.getString("name"));
                    airport.setLocId(rs.getString("loc_id"));
                    airport.setType(rs.getString("type"));
                    airport.setCity(rs.getString("city"));
                    airport.setState(rs.getString("state"));
                    airport.setLatitude(rs.getString("latitude"));
                    airport.setLongitude(rs.getString("longitude"));
                    airport.setElevation(rs.getDouble("elevation"));
                    airport.setCtaf(rs.getString("ctaf"));
                    airport.setUnicom(rs.getString("unicom"));
                    airport.setTower(rs.getInt("tower") == 1);
                    airport.setCustom(rs.getInt("custom") == 1);
                }
            }
        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return airport;
    }

    // Update a Custom Airport
    public static boolean updateAirport(int id, String relationalID, String name, String locId, String type,
                                        String city, String state, String latitude, String longitude,
                                        String elevation, String ctaf, String unicom, boolean tower) {

        System.out.println(Helper.changeColor("Updating Airport...", Helper.Color.YELLOW));

        // Update the airport in the database
        String query = "UPDATE airports " +
                "SET name = ?, loc_id = ?, type = ?, city = ?, state = ?, latitude = ?, longitude = ?, elevation = ?, " +
                "ctaf = ?, unicom = ?, tower = ? " +
                "WHERE id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setString(1, name.toUpperCase());
            ps.setString(2, locId.toUpperCase());
            ps.setString(3, type.toUpperCase());
            ps.setString(4, city.toUpperCase());
            ps.setString(5, state.toUpperCase());
            ps.setString(6, latitude.toUpperCase());
            ps.setString(7, longitude.toUpperCase());
            if (!elevation.isEmpty()) ps.setDouble(8, Double.parseDouble(elevation)); else ps.setNull(8, Types.DOUBLE);
            if (!ctaf.isEmpty()) ps.setString(9, ctaf); else ps.setNull(9, Types.CHAR);
            if (!unicom.isEmpty()) ps.setString(10, unicom); else ps.setNull(10, Types.CHAR);
            ps.setInt(11, (tower ? 1 : 0));
            ps.setInt(12, id);

            int rowsAffected = ps.executeUpdate();
            System.out.println("\tUpdated Airport: " + rowsAffected + " row affected.");
            if (rowsAffected == 0) {
                System.out.println("\tAirport Update Failed.");
                return false;
            }

            System.out.println("\tAirport ID: " + id);
            System.out.println("\tRelational ID: " + relationalID);

            System.out.println(Helper.changeColor("Airport Updated", Helper.Color.GREEN) + "\n");

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return true;
    }

    // Delete an Airport
    public static void deleteAirport(int id, String relationalID) {

        System.out.println(Helper.changeColor("Deleting Airport...", Helper.Color.YELLOW));

        // Delete the airport from the database
        String airportQuery = "DELETE FROM airports WHERE id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(airportQuery)) {
            ps.setInt(1, id);

            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 0) {
                System.out.println("\tAirport Deletion Failed");
            } else {
                System.out.println("\tDeleted Airport with ID: " + id);
            }

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        // Delete the runways from the database
        String runwayQuery = "DELETE FROM runways WHERE relational_id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(runwayQuery)) {
            ps.setString(1, relationalID);

            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 0) {
                System.out.println("\tRunway Deletion Failed");
            } else {
                System.out.println("\tDeleted Runway with ID: " + id);
            }

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }
    }
}
