//*******************************************************************
// Field
//
// Constructs a Field Object for validation.
//*******************************************************************

package main.java.models;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TextField;

public class Field {

    private final SimpleStringProperty
            name = new SimpleStringProperty(),
            value = new SimpleStringProperty(),
            type = new SimpleStringProperty();

    private final SimpleIntegerProperty
            length = new SimpleIntegerProperty();

    private final SimpleBooleanProperty
            required = new SimpleBooleanProperty();

    private TextField textField;

    public Field(String name, TextField textField, String value, String type, int length, boolean required) {
        setName(name);
        setTextField(textField);
        setValue(value);
        setType(type);
        setLength(length);
        setRequired(required);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public TextField getTextField() {
        return textField;
    }

    public void setTextField(TextField textField) {
        this.textField = textField;
    }

    public String getValue() {
        return value.get();
    }

    public SimpleStringProperty valueProperty() {
        return value;
    }

    public void setValue(String value) {
        this.value.set(value);
    }

    public String getType() {
        return type.get();
    }

    public SimpleStringProperty typeProperty() {
        return type;
    }

    public void setType(String type) {
        this.type.set(type);
    }

    public int getLength() {
        return length.get();
    }

    public SimpleIntegerProperty lengthProperty() {
        return length;
    }

    public void setLength(int length) {
        this.length.set(length);
    }

    public boolean isRequired() {
        return required.get();
    }

    public void setRequired(boolean required) {
        this.required.set(required);
    }
}
