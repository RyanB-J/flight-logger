//*******************************************************************
// FlightLog
//
// Constructs a FlightLog Object and controls FlightLog related operations from the database.
//*******************************************************************

package main.java.models;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.java.util.Database;
import main.java.util.Helper;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;

public class FlightLog {

    private final SimpleIntegerProperty
        id = new SimpleIntegerProperty(),
        flightNumber = new SimpleIntegerProperty(),
        aircraftEngineNumber = new SimpleIntegerProperty(),
        dayTakeOffs = new SimpleIntegerProperty(),
        dayLandings = new SimpleIntegerProperty(),
        nightTakeOffs = new SimpleIntegerProperty(),
        nightLandings = new SimpleIntegerProperty(),
        instrumentLandings = new SimpleIntegerProperty(),
        dayHours = new SimpleIntegerProperty(),
        dayMinutes = new SimpleIntegerProperty(),
        nightHours = new SimpleIntegerProperty(),
        nightMinutes = new SimpleIntegerProperty(),
        groundHours = new SimpleIntegerProperty(),
        groundMinutes = new SimpleIntegerProperty(),
        crossCountryHours = new SimpleIntegerProperty(),
        crossCountryMinutes = new SimpleIntegerProperty(),
        instructionHours = new SimpleIntegerProperty(),
        instructionMinutes = new SimpleIntegerProperty(),
        ifrHours = new SimpleIntegerProperty(),
        ifrMinutes = new SimpleIntegerProperty(),
        hoodHours = new SimpleIntegerProperty(),
        hoodMinutes = new SimpleIntegerProperty(),
        simulatorHours = new SimpleIntegerProperty(),
        simulatorMinutes = new SimpleIntegerProperty(),
        totalHours = new SimpleIntegerProperty(),
        totalMinutes = new SimpleIntegerProperty(),
        windHeading = new SimpleIntegerProperty(),
        windSpeed = new SimpleIntegerProperty();

    private final SimpleStringProperty
        aircraftIdent = new SimpleStringProperty(),
        departureAirport = new SimpleStringProperty(),
        departureAirportCode = new SimpleStringProperty(),
        departureRunway = new SimpleStringProperty(),
        arrivalAirport = new SimpleStringProperty(),
        arrivalAirportCode = new SimpleStringProperty(),
        arrivalRunway = new SimpleStringProperty(),
        aircraftMake = new SimpleStringProperty(),
        aircraftModel = new SimpleStringProperty(),
        aircraftEngineClass = new SimpleStringProperty(),
        aircraftICAOCode = new SimpleStringProperty(),
        instrumentApproaches = new SimpleStringProperty(),
        pilotType = new SimpleStringProperty(),
        pilotStatus = new SimpleStringProperty(),
        weather = new SimpleStringProperty();

    private final SimpleBooleanProperty
        sunny = new SimpleBooleanProperty(),
        cloudy = new SimpleBooleanProperty(),
        rainy = new SimpleBooleanProperty(),
        stormy = new SimpleBooleanProperty(),
        hurricane = new SimpleBooleanProperty(),
        lightning = new SimpleBooleanProperty(),
        typhoon = new SimpleBooleanProperty(),
        blizzard = new SimpleBooleanProperty(),
        foggy = new SimpleBooleanProperty(),
        hot = new SimpleBooleanProperty(),
        cold = new SimpleBooleanProperty(),
        windy = new SimpleBooleanProperty();

    private final SimpleObjectProperty<LocalDate>
        date = new SimpleObjectProperty<>();

    private final SimpleObjectProperty<LocalTime>
        departureTime = new SimpleObjectProperty<>(),
        arrivalTime = new SimpleObjectProperty<>();

    private static FlightLog currentFlightLog;

    public FlightLog() { }

    public FlightLog( int id, LocalDate date, int flightNumber, String aircraftIdent,
                      String departureAirport, String departureRunway, LocalTime departureTime,
                      String arrivalAirport, String arrivalRunway, LocalTime arrivalTime,
                      String aircraftMake, String aircraftModel, int aircraftEngineNumber, String aircraftEngineClass,
                      String aircraftICAOCode, int dayTakeOffs, int dayLandings, int nightTakeOffs, int nightLandings,
                      int instrumentLandings, String instrumentApproaches, String pilotType, String pilotStatus,
                      int dayHours, int dayMinutes, int nightHours, int nightMinutes, int groundHours,
                      int groundMinutes, int crossCountryHours, int crossCountryMinutes, int instructionHours,
                      int instructionMinutes, int ifrHours, int ifrMinutes, int hoodHours, int hoodMinutes,
                      int simulatorHours, int simulatorMinutes, int windHeading, int windSpeed, boolean isSunny,
                      boolean isCloudy, boolean isRainy, boolean isStormy, boolean isHurricane, boolean isLightning,
                      boolean isTyphoon, boolean isBlizzard, boolean isFoggy, boolean isHot, boolean isCold,
                      boolean isWindy, String weather) {
        setId(id);
        setDate(date);
        setFlightNumber(flightNumber);
        setAircraftIdent(aircraftIdent);
        setDepartureAirport(departureAirport);
        setDepartureRunway(departureRunway);
        setDepartureTime(departureTime);
        setArrivalAirport(arrivalAirport);
        setArrivalRunway(arrivalRunway);
        setArrivalTime(arrivalTime);
        setAircraftMake(aircraftMake);
        setAircraftModel(aircraftModel);
        setAircraftEngineNumber(aircraftEngineNumber);
        setAircraftEngineClass(aircraftEngineClass);
        setAircraftICAOCode(aircraftICAOCode);
        setDayTakeOffs(dayTakeOffs);
        setDayLandings(dayLandings);
        setNightTakeOffs(nightTakeOffs);
        setNightLandings(nightLandings);
        setInstrumentLandings(instrumentLandings);
        setInstrumentApproaches(instrumentApproaches);
        setPilotType(pilotType);
        setPilotStatus(pilotStatus);
        setDayHours(dayHours);
        setDayMinutes(dayMinutes);
        setNightHours(nightHours);
        setNightMinutes(nightMinutes);
        setGroundHours(groundHours);
        setGroundMinutes(groundMinutes);
        setCrossCountryHours(crossCountryHours);
        setCrossCountryMinutes(crossCountryMinutes);
        setInstructionHours(instructionHours);
        setInstructionMinutes(instructionMinutes);
        setIfrHours(ifrHours);
        setIfrMinutes(ifrMinutes);
        setHoodHours(hoodHours);
        setHoodMinutes(hoodMinutes);
        setSimulatorHours(simulatorHours);
        setSimulatorMinutes(simulatorMinutes);
        setWindHeading(windHeading);
        setWindSpeed(windSpeed);
        setSunny(isSunny);
        setCloudy(isCloudy);
        setRainy(isRainy);
        setStormy(isStormy);
        setHurricane(isHurricane);
        setLightning(isLightning);
        setTyphoon(isTyphoon);
        setBlizzard(isBlizzard);
        setFoggy(isFoggy);
        setHot(isHot);
        setCold(isCold);
        setWindy(isWindy);
        setWeather(weather);
    }

    public static FlightLog getCurrentFlightLog() {
        return currentFlightLog;
    }

    public static void setCurrentFlightLog(FlightLog newFlightLog) {
        currentFlightLog = newFlightLog;
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public int getFlightNumber() {
        return flightNumber.get();
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber.set(flightNumber);
    }

    public int getAircraftEngineNumber() {
        return aircraftEngineNumber.get();
    }

    public void setAircraftEngineNumber(int aircraftEngineNumber) {
        this.aircraftEngineNumber.set(aircraftEngineNumber);
    }

    public int getDayTakeOffs() {
        return dayTakeOffs.get();
    }

    public void setDayTakeOffs(int dayTakeOffs) {
        this.dayTakeOffs.set(dayTakeOffs);
    }

    public int getDayLandings() {
        return dayLandings.get();
    }

    public void setDayLandings(int dayLandings) {
        this.dayLandings.set(dayLandings);
    }

    public int getNightTakeOffs() {
        return nightTakeOffs.get();
    }

    public void setNightTakeOffs(int nightTakeOffs) {
        this.nightTakeOffs.set(nightTakeOffs);
    }

    public int getNightLandings() {
        return nightLandings.get();
    }

    public void setNightLandings(int nightLandings) {
        this.nightLandings.set(nightLandings);
    }

    public int getInstrumentLandings() {
        return instrumentLandings.get();
    }

    public void setInstrumentLandings(int instrumentLandings) {
        this.instrumentLandings.set(instrumentLandings);
    }

    public int getDayHours() {
        return dayHours.get();
    }

    public void setDayHours(int dayHours) {
        this.dayHours.set(dayHours);
    }

    public int getDayMinutes() {
        return dayMinutes.get();
    }

    public void setDayMinutes(int dayMinutes) {
        this.dayMinutes.set(dayMinutes);
    }

    public int getNightHours() {
        return nightHours.get();
    }

    public void setNightHours(int nightHours) {
        this.nightHours.set(nightHours);
    }

    public int getNightMinutes() {
        return nightMinutes.get();
    }

    public void setNightMinutes(int nightMinutes) {
        this.nightMinutes.set(nightMinutes);
    }

    public int getGroundHours() {
        return groundHours.get();
    }

    public void setGroundHours(int groundHours) {
        this.groundHours.set(groundHours);
    }

    public int getGroundMinutes() {
        return groundMinutes.get();
    }

    public void setGroundMinutes(int groundMinutes) {
        this.groundMinutes.set(groundMinutes);
    }

    public int getCrossCountryHours() {
        return crossCountryHours.get();
    }

    public void setCrossCountryHours(int crossCountryHours) {
        this.crossCountryHours.set(crossCountryHours);
    }

    public int getCrossCountryMinutes() {
        return crossCountryMinutes.get();
    }

    public void setCrossCountryMinutes(int crossCountryMinutes) {
        this.crossCountryMinutes.set(crossCountryMinutes);
    }

    public int getInstructionHours() {
        return instructionHours.get();
    }

    public void setInstructionHours(int instructionHours) {
        this.instructionHours.set(instructionHours);
    }

    public int getInstructionMinutes() {
        return instructionMinutes.get();
    }

    public void setInstructionMinutes(int instructionMinutes) {
        this.instructionMinutes.set(instructionMinutes);
    }

    public int getIfrHours() {
        return ifrHours.get();
    }

    public void setIfrHours(int ifrHours) {
        this.ifrHours.set(ifrHours);
    }

    public int getIfrMinutes() {
        return ifrMinutes.get();
    }

    public void setIfrMinutes(int ifrMinutes) {
        this.ifrMinutes.set(ifrMinutes);
    }

    public int getHoodHours() {
        return hoodHours.get();
    }

    public void setHoodHours(int hoodHours) {
        this.hoodHours.set(hoodHours);
    }

    public int getHoodMinutes() {
        return hoodMinutes.get();
    }

    public void setHoodMinutes(int hoodMinutes) {
        this.hoodMinutes.set(hoodMinutes);
    }

    public int getSimulatorHours() {
        return simulatorHours.get();
    }

    public void setSimulatorHours(int simulatorHours) {
        this.simulatorHours.set(simulatorHours);
    }

    public int getSimulatorMinutes() {
        return simulatorMinutes.get();
    }

    public void setSimulatorMinutes(int simulatorMinutes) {
        this.simulatorMinutes.set(simulatorMinutes);
    }

    public int getTotalMinutes() {
        return totalMinutes.get();
    }

    public void setTotalMinutes(int totalMinutes) {
        this.totalMinutes.set(totalMinutes);
    }

    public int getWindHeading() {
        return windHeading.get();
    }

    public void setWindHeading(int windHeading) {
        this.windHeading.set(windHeading);
    }

    public int getWindSpeed() {
        return windSpeed.get();
    }

    public void setWindSpeed(int windSpeed) {
        this.windSpeed.set(windSpeed);
    }

    public String getAircraftIdent() {
        return aircraftIdent.get();
    }

    public void setAircraftIdent(String aircraftIdent) {
        this.aircraftIdent.set(aircraftIdent);
    }

    public String getDepartureAirport() {
        return departureAirport.get();
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport.set(departureAirport);
    }

    public String getDepartureRunway() {
        return departureRunway.get();
    }

    public void setDepartureRunway(String departureRunway) {
        this.departureRunway.set(departureRunway);
    }

    public String getArrivalAirport() {
        return arrivalAirport.get();
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport.set(arrivalAirport);
    }

    public String getArrivalRunway() {
        return arrivalRunway.get();
    }

    public void setArrivalRunway(String arrivalRunway) {
        this.arrivalRunway.set(arrivalRunway);
    }

    public String getAircraftMake() {
        return aircraftMake.get();
    }

    public void setAircraftMake(String aircraftMake) {
        this.aircraftMake.set(aircraftMake);
    }

    public String getAircraftModel() {
        return aircraftModel.get();
    }

    public void setAircraftModel(String aircraftModel) {
        this.aircraftModel.set(aircraftModel);
    }

    public String getAircraftEngineClass() {
        return aircraftEngineClass.get();
    }

    public void setAircraftEngineClass(String aircraftEngineClass) {
        this.aircraftEngineClass.set(aircraftEngineClass);
    }

    public String getAircraftICAOCode() {
        return aircraftICAOCode.get();
    }

    public void setAircraftICAOCode(String aircraftICAOCode) {
        this.aircraftICAOCode.set(aircraftICAOCode);
    }

    public String getInstrumentApproaches() {
        return instrumentApproaches.get();
    }

    public void setInstrumentApproaches(String instrumentApproaches) {
        this.instrumentApproaches.set(instrumentApproaches);
    }

    public String getPilotType() {
        return pilotType.get();
    }

    public void setPilotType(String pilotType) {
        this.pilotType.set(pilotType);
    }

    public String getPilotStatus() {
        return pilotStatus.get();
    }

    public void setPilotStatus(String pilotStatus) {
        this.pilotStatus.set(pilotStatus);
    }

    public String getWeather() {
        return weather.get();
    }

    public void setWeather(String weather) {
        this.weather.set(weather);
    }

    public boolean isSunny() {
        return sunny.get();
    }

    public void setSunny(boolean sunny) {
        this.sunny.set(sunny);
    }

    public boolean isCloudy() {
        return cloudy.get();
    }

    public void setCloudy(boolean cloudy) {
        this.cloudy.set(cloudy);
    }

    public boolean isRainy() {
        return rainy.get();
    }

    public void setRainy(boolean rainy) {
        this.rainy.set(rainy);
    }

    public boolean isStormy() {
        return stormy.get();
    }

    public void setStormy(boolean stormy) {
        this.stormy.set(stormy);
    }

    public boolean isHurricane() {
        return hurricane.get();
    }

    public void setHurricane(boolean hurricane) {
        this.hurricane.set(hurricane);
    }

    public boolean isLightning() {
        return lightning.get();
    }

    public void setLightning(boolean lightning) {
        this.lightning.set(lightning);
    }

    public boolean isTyphoon() {
        return typhoon.get();
    }

    public void setTyphoon(boolean typhoon) {
        this.typhoon.set(typhoon);
    }

    public boolean isBlizzard() {
        return blizzard.get();
    }

    public void setBlizzard(boolean blizzard) {
        this.blizzard.set(blizzard);
    }

    public boolean isFoggy() {
        return foggy.get();
    }

    public void setFoggy(boolean foggy) {
        this.foggy.set(foggy);
    }

    public boolean isHot() {
        return hot.get();
    }

    public void setHot(boolean hot) {
        this.hot.set(hot);
    }

    public boolean isCold() {
        return cold.get();
    }

    public void setCold(boolean cold) {
        this.cold.set(cold);
    }

    public boolean isWindy() {
        return windy.get();
    }

    public void setWindy(boolean windy) {
        this.windy.set(windy);
    }

    public LocalDate getDate() {
        return date.get();
    }

    public void setDate(LocalDate date) {
        this.date.set(date);
    }

    public LocalTime getDepartureTime() {
        return departureTime.get();
    }

    public void setDepartureTime(LocalTime departureTime) {
        this.departureTime.set(departureTime);
    }

    public LocalTime getArrivalTime() {
        return arrivalTime.get();
    }

    public void setArrivalTime(LocalTime arrivalTime) {
        this.arrivalTime.set(arrivalTime);
    }

    // Pilot Types
    public static final ObservableList<String> pilotTypes = FXCollections.observableArrayList(
            "Pilot", "Flight Instructor", "Ground Instructor", "Simulator"
    );

    // Pilot Statuses
    public static final ObservableList<String> pilotStatuses = FXCollections.observableArrayList(
            "Pilot In Command", "Student Pilot In Command", "Pilot In Command Under Supervision",
            "Dual Pilot and Instructor Command"
    );

    // Create a Flight Log
    public static boolean createFlightLog(FlightLog flightLog, User user) {

        System.out.println(Helper.changeColor("Creating Flight Log...", Helper.Color.YELLOW));

        // Add the Flight Log to the database
        String query = "INSERT INTO flight_logs VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query,
                Statement.RETURN_GENERATED_KEYS)) {
            ps.setInt(1, user.getId());
            ps.setDate(2, Date.valueOf(flightLog.getDate()));
            if (flightLog.getFlightNumber() != 0) ps.setInt(3, flightLog.getFlightNumber()); else ps.setNull(3, Types.SMALLINT);
            if (!flightLog.getAircraftIdent().equals("")) ps.setString(4, flightLog.getAircraftIdent().toUpperCase()); else ps.setNull(4, Types.VARCHAR);
            ps.setString(5, flightLog.getDepartureAirport().toUpperCase());
            if (!flightLog.getDepartureRunway().equals("")) ps.setString(6, flightLog.getDepartureRunway()); else ps.setNull(6, Types.VARCHAR);
            ps.setTime(7, Time.valueOf(flightLog.getDepartureTime()));
            ps.setString(8, flightLog.getArrivalAirport().toUpperCase());
            if (!flightLog.getArrivalRunway().equals("")) ps.setString(9, flightLog.getArrivalRunway()); else ps.setNull(9, Types.VARCHAR);
            ps.setTime(10, Time.valueOf(flightLog.getArrivalTime()));
            ps.setString(11, flightLog.getAircraftMake().toUpperCase());
            ps.setString(12, flightLog.getAircraftModel().toUpperCase());
            if (flightLog.getAircraftEngineNumber() != 0) ps.setInt(13, flightLog.getAircraftEngineNumber()); else ps.setNull(13, Types.TINYINT);
            if (!flightLog.getAircraftEngineClass().equals("")) ps.setString(14, flightLog.getAircraftEngineClass().toUpperCase()); else ps.setNull(14, Types.VARCHAR);
            if (!flightLog.getAircraftICAOCode().equals("")) ps.setString(15, flightLog.getAircraftICAOCode().toUpperCase()); else ps.setNull(15, Types.VARCHAR);
            ps.setInt(16, flightLog.getDayTakeOffs());
            ps.setInt(17, flightLog.getDayLandings());
            ps.setInt(18, flightLog.getNightTakeOffs());
            ps.setInt(19, flightLog.getNightLandings());
            ps.setInt(20, flightLog.getInstrumentLandings());
            if (!flightLog.getInstrumentApproaches().equals("")) ps.setString(21, flightLog.getInstrumentApproaches().toUpperCase()); else ps.setNull(21, Types.VARCHAR);
            ps.setString(22, flightLog.getPilotType().toUpperCase());
            ps.setString(23, flightLog.getPilotStatus().toUpperCase());
            ps.setInt(24, flightLog.getDayHours());
            ps.setInt(25, flightLog.getDayMinutes());
            ps.setInt(26, flightLog.getNightHours());
            ps.setInt(27, flightLog.getNightMinutes());
            ps.setInt(28, flightLog.getGroundHours());
            ps.setInt(29, flightLog.getGroundMinutes());
            ps.setInt(30, flightLog.getCrossCountryHours());
            ps.setInt(31, flightLog.getCrossCountryMinutes());
            ps.setInt(32, flightLog.getInstructionHours());
            ps.setInt(33, flightLog.getInstructionMinutes());
            ps.setInt(34, flightLog.getIfrHours());
            ps.setInt(35, flightLog.getIfrMinutes());
            ps.setInt(36, flightLog.getHoodHours());
            ps.setInt(37, flightLog.getHoodMinutes());
            ps.setInt(38, flightLog.getSimulatorHours());
            ps.setInt(39, flightLog.getSimulatorMinutes());
            ps.setInt(40, flightLog.getWindHeading());
            ps.setInt(41, flightLog.getWindSpeed());
            ps.setInt(42, flightLog.isSunny() ? 1 : 0);
            ps.setInt(43, flightLog.isCloudy() ? 1 : 0);
            ps.setInt(44, flightLog.isRainy() ? 1 : 0);
            ps.setInt(45, flightLog.isStormy() ? 1 : 0);
            ps.setInt(46, flightLog.isHurricane() ? 1 : 0);
            ps.setInt(47, flightLog.isLightning() ? 1 : 0);
            ps.setInt(48, flightLog.isTyphoon() ? 1 : 0);
            ps.setInt(49, flightLog.isBlizzard() ? 1 : 0);
            ps.setInt(50, flightLog.isFoggy() ? 1 : 0);
            ps.setInt(51, flightLog.isHot() ? 1 : 0);
            ps.setInt(52, flightLog.isCold() ? 1 : 0);
            ps.setInt(53, flightLog.isWindy() ? 1 : 0);

            int rowsAffected = ps.executeUpdate();
            System.out.println("\tCreated Flight Log: " + rowsAffected + " row affected.");
            if (rowsAffected == 0) {
                System.out.println("\tFlight Log Creation Failed");
                return false;
            }

            // Get the key of the insert
            try (ResultSet generatedKey = ps.getGeneratedKeys()) {
                if (generatedKey.next()) {
                    System.out.println("\tLog ID: " + generatedKey.getInt(1));
                }
            }

            System.out.println(Helper.changeColor("Flight Log Created", Helper.Color.GREEN) + "\n");


        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return true;
    }

    // Get All Flight Logs
    public static ObservableList<FlightLog> getFlightLogs(User user) {
        ObservableList<FlightLog> flightLogList = FXCollections.observableArrayList();

        String query = "SELECT * FROM flight_logs WHERE user_id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setInt(1, user.getId());

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {

                    // Create Weather String
                    HashMap<String, Boolean> weatherHashMap = new HashMap<String, Boolean>()
                    {{
                        put("Sunny", (rs.getInt("weather_sunny")) == 1);
                        put("Cloudy", (rs.getInt("weather_cloudy") == 1));
                        put("Rainy", (rs.getInt("weather_rainy") == 1));
                        put("Stormy", (rs.getInt("weather_stormy") == 1));
                        put("Hurricane", (rs.getInt("weather_hurricane") == 1));
                        put("Lightning", (rs.getInt("weather_lightning") == 1));
                        put("Typhoon", (rs.getInt("weather_typhoon") == 1));
                        put("Blizzard", (rs.getInt("weather_blizzard") == 1));
                        put("Foggy", (rs.getInt("weather_foggy") == 1));
                        put("Hot", (rs.getInt("weather_hot") == 1));
                        put("Cold", (rs.getInt("weather_cold") == 1));
                        put("Windy", (rs.getInt("weather_windy") == 1));
                    }};

                    // Create the Flight Log Object
                    flightLogList.add(new FlightLog(
                            rs.getInt("id"),
                            rs.getDate("log_date").toLocalDate(),
                            rs.getInt("flight_number"),
                            rs.getString("aircraft_ident"),
                            Helper.toTitleCase(rs.getString("departure_airport")),
                            rs.getString("departure_runway"),
                            rs.getTime("departure_time").toLocalTime(),
                            Helper.toTitleCase(rs.getString("arrival_airport")),
                            rs.getString("arrival_runway"),
                            rs.getTime("arrival_time").toLocalTime(),
                            Helper.toTitleCase(rs.getString("aircraft_make")),
                            Helper.toTitleCase(rs.getString("aircraft_model")),
                            rs.getInt("aircraft_engine_number"),
                            Helper.toTitleCase(rs.getString("aircraft_engine_class")),
                            rs.getString("aircraft_icao_code"),
                            rs.getInt("day_takeoffs"),
                            rs.getInt("day_landings"),
                            rs.getInt("night_takeoffs"),
                            rs.getInt("night_landings"),
                            rs.getInt("instrument_landings"),
                            rs.getString("instrument_approaches"),
                            Helper.toTitleCase(rs.getString("pilot_type")),
                            Helper.toTitleCase(rs.getString("pilot_status")),
                            rs.getInt("day_hours"),
                            rs.getInt("day_minutes"),
                            rs.getInt("night_hours"),
                            rs.getInt("night_minutes"),
                            rs.getInt("ground_hours"),
                            rs.getInt("ground_minutes"),
                            rs.getInt("cross_country_hours"),
                            rs.getInt("cross_country_minutes"),
                            rs.getInt("instruction_hours"),
                            rs.getInt("instruction_minutes"),
                            rs.getInt("ifr_hours"),
                            rs.getInt("ifr_minutes"),
                            rs.getInt("hood_hours"),
                            rs.getInt("hood_minutes"),
                            rs.getInt("simulator_hours"),
                            rs.getInt("simulator_minutes"),
                            rs.getInt("wind_heading"),
                            rs.getInt("wind_speed"),
                            weatherHashMap.get("Sunny"),
                            weatherHashMap.get("Cloudy"),
                            weatherHashMap.get("Rainy"),
                            weatherHashMap.get("Stormy"),
                            weatherHashMap.get("Hurricane"),
                            weatherHashMap.get("Lightning"),
                            weatherHashMap.get("Typhoon"),
                            weatherHashMap.get("Blizzard"),
                            weatherHashMap.get("Foggy"),
                            weatherHashMap.get("Hot"),
                            weatherHashMap.get("Cold"),
                            weatherHashMap.get("Windy"),
                            Helper.booleansToString(weatherHashMap)
                    ));
                }
            }

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return flightLogList;
    }

    // Update a Flight Log
    public static boolean updateFlightLog(FlightLog flightLog) {

        System.out.println(Helper.changeColor("Updating Flight Log...", Helper.Color.YELLOW));

        String query = "UPDATE flight_logs SET log_date = ?, flight_number = ?, aircraft_ident = ?, departure_airport = ?, " +
                "departure_runway = ?, departure_time = ?, arrival_airport = ?, arrival_runway = ?, arrival_time = ?, " +
                "aircraft_make = ?, aircraft_model = ?, aircraft_engine_number = ?, aircraft_engine_class = ?, " +
                "aircraft_icao_code = ?, day_takeoffs = ?, day_landings = ?, night_takeoffs = ?, night_landings = ?, " +
                "instrument_landings = ?, instrument_approaches = ?, pilot_type = ?, pilot_status = ?, day_hours = ?, " +
                "day_minutes = ?, night_hours = ?, night_minutes = ?, ground_hours = ?, ground_minutes = ?, " +
                "cross_country_hours = ?, cross_country_minutes = ?, instruction_hours = ?, instruction_minutes = ?, " +
                "ifr_hours = ?, ifr_minutes = ?, hood_hours = ?, hood_minutes = ?, simulator_hours = ?, simulator_minutes = ?, " +
                "wind_heading = ?, wind_speed = ?, weather_sunny = ?, weather_cloudy = ?, weather_rainy = ?, " +
                "weather_stormy = ?, weather_hurricane = ?, weather_lightning = ?, weather_typhoon = ?, weather_blizzard = ?, " +
                "weather_foggy = ?, weather_hot = ?, weather_cold = ?, weather_windy = ? " +
                "WHERE id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setDate(1, Date.valueOf(flightLog.getDate()));
            if (flightLog.getFlightNumber() != 0) ps.setInt(2, flightLog.getFlightNumber()); else ps.setNull(2, Types.SMALLINT);
            if (!flightLog.getAircraftIdent().equals("")) ps.setString(3, flightLog.getAircraftIdent().toUpperCase()); else ps.setNull(3, Types.VARCHAR);
            ps.setString(4, flightLog.getDepartureAirport().toUpperCase());
            if (!flightLog.getDepartureRunway().equals("")) ps.setString(5, flightLog.getDepartureRunway()); else ps.setNull(5, Types.VARCHAR);
            ps.setTime(6, Time.valueOf(flightLog.getDepartureTime()));
            ps.setString(7, flightLog.getArrivalAirport().toUpperCase());
            if (!flightLog.getArrivalRunway().equals("")) ps.setString(8, flightLog.getArrivalRunway()); else ps.setNull(8, Types.VARCHAR);
            ps.setTime(9, Time.valueOf(flightLog.getArrivalTime()));
            ps.setString(10, flightLog.getAircraftMake().toUpperCase());
            ps.setString(11, flightLog.getAircraftModel().toUpperCase());
            if (flightLog.getAircraftEngineNumber() != 0) ps.setInt(12, flightLog.getAircraftEngineNumber()); else ps.setNull(12, Types.TINYINT);
            if (!flightLog.getAircraftEngineClass().equals("")) ps.setString(13, flightLog.getAircraftEngineClass().toUpperCase()); else ps.setNull(13, Types.VARCHAR);
            if (!flightLog.getAircraftICAOCode().equals("")) ps.setString(14, flightLog.getAircraftICAOCode().toUpperCase()); else ps.setNull(14, Types.VARCHAR);
            ps.setInt(15, flightLog.getDayTakeOffs());
            ps.setInt(16, flightLog.getDayLandings());
            ps.setInt(17, flightLog.getNightTakeOffs());
            ps.setInt(18, flightLog.getNightLandings());
            ps.setInt(19, flightLog.getInstrumentLandings());
            if (!flightLog.getInstrumentApproaches().equals("")) ps.setString(20, flightLog.getInstrumentApproaches().toUpperCase()); else ps.setNull(20, Types.VARCHAR);
            ps.setString(21, flightLog.getPilotType().toUpperCase());
            ps.setString(22, flightLog.getPilotStatus().toUpperCase());
            ps.setInt(23, flightLog.getDayHours());
            ps.setInt(24, flightLog.getDayMinutes());
            ps.setInt(25, flightLog.getNightHours());
            ps.setInt(26, flightLog.getNightMinutes());
            ps.setInt(27, flightLog.getGroundHours());
            ps.setInt(28, flightLog.getGroundMinutes());
            ps.setInt(29, flightLog.getCrossCountryHours());
            ps.setInt(30, flightLog.getCrossCountryMinutes());
            ps.setInt(31, flightLog.getInstructionHours());
            ps.setInt(32, flightLog.getInstructionMinutes());
            ps.setInt(33, flightLog.getIfrHours());
            ps.setInt(34, flightLog.getIfrMinutes());
            ps.setInt(35, flightLog.getHoodHours());
            ps.setInt(36, flightLog.getHoodMinutes());
            ps.setInt(37, flightLog.getSimulatorHours());
            ps.setInt(38, flightLog.getSimulatorMinutes());
            ps.setInt(39, flightLog.getWindHeading());
            ps.setInt(40, flightLog.getWindSpeed());
            ps.setInt(41, flightLog.isSunny() ? 1 : 0);
            ps.setInt(42, flightLog.isCloudy() ? 1 : 0);
            ps.setInt(43, flightLog.isRainy() ? 1 : 0);
            ps.setInt(44, flightLog.isStormy() ? 1 : 0);
            ps.setInt(45, flightLog.isHurricane() ? 1 : 0);
            ps.setInt(46, flightLog.isLightning() ? 1 : 0);
            ps.setInt(47, flightLog.isTyphoon() ? 1 : 0);
            ps.setInt(48, flightLog.isBlizzard() ? 1 : 0);
            ps.setInt(49, flightLog.isFoggy() ? 1 : 0);
            ps.setInt(50, flightLog.isHot() ? 1 : 0);
            ps.setInt(51, flightLog.isCold() ? 1 : 0);
            ps.setInt(52, flightLog.isWindy() ? 1 : 0);
            ps.setInt(53, flightLog.getId());

            int rowsAffected = ps.executeUpdate();
            System.out.println("\tUpdated Flight Log: " + rowsAffected + " row affected.");
            if (rowsAffected == 0) {
                System.out.println("\tFlight Log Update Failed");
                return false;
            }

            System.out.println("\tLog ID: " + flightLog.getId());

            System.out.println(Helper.changeColor("Flight Log Updated", Helper.Color.GREEN) + "\n");

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return true;
    }

    // Delete a flight log
    public static void deleteFlightLog(int logID) {

        System.out.println(Helper.changeColor("Deleting Flight Log...", Helper.Color.YELLOW));

        // Delete the Flight Log from the database
        String query = "DELETE FROM flight_logs WHERE id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setInt(1, logID);

            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 0) {
                System.out.println("\tFlight Log Deletion Failed");
            } else {
                System.out.println("\tDeleted Flight Log with ID: " + logID);
            }

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

    }

}