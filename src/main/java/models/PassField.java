//*******************************************************************
// PassField
//
// Constructs a Password Field Object for validation.
//*******************************************************************

package main.java.models;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.PasswordField;

public class PassField {

    private final SimpleStringProperty
            name = new SimpleStringProperty(),
            value = new SimpleStringProperty();

    private final SimpleIntegerProperty
            length = new SimpleIntegerProperty();

    private PasswordField passField;

    public PassField(String name, PasswordField passField, String value, int length) {
        setName(name);
        setPassField(passField);
        setValue(value);
        setLength(length);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public PasswordField getPassField() {
        return passField;
    }

    public void setPassField(PasswordField passField) {
        this.passField = passField;
    }

    public String getValue() {
        return value.get();
    }

    public SimpleStringProperty valueProperty() {
        return value;
    }

    public void setValue(String value) {
        this.value.set(value);
    }

    public int getLength() {
        return length.get();
    }

    public SimpleIntegerProperty lengthProperty() {
        return length;
    }

    public void setLength(int length) {
        this.length.set(length);
    }
}
