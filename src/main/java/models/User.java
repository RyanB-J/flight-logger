//*******************************************************************
// User
//
// Constructs a User Object and controls User related operations from the database.
//*******************************************************************

package main.java.models;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.java.util.Database;
import main.java.util.Helper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class User {

    private final SimpleIntegerProperty
            id = new SimpleIntegerProperty();

    private final SimpleStringProperty
            username = new SimpleStringProperty(),
            email = new SimpleStringProperty(),
            password = new SimpleStringProperty();

    private final SimpleBooleanProperty
            instructor = new SimpleBooleanProperty();

    public User() {}

    public User(int id, String username, String email, String password, boolean instructor) {
        setId(id);
        setUsername(username);
        setEmail(email);
        setPassword(password);
        setInstructor(instructor);
    }

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getUsername() {
        return username.get();
    }

    public SimpleStringProperty usernameProperty() {
        return username;
    }

    public void setUsername(String username) {
        this.username.set(username);
    }

    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getPassword() {
        return password.get();
    }

    public SimpleStringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public boolean isInstructor() {
        return instructor.get();
    }

    public SimpleBooleanProperty instructorProperty() {
        return instructor;
    }

    public void setInstructor(boolean instructor) {
        this.instructor.set(instructor);
    }

    // Create a user
    public static User createUser(String username, String password, String email, boolean instructor) {

        System.out.println(Helper.changeColor("Creating User...", Helper.Color.YELLOW));

        // Instantiate User object
        User newUser = new User();

        // Run the INSERT query
        String query = "INSERT INTO users" +
                " (username, password, email, instructor)" +
                " VALUES (?, ?, ?, ?)";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query,
                Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, email);
            ps.setInt(4, (instructor ? 1 : 0));

            int rowsAffected = ps.executeUpdate();
            System.out.println("\tCreated User: " + rowsAffected + " row affected.");
            if (rowsAffected == 0) System.out.println("\tUser Creation Failed");

            // Get the key of the insert
            try (ResultSet generatedKey = ps.getGeneratedKeys()) {
                if (generatedKey.next()) {
                    newUser.setId(generatedKey.getInt(1));
                    System.out.println("\tUser ID: " + newUser.getId());
                }
            }

            // Add to the new User object
            newUser.setUsername(username);
            newUser.setPassword(password);
            newUser.setEmail(email);
            newUser.setInstructor(instructor);

            System.out.println(Helper.changeColor("User Created", Helper.Color.GREEN) + "\n");

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return newUser;
    }

    // Get all non-instructor usernames
    public static ObservableList<String> getNonInstructorUsernames(User currentUser) {
        ObservableList<String> userList = FXCollections.observableArrayList();

        String query = "SELECT username FROM users WHERE instructor = 0 OR id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setInt(1, currentUser.getId());

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    userList.add(rs.getString("username"));
                }
            }
        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return userList;
    }

    // Get a user by their username
    public static User getUserByName(String username) {
        User matchedUser = new User();

        String query = "SELECT * FROM users WHERE username = LOWER(?)";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setString(1, username.toLowerCase());

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    matchedUser.setId(rs.getInt("id"));
                    matchedUser.setUsername(rs.getString("username"));
                    matchedUser.setPassword(rs.getString("password"));
                    matchedUser.setEmail(rs.getString("email"));
                    matchedUser.setInstructor(rs.getInt("instructor") == 1);
                }
            }

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return matchedUser;
    }

    // Check if a user exists
    public static boolean exists(String username) {
        boolean existingUser = false;

        String query = "SELECT EXISTS(SELECT username FROM users WHERE username = LOWER(?))";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setString(1, username.toLowerCase());

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    int result = rs.getInt(1);
                    if (result > 0) existingUser = true;
                }
            }

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return existingUser;
    }

    // Check if username and password match
    public static boolean authenticate(String username, String password) {
        boolean authenticated = false;

        String query = "SELECT password FROM users WHERE username = LOWER(?)";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setString(1, username.toLowerCase());

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    if (password.equals(rs.getString(1))) {
                        authenticated = true;
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return authenticated;
    }

    // Update a user
    public static User updateUser(User currentUser, String username, String password, String email, boolean instructor) {

        // Run the UPDATE query
        String query = "UPDATE users SET username = ?, password = ?, email = ?, instructor = ? WHERE id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query,
                Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, email);
            ps.setInt(4, (instructor ? 1 : 0));
            ps.setInt(5, currentUser.getId());

            int rowsAffected = ps.executeUpdate();
            System.out.println("\tUpdated User: " + rowsAffected + " row affected.");
            if (rowsAffected == 0) System.out.println("\tUser Update Failed");

            // Get the key of the insert
            try (ResultSet generatedKey = ps.getGeneratedKeys()) {
                if (generatedKey.next()) {
                    currentUser.setId(generatedKey.getInt(1));
                    System.out.println("\tUser ID: " + currentUser.getId());
                }
            }

            // Add to the new User object
            currentUser.setUsername(username);
            currentUser.setPassword(password);
            currentUser.setEmail(email);
            currentUser.setInstructor(instructor);

            System.out.println(Helper.changeColor("User Updated", Helper.Color.GREEN) + "\n");

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return currentUser;
    }


}
