//*******************************************************************
// Runway
//
// Constructs a Runway Object and controls Runway related operations from the database.
//*******************************************************************

package main.java.models;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.java.util.Database;
import main.java.util.Helper;

import java.sql.*;

public class Runway {

    private final SimpleIntegerProperty
            id = new SimpleIntegerProperty(),
            length = new SimpleIntegerProperty(),
            width = new SimpleIntegerProperty(),
            baseHeading = new SimpleIntegerProperty(),
            reciprocalHeading = new SimpleIntegerProperty();

    private final SimpleDoubleProperty
            baseElevation = new SimpleDoubleProperty(),
            reciprocalElevation = new SimpleDoubleProperty();

    private final SimpleStringProperty
            relationalId = new SimpleStringProperty(),
            name = new SimpleStringProperty(),
            baseNumber = new SimpleStringProperty(),
            reciprocalNumber = new SimpleStringProperty(),
            surface = new SimpleStringProperty();

    public Runway() { }

    public Runway(int id, String relationalId, String name, int length, int width, String surface, String baseNumber,
                  int baseHeading, Double baseElevation, String reciprocalNumber, int reciprocalHeading,
                  Double reciprocalElevation) {
        setId(id);
        setRelationalId(relationalId);
        setName(name);
        setLength(length);
        setWidth(width);
        setSurface(surface);
        setBaseNumber(baseNumber);
        setBaseHeading(baseHeading);
        setBaseElevation(baseElevation);
        setReciprocalNumber(reciprocalNumber);
        setReciprocalHeading(reciprocalHeading);
        setReciprocalElevation(reciprocalElevation);
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public int getLength() {
        return length.get();
    }

    public void setLength(int length) {
        this.length.set(length);
    }

    public int getWidth() {
        return width.get();
    }

    public void setWidth(int width) {
        this.width.set(width);
    }

    public int getBaseHeading() {
        return baseHeading.get();
    }

    public void setBaseHeading(int baseHeading) {
        this.baseHeading.set(baseHeading);
    }

    public Double getBaseElevation() {
        return baseElevation.get();
    }

    public void setBaseElevation(Double baseElevation) {
        this.baseElevation.set(baseElevation);
    }

    public int getReciprocalHeading() {
        return reciprocalHeading.get();
    }

    public void setReciprocalHeading(int reciprocalHeading) {
        this.reciprocalHeading.set(reciprocalHeading);
    }

    public Double getReciprocalElevation() {
        return reciprocalElevation.get();
    }

    public void setReciprocalElevation(Double reciprocalElevation) {
        this.reciprocalElevation.set(reciprocalElevation);
    }

    public String getRelationalId() {
        return relationalId.get();
    }

    public void setRelationalId(String relationalId) {
        this.relationalId.set(relationalId);
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getBaseNumber() {
        return baseNumber.get();
    }

    public void setBaseNumber(String baseNumber) {
        this.baseNumber.set(baseNumber);
    }

    public String getReciprocalNumber() {
        return reciprocalNumber.get();
    }

    public void setReciprocalNumber(String reciprocalNumber) {
        this.reciprocalNumber.set(reciprocalNumber);
    }

    public String getSurface() {
        return surface.get();
    }

    public void setSurface(String surface) {
        this.surface.set(surface);
    }

    // Surfaces
    public static final ObservableList<String> surfaces = FXCollections.observableArrayList(
            "", "Asphalt", "Concrete", "Turf", "Dirt", "Gravel", "Water", "Mats", "Roof", "Sand", "Other"
    );

    // Create Runway
    public static void createRunway(String relationalID, String name, int length, int width, String surface,
                                    String baseNumber, int baseHeading, Double baseElevation, String reciprocalNumber,
                                    int reciprocalHeading, Double reciprocalElevation) {

        // Add the runway to the database
        String query = "INSERT INTO runways " +
                "(relational_id, name, length, width, surface, base_num, base_head, base_elev, rec_num, rec_head, rec_elev) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query,
                Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, relationalID);
            ps.setString(2, name.toUpperCase());
            if (length > 0) ps.setInt(3, length); else ps.setNull(3, Types.SMALLINT);
            if (width > 0) ps.setInt(4, width); else ps.setNull(4, Types.SMALLINT);
            ps.setString(5, surface.toUpperCase());
            if (!baseNumber.isEmpty()) ps.setString(6, baseNumber); else ps.setNull(6, Types.CHAR);
            ps.setInt(7, baseHeading);
            ps.setDouble(8, baseElevation);
            if (!reciprocalNumber.isEmpty()) ps.setString(9, reciprocalNumber); else ps.setNull(9, Types.CHAR);
            ps.setInt(10, reciprocalHeading);
            ps.setDouble(11, reciprocalElevation);

            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 0) {
                System.out.println("\t\tRunway Creation Failed");
            }

            // Get the key of the insert
            try (ResultSet generatedKey = ps.getGeneratedKeys()) {
                if (generatedKey.next()) {
                    System.out.println("\t\tCreated Runway with ID: " + generatedKey.getInt(1) +
                            " with relations to ID: " + relationalID);
                }
            }

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }
    }

    // Get Runways
    public static ObservableList<Runway> getRunways(String relationalID) {
        ObservableList<Runway> runwayList = FXCollections.observableArrayList();

        String query = "SELECT * FROM runways WHERE relational_id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setString(1, relationalID);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Runway runway = new Runway(
                            rs.getInt("id"),
                            rs.getString("relational_id"),
                            rs.getString("name"),
                            rs.getInt("length"),
                            rs.getInt("width"),
                            rs.getString("surface"),
                            rs.getString("base_num"),
                            rs.getInt("base_head"),
                            rs.getDouble("base_elev"),
                            rs.getString("rec_num"),
                            rs.getInt("rec_head"),
                            rs.getDouble("rec_elev")
                    );
                    runwayList.add(runway);
                }
            }

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        return runwayList;
    }

    // Update a Runway
    public static void updateRunway(int id, String relationalID, String name, int length, int width, String surface,
                                    String baseNumber, int baseHeading, Double baseElevation, String reciprocalNumber,
                                    int reciprocalHeading, Double reciprocalElevation) {

        // Update the runway in the database
        String query = "UPDATE runways " +
                "SET name = ?, length = ?, width = ?, surface = ?, base_num = ?, base_head = ?, base_elev = ?, " +
                "rec_num = ?, rec_head = ?, rec_elev = ? " +
                "WHERE id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setString(1, name.toUpperCase());
            if (length > 0) ps.setInt(2, length); else ps.setNull(2, Types.SMALLINT);
            if (width > 0) ps.setInt(3, width); else ps.setNull(3, Types.SMALLINT);
            ps.setString(4, surface.toUpperCase());
            if (!baseNumber.isEmpty()) ps.setString(5, baseNumber); else ps.setNull(5, Types.CHAR);
            ps.setInt(6, baseHeading);
            ps.setDouble(7, baseElevation);
            if (!reciprocalNumber.isEmpty()) ps.setString(8, reciprocalNumber); else ps.setNull(8, Types.CHAR);
            ps.setInt(9, reciprocalHeading);
            ps.setDouble(10, reciprocalElevation);
            ps.setInt(11, id);

            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 0) {
                System.out.println("\t\tRunway Update Failed");
            } else {
                System.out.println("\t\tUpdated Runway with ID: " + id + " with relations to ID: " + relationalID);
            }



        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }
    }

    // Delete a Runway
    public static void deleteRunway(int id) {

        // Delete the runway from the database
        String query = "DELETE FROM runways WHERE id = ?";
        try (PreparedStatement ps = Database.getConnection().prepareStatement(query)) {
            ps.setInt(1, id);

            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 0) {
                System.out.println("\t\tRunway Deletion Failed");
            } else {
                System.out.println("\t\tDeleted Runway with ID: " + id);
            }

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }
    }

}
