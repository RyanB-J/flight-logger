///////////////////////////////////////////////////////////////////////////////
//
// Title:   Flight Logger
// Course:  C868 - Software Development Capstone
// Author:  Ryan Bains-Jordan
// Email:   rbains1@wgu.edu
//
///////////////////////////////////////////////////////////////////////////////

//********************
// Test Users
// Ryan - password$2
// Samantha - %password%2
// Justin - password1234$+
//********************

package main.java;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import main.java.models.FlightLog;
import main.java.models.User;
import main.java.util.Database;
import main.java.util.AppAlerts;
import main.java.util.Helper;

import java.io.IOException;

public class FlightLogger extends Application {

    // State Variables
    private static Stage stage;
    private static Stage modalStage;

    // Current User
    private static User currentUser = null;
    public static User getCurrentUser() {
        return currentUser;
    }
    public static void setCurrentUser(User currentUser) {
        FlightLogger.currentUser = currentUser;
        System.out.println("Logged in as " + Helper.changeColor(currentUser.getUsername(), Helper.Color.BLUE) + "\n");
    }

    // Test method to start on a different screen
    private static void developerMode(String username, String startingScreen) {
        setCurrentUser(User.getUserByName(username));
        System.out.println(Helper.changeColor("Developer Mode Activated", Helper.Color.GREEN));
        System.out.println("\tLogged in as " + Helper.changeColor(currentUser.getUsername(), Helper.Color.BLUE));
        System.out.println("\tStarting on screen: " + Helper.changeColor(startingScreen, Helper.Color.BLUE) + "\n");
    }

    // General scene changer for all scenes
    public static void changeSceneHandler(String FXMLFile) throws IOException {
        Parent viewFile = FXMLLoader.load(FlightLogger.class.getResource("/main/resources/views/" + FXMLFile + ".fxml"));
        Scene scene = new Scene(viewFile);

        modalStage = null;
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                if (FlightLog.getCurrentFlightLog() != null) {
                    if (AppAlerts.throwFlightLogProgressWarning() == ButtonType.OK) {
                        Platform.exit();
                    } else event.consume();
                } else Platform.exit();
            }
        });
    }

    // Scene adder for adding a modal scene
    public static void addSceneHandler(String FXMLFile, String title) throws IOException {
        Parent viewFile = FXMLLoader.load(FlightLogger.class.getResource("/main/resources/views/" + FXMLFile + ".fxml"));
        Scene scene = new Scene(viewFile);

        Stage stage = new Stage();
        modalStage = stage;
        stage.setTitle(title);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
    }

    // Shortcut for scene changing
    public static void changeScenes(String FXMLFile) {
        try {
            changeSceneHandler(FXMLFile);
        } catch (IOException e) {
            System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
        }
    }

    // Prompt a save confirmation before changing scenes (For Flight Logging)
    public static void changeScenesWithSave(String FXMLFile) {
        ButtonType alertResult = AppAlerts.throwSaveChangesConfirmationAlert();
        if (alertResult != ButtonType.CANCEL) {
            if (alertResult == ButtonType.NO) {
                FlightLog.setCurrentFlightLog(null);
            }
            FlightLogger.changeScenes(FXMLFile);
        }
    }

    // Prompt a confirmation before changing scenes
    public static void changeScenesWithConfirmation(String FXMLFile, FlightLog newCurrentLog) {
        if (FlightLog.getCurrentFlightLog() != null) {
            ButtonType alertResult = AppAlerts.throwFlightLogProgressWarning();
            if (alertResult == ButtonType.OK) {
                FlightLog.setCurrentFlightLog(newCurrentLog);
                FlightLogger.changeScenes(FXMLFile);
            }
        } else {
            FlightLog.setCurrentFlightLog(newCurrentLog);
            FlightLogger.changeScenes(FXMLFile);
        }
    }

    // Shortcut for adding a scene
    public static void addScene(String FXMLFile, String title) {
        try {
            FlightLogger.addSceneHandler(FXMLFile, title);
        } catch (IOException e) {
            System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/main/resources/views/LogIn.fxml"));

        Scene scene = new Scene(root);
        stage = primaryStage;
        stage.setTitle("Flight Logger");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    // Getters
    public static Stage getStage() { return stage; }
    public static Stage getModalStage() { return modalStage; }

    // Main Method
    public static void main(String[] args) {
        Database.connect();
        //developerMode("Ryan", "FlightLogs");
        launch(args);
        Database.disconnect();
    }
}
