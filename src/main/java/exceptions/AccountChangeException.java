//*******************************************************************
// AccountChangeException
//
// A custom Runtime exception that fires when there is a problem changing account properties.
//*******************************************************************

package main.java.exceptions;

public class AccountChangeException extends RuntimeException{

    public AccountChangeException(String message) {
        super(message);
    }
}
