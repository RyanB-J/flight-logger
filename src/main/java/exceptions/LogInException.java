//*******************************************************************
// LogInException
//
// A custom Runtime exception that fires when there is a problem logging in.
//*******************************************************************

package main.java.exceptions;

public class LogInException extends RuntimeException {

    public LogInException(String message) {
        super(message);
    }
}
