//*******************************************************************
// SelectionValidationException
//
// A custom Runtime exception that fires when an invalid selection is made.
//*******************************************************************

package main.java.exceptions;

public class SelectionValidationException extends RuntimeException {

    public SelectionValidationException(String message) {
        super(message);
    }
}
