//*******************************************************************
// FieldValidationException
//
// A custom Runtime exception that fires when one or more fields are invalid.
//*******************************************************************

package main.java.exceptions;

public class FieldValidationException extends RuntimeException {

    public FieldValidationException(String message) {
        super(message);
    }
}
