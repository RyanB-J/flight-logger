//*******************************************************************
// RegistrationException
//
// A custom Runtime exception that fires when there is a problem registering.
//*******************************************************************

package main.java.exceptions;

public class RegistrationException extends RuntimeException {

    public RegistrationException(String message) {
        super(message);
    }
}
