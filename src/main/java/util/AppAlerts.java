//*******************************************************************
// AppAlerts
//
// Configurations for all pop-up alerts across the app
//*******************************************************************

package main.java.util;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;


public class AppAlerts {

    // Flight Logger Success Alert
    public static void throwSuccessAlert(String contentText) {
        Alert successAlert = new Alert(Alert.AlertType.INFORMATION);
        successAlert.setTitle("Flight Logger");
        successAlert.setHeaderText("Success");
        successAlert.setContentText(contentText + "\n");
        successAlert.showAndWait();
    }

    // Registration Alert
    public static void throwRegistrationAlert(String contentText) {
        Alert registrationAlert = new Alert(Alert.AlertType.ERROR);
        registrationAlert.setTitle("Registration Error");
        registrationAlert.setHeaderText("Registration Failed");
        registrationAlert.setContentText(contentText + "\n");
        ExceptionManager.throwRegistrationException("A problem registering an account has occurred.");
        registrationAlert.showAndWait();
    }

    // Log In Alert
    public static void throwLogInAlert(String contentText) {
        Alert loginAlert = new Alert(Alert.AlertType.ERROR);
        loginAlert.setTitle("Log In Error");
        loginAlert.setHeaderText("Log In Failed");
        loginAlert.setContentText(contentText + "\n");
        ExceptionManager.throwRegistrationException("A problem logging into an account has occurred.");
        loginAlert.showAndWait();
    }

    // Account Change Alert
    public static void throwAccountChangeAlert(String contentText) {
        Alert accountAlert = new Alert(Alert.AlertType.ERROR);
        accountAlert.setTitle("Account Change Error");
        accountAlert.setHeaderText("Account Change Failed");
        accountAlert.setContentText(contentText + "\n");
        ExceptionManager.throwRegistrationException("A problem changing an account has occurred.");
        accountAlert.showAndWait();
    }

    // Field Validation Alert
    public static void throwFieldValidationAlert(String contentText) {
        Alert fieldValidationAlert = new Alert(Alert.AlertType.ERROR);
        fieldValidationAlert.setTitle("Field Validation Error");
        fieldValidationAlert.setHeaderText("Invalid Input");
        fieldValidationAlert.setContentText(contentText);
        ExceptionManager.throwFieldValidationException("One or more fields for submission have invalid data.");
        fieldValidationAlert.showAndWait();
    }

    // Selection Validation Alert
    public static void throwSelectionValidationAlert(String contentText) {
        Alert selectionValidationAlert = new Alert(Alert.AlertType.ERROR);
        selectionValidationAlert.setTitle("Selection Validation Error");
        selectionValidationAlert.setHeaderText("Invalid Selection");
        selectionValidationAlert.setContentText(contentText);
        ExceptionManager.throwSelectionValidationException("A selection control has been configured with an invalid selection.");
        selectionValidationAlert.showAndWait();
    }

    // Bypassable Field Validation Alert (obsolete)
    public static ButtonType throwBypassableFieldValidationAlert(String contentText) {
        Alert bypassableFieldValidationAlert = new Alert(Alert.AlertType.WARNING);
        bypassableFieldValidationAlert.setTitle("Field Validation Warning");
        bypassableFieldValidationAlert.setHeaderText("Invalid Input");
        bypassableFieldValidationAlert.setContentText(contentText);
        bypassableFieldValidationAlert.getButtonTypes().setAll(ButtonType.OK, ButtonType.CANCEL);
        bypassableFieldValidationAlert.showAndWait();
        return bypassableFieldValidationAlert.getResult();
    }

    // Delete Confirmation Alert
    public static ButtonType throwDeleteConfirmationAlert(String headerText) {
        Alert deleteConfirmationAlert = new Alert(Alert.AlertType.WARNING);
        deleteConfirmationAlert.setTitle("Delete Confirmation Warning");
        deleteConfirmationAlert.setHeaderText(headerText);
        deleteConfirmationAlert.setContentText("Are you sure? This action can not be undone.\n");
        deleteConfirmationAlert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        deleteConfirmationAlert.showAndWait();
        return deleteConfirmationAlert.getResult();
    }

    // Cancel Progress Confirmation Alert
    public static ButtonType throwFlightLogProgressWarning() {
        Alert flightLogProgressAlert = new Alert(Alert.AlertType.WARNING);
        flightLogProgressAlert.setTitle("Flight Logger");
        flightLogProgressAlert.setHeaderText("Are you sure?");
        flightLogProgressAlert.setContentText("You have unfinished changes to a Flight Log. These changes will not be saved.\n");
        flightLogProgressAlert.getButtonTypes().setAll(ButtonType.CANCEL, ButtonType.OK);
        flightLogProgressAlert.showAndWait();
        return flightLogProgressAlert.getResult();
    }

    // Save Progress Confirmation Alert
    public static ButtonType throwSaveChangesConfirmationAlert() {
        Alert saveConfirmationAlert = new Alert(Alert.AlertType.CONFIRMATION);
        saveConfirmationAlert.setTitle("Flight Logger");
        saveConfirmationAlert.setHeaderText("Save Changes?");
        saveConfirmationAlert.setContentText("Changes will be saved as long as the app remains open.\n");
        saveConfirmationAlert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
        saveConfirmationAlert.showAndWait();
        return saveConfirmationAlert.getResult();
    }

    // Resume Progress Confirmation Alert
    public static ButtonType throwResumeChangesConfirmationAlert() {
        Alert resumeConfirmationAlert = new Alert(Alert.AlertType.CONFIRMATION);
        resumeConfirmationAlert.setTitle("Flight Logger");
        resumeConfirmationAlert.setHeaderText("Resume Progress?");
        resumeConfirmationAlert.setContentText("Continue where you left off before closing out of the flight logging process.\n");
        resumeConfirmationAlert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
        resumeConfirmationAlert.showAndWait();
        return resumeConfirmationAlert.getResult();
    }
}
