//*******************************************************************
// FileManager
//
// Methods for interacting with files
//*******************************************************************

package main.java.util;

import javafx.stage.FileChooser;
import main.java.FlightLogger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class FileManager {

    // File Extensions
    public enum EXT {
        CSV, JSON
    }
    private static final FileChooser.ExtensionFilter csv = new FileChooser.ExtensionFilter("Comma-Separated Values File", ".csv");
    private static final FileChooser.ExtensionFilter json = new FileChooser.ExtensionFilter("JavaScript Object Notation File", ".json");

    public static void saveFile(String content, EXT extension, String initialName) {

        // Save the file with the OS native file chooser
        FileChooser fileChooser = new FileChooser();
        switch (extension) {
            case CSV:
                fileChooser.getExtensionFilters().add(csv);
                break;
            case JSON:
                fileChooser.getExtensionFilters().add(json);
                break;
        }

        // Initial Name
        if (initialName != null) {
            fileChooser.setInitialFileName(initialName);
        }

        // Show save file dialog
        File file = fileChooser.showSaveDialog(FlightLogger.getStage());

        if (file != null) {
            try {
                PrintWriter writer = new PrintWriter(file);
                writer.println(content);
                writer.close();
            } catch (IOException e) {
                System.out.println(Helper.changeColor(e.getMessage(), Helper.Color.RED));
            }
        }

    }

}
