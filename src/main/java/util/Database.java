//*******************************************************************
// Database
//
// Controls the connection and disconnection to the database for all database operations.
//*******************************************************************

package main.java.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

    // Database connection parameters
    private static final String DBURL = "jdbc:mariadb://ryanbj.com:3306/";
    private static final String DBDATABASE = "flight_logger";
    private static final String DBUSERNAME = "pilot";
    private static final String DBPASSWORD = "airplane1234";
    private static final String DBDRIVER = "org.mariadb.jdbc.Driver";

    private static Connection conn = null;

    // Create a connection to the database
    public static Connection connect() {
        try {
            Class.forName(DBDRIVER);
            conn = DriverManager.getConnection(DBURL + DBDATABASE, DBUSERNAME, DBPASSWORD);
            System.out.println(Helper.changeColor("Database connection successful", Helper.Color.GREEN) + "\n");

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return conn;
    }

    // Close the connection to the database
    public static void disconnect() {
        try {
            conn.close();
            System.out.println(Helper.changeColor("Database connection closed", Helper.Color.GREEN));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Return database connection
    public static Connection getConnection() {
        return conn;
    }
}
