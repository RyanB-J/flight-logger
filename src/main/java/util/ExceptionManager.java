//*******************************************************************
// ExceptionManager
//
// Methods for throwing custom Application specific main.java.exceptions
//*******************************************************************

package main.java.util;

import main.java.exceptions.*;

public class ExceptionManager {

    public static void throwLogInException(String message) {
        try {
            throw new LogInException(message);
        } catch (LogInException e) {
            System.out.println(Helper.changeColor(e.getMessage(), Helper.Color.RED));
        }
    }

    public static void throwRegistrationException(String message) {
        try {
            throw new RegistrationException(message);
        } catch (RegistrationException e) {
            System.out.println(Helper.changeColor(e.getMessage(), Helper.Color.RED));
        }
    }

    public static void throwAccountChangeException(String message) {
        try {
            throw new AccountChangeException(message);
        } catch (AccountChangeException e) {
            System.out.println(Helper.changeColor(e.getMessage(), Helper.Color.RED));
        }
    }

    public static void throwFieldValidationException(String message) {
        try {
            throw new FieldValidationException(message);
        } catch (FieldValidationException e) {
            System.out.println(Helper.changeColor(e.getMessage(), Helper.Color.RED));
        }
    }

    public static void throwSelectionValidationException(String message) {
        try {
            throw new SelectionValidationException(message);
        } catch (SelectionValidationException e) {
            System.out.println(Helper.changeColor(e.getMessage(), Helper.Color.RED));
        }
    }
}
