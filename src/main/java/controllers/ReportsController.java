//*******************************************************************
// ReportsController
//
// Controls the interactions for Reports.fxml.
// This is used to view and generate files of accumulated flight log data based on the user.
//*******************************************************************

package main.java.controllers;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import main.java.FlightLogger;
import main.java.models.FlightLog;
import main.java.models.User;
import main.java.util.AppAlerts;
import main.java.util.FileManager;
import main.java.util.Helper;

import java.net.URL;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.ResourceBundle;

public class ReportsController implements Initializable {

    // Components
    @FXML private ComboBox<String> pilotComboBox;
    @FXML private Label logsNumberLabel;
    @FXML private Label dayTakeOffsLabel;
    @FXML private Label dayLandingsLabel;
    @FXML private Label nightTakeOffsLabel;
    @FXML private Label nightLandingsLabel;
    @FXML private Label instrumentLandingsLabel;
    @FXML private Label totalFlightTimeHoursLabel;
    @FXML private Label totalFlightTimeMinutesLabel;
    @FXML private Label pilotNumberLabel;
    @FXML private Label flightInstructorNumberLabel;
    @FXML private Label groundInstructorNumberLabel;
    @FXML private Label simulatorNumberLabel;
    @FXML private Label picNumberLabel;
    @FXML private Label spicNumberLabel;
    @FXML private Label picusNumberLabel;
    @FXML private Label dualNumberLabel;
    @FXML private Label dayTimeFlyingLabel;
    @FXML private Label nightTimeFlyingLabel;
    @FXML private Label groundTimeLabel;
    @FXML private Label crossCountryLabel;
    @FXML private Label instructionTimeLabel;
    @FXML private Label ifrFlightTimeLabel;
    @FXML private Label hoodTimeLabel;
    @FXML private Label simulatorTimeLabel;
    @FXML private Label totalTimeLabel;
    @FXML private Label highestEngineNumberLabel;
    @FXML private TextArea engineClassesTextArea;
    @FXML private ListView<String> columnsListView;
    
    // Flight Logs Table
    @FXML private TableView<FlightLog> flightLogsTableView;
    @FXML private TableColumn<FlightLog, LocalDate> dateTableColumn;
    @FXML private TableColumn<FlightLog, Integer> flightNumberTableColumn;
    @FXML private TableColumn<FlightLog, String> fromTableColumn;
    @FXML private TableColumn<FlightLog, String> toTableColumn;
    @FXML private TableColumn<FlightLog, String> pilotTableColumn;
    @FXML private TableColumn<FlightLog, String> statusTableColumn;

    // State Variables
    private static final ObservableList<FlightLog> userFlightLogs = FXCollections.observableArrayList();
    private static final ObservableList<String> includedColumns = FXCollections.observableArrayList(
            "Date", "Flight Number", "Aircraft Identification", "Departure Airport", "Departure Runway",
            "Departure Time", "Arrival Airport", "Arrival Runway", "Arrival Time", "Aircraft Make and Model",
            "Aircraft Engine Number and Class", "Aircraft ICAO Code", "Day Take Offs and Landings",
            "Night Take Offs and Landings", "Instrument Landings", "Instrument Approaches", "Pilot Type and Status",
            "Day Flight Time", "Night Flight Time", "Ground Time", "Cross Country Flight Time", "Instruction Time",
            "IFR Flight Time", "Under the Hood Flight Time", "Simulator Time", "Wind Heading and Speed", "Weather"
    );

    // Set Field Values
    private void setFieldValues(User user) {

        // Counter Variables
        int totalHours = 0, totalMinutes = 0;
        int numberOfLogs = 0, dayTimeTakeOffs = 0, dayTimeLandings = 0, nightTimeTakeOffs = 0, nightTimeLandings = 0,
                instrumentLandings = 0, pilotNumber = 0, flightInstructorNumber = 0,
                groundInstructorNumber = 0, simulatorNumber = 0, picNumber = 0, spicNumber = 0, picusNumber = 0,
                dualNumber = 0, dayTimeHours = 0, dayTimeMinutes = 0, nightTimeHours = 0,
                nightTimeMinutes = 0, groundTimeHours = 0, groundTimeMinutes = 0, crossCountryHours = 0,
                crossCountryMinutes = 0, instructionHours = 0, instructionMinutes = 0, ifrHours = 0,
                ifrMinutes = 0, hoodHours = 0, hoodMinutes = 0, simulatorHours = 0, simulatorMinutes = 0,
                highestEngineNumber = 0;
        String totalFlightTime = "0:00", engineClasses = "";
        boolean piston = false, turboprop = false, jet = false, helicopter = false;

        // Engine Classes Hash Map
        HashMap<String, Boolean> engineClassesHashMap = new HashMap<>();

        // Loop through all logs and increment counter variables
        for (FlightLog flightLog : FlightLog.getFlightLogs(user)) {
            numberOfLogs++;

            // Take Offs and Landings
            dayTimeTakeOffs += flightLog.getDayTakeOffs();
            dayTimeLandings += flightLog.getDayLandings();
            nightTimeTakeOffs += flightLog.getNightTakeOffs();
            nightTimeLandings += flightLog.getNightLandings();
            instrumentLandings += flightLog.getInstrumentLandings();

            // Pilot Types/Statuses
            switch (flightLog.getPilotType()) {
                case "Pilot": pilotNumber++; break;
                case "Flight Instructor": flightInstructorNumber++; break;
                case "Ground Instructor": groundInstructorNumber++; break;
                case "Simulator": simulatorNumber++;
            }
            switch (flightLog.getPilotStatus()) {
                case "Pilot In Command": picNumber++; break;
                case "Student Pilot In Command": spicNumber++; break;
                case "Pilot In Command Under Supervision": picusNumber++; break;
                case "Dual Pilot and Instructor Command": dualNumber++; break;
            }

            // Flight Times
            dayTimeHours += flightLog.getDayHours();
            dayTimeMinutes += flightLog.getDayMinutes();
            nightTimeHours += flightLog.getNightHours();
            nightTimeMinutes += flightLog.getNightMinutes();
            groundTimeHours += flightLog.getGroundHours();
            groundTimeMinutes += flightLog.getGroundMinutes();
            instructionHours += flightLog.getInstructionHours();
            instructionMinutes += flightLog.getInstructionMinutes();
            ifrHours += flightLog.getIfrHours();
            ifrMinutes += flightLog.getIfrMinutes();
            hoodHours += flightLog.getHoodHours();
            hoodMinutes += flightLog.getHoodMinutes();
            simulatorHours += flightLog.getSimulatorHours();
            simulatorMinutes += flightLog.getSimulatorMinutes();

            // Totals
            totalHours += (flightLog.getDayHours() + flightLog.getNightHours() + flightLog.getGroundHours() +
                    flightLog.getCrossCountryHours() + flightLog.getInstructionHours() + flightLog.getIfrHours() +
                    flightLog.getHoodHours() + flightLog.getSimulatorHours());

            totalMinutes += (flightLog.getDayMinutes() + flightLog.getNightMinutes() + flightLog.getGroundMinutes() +
                    flightLog.getCrossCountryMinutes() + flightLog.getInstructionMinutes() + flightLog.getIfrMinutes() +
                    flightLog.getHoodMinutes() + flightLog.getSimulatorMinutes());

            // Aircraft
            if (flightLog.getAircraftEngineNumber() > highestEngineNumber) {
                highestEngineNumber = flightLog.getAircraftEngineNumber();
            }
            switch (flightLog.getAircraftEngineClass()) {
                case "Piston": piston = true; break;
                case "Turboprop": turboprop = true; break;
                case "Jet": jet = true; break;
                case "Helicopter": helicopter = true; break;
            }
        }

        // Populate all of the labels
        totalFlightTime = Helper.calculateTime(totalHours, totalMinutes);
        logsNumberLabel.setText(Integer.toString(numberOfLogs));
        dayTakeOffsLabel.setText(Integer.toString(dayTimeTakeOffs));
        dayLandingsLabel.setText(Integer.toString(dayTimeLandings));
        nightTakeOffsLabel.setText(Integer.toString(nightTimeTakeOffs));
        nightLandingsLabel.setText(Integer.toString(nightTimeLandings));
        instrumentLandingsLabel.setText(Integer.toString(instrumentLandings));
        totalFlightTimeHoursLabel.setText(totalFlightTime.substring(0,totalFlightTime.indexOf(':')));
        totalFlightTimeMinutesLabel.setText(totalFlightTime.substring(totalFlightTime.indexOf(':') + 1));
        pilotNumberLabel.setText(Integer.toString(picNumber));
        flightInstructorNumberLabel.setText(Integer.toString(flightInstructorNumber));
        groundInstructorNumberLabel.setText(Integer.toString(groundInstructorNumber));
        simulatorNumberLabel.setText(Integer.toString(simulatorNumber));
        picNumberLabel.setText(Integer.toString(picNumber));
        spicNumberLabel.setText(Integer.toString(spicNumber));
        picusNumberLabel.setText(Integer.toString(picusNumber));
        dualNumberLabel.setText(Integer.toString(dualNumber));
        dayTimeFlyingLabel.setText(Helper.calculateTime(dayTimeHours, dayTimeMinutes));
        nightTimeFlyingLabel.setText(Helper.calculateTime(nightTimeHours, nightTimeMinutes));
        groundTimeLabel.setText(Helper.calculateTime(groundTimeHours, groundTimeMinutes));
        crossCountryLabel.setText(Helper.calculateTime(crossCountryHours, crossCountryMinutes));
        instructionTimeLabel.setText(Helper.calculateTime(instructionHours, instructionMinutes));
        ifrFlightTimeLabel.setText(Helper.calculateTime(ifrHours, ifrMinutes));
        hoodTimeLabel.setText(Helper.calculateTime(hoodHours, hoodMinutes));
        simulatorTimeLabel.setText(Helper.calculateTime(simulatorHours, simulatorMinutes));
        totalTimeLabel.setText(totalFlightTime);
        highestEngineNumberLabel.setText(Integer.toString(highestEngineNumber));

        // Populate Engine Classes Text Area
        engineClassesHashMap.put("Piston", piston);
        engineClassesHashMap.put("Turboprop", turboprop);
        engineClassesHashMap.put("Jet", jet);
        engineClassesHashMap.put("Helicopter", helicopter);
        engineClassesTextArea.setText(Helper.booleansToString(engineClassesHashMap));
    }

    // Navigation Controls
    @FXML private void goToFlightLogs(ActionEvent event) { FlightLogger.changeScenes("FlightLogs"); }
    @FXML private void goToAircraft(ActionEvent event) { FlightLogger.changeScenes("Aircraft"); }
    @FXML private void goToAirports(ActionEvent event) { FlightLogger.changeScenes("Airports"); }
    @FXML private void goToAccount(ActionEvent event) { FlightLogger.changeScenes("Account"); }
    @FXML private void logOut(ActionEvent event) { FlightLogger.changeScenes("LogIn"); }

    // Change the Pilot
    @FXML private void changePilot(ActionEvent event) {
        setFieldValues(User.getUserByName(pilotComboBox.getValue()));
        userFlightLogs.setAll(FlightLog.getFlightLogs(User.getUserByName(pilotComboBox.getValue())));
        flightLogsTableView.setItems(userFlightLogs);
    }

    // Select All Columns
    @FXML private void selectAllColumns(ActionEvent event) {
        columnsListView.getSelectionModel().selectAll();
    }

    // Deselect All Columns
    @FXML private void deselectAllColumns(ActionEvent event) {
        columnsListView.getSelectionModel().clearSelection();
    }

    // Export As CSV
    @FXML private void generateCSV(ActionEvent event) {

        if (!columnsListView.getSelectionModel().getSelectedItems().isEmpty()) {

            ObservableList<String> selectedItems = columnsListView.getSelectionModel().getSelectedItems();
            StringBuilder fileContent = new StringBuilder();

            // Header Row
            for (String item : selectedItems) {
                switch (item) {
                    case "Aircraft Make and Model":
                        fileContent.append("Aircraft Make,Aircraft Model,");
                        break;
                    case "Aircraft Engine Number and Class":
                        fileContent.append("No. of Engines,Engine Class,");
                        break;
                    case "Day Take Offs and Landings":
                        fileContent.append("Day Take Offs,Day Landings,");
                        break;
                    case "Night Take Offs and Landings":
                        fileContent.append("Night Take Offs,Night Landings,");
                        break;
                    case "Pilot Type and Status":
                        fileContent.append("Pilot Type,Pilot Status,");
                        break;
                    case "Wind Heading and Speed":
                        fileContent.append("Wind Heading,Wind Speed,");
                        break;
                    default:
                        fileContent.append(item).append(",");
                }
            }
            fileContent.append("\n");

            // Data
            for (FlightLog flightLog : userFlightLogs) {
                if (selectedItems.contains("Date")) fileContent.append(flightLog.getDate().toString()).append(",");
                if (selectedItems.contains("Flight Number"))
                    fileContent.append(flightLog.getFlightNumber()).append(",");
                if (selectedItems.contains("Aircraft Identification"))
                    fileContent.append(flightLog.getAircraftIdent()).append(",");
                if (selectedItems.contains("Departure Airport"))
                    fileContent.append(flightLog.getDepartureAirport()).append(",");
                if (selectedItems.contains("Departure Runway"))
                    fileContent.append(flightLog.getDepartureRunway()).append(",");
                if (selectedItems.contains("Departure Time"))
                    fileContent.append(flightLog.getDepartureTime()).append(",");
                if (selectedItems.contains("Arrival Airport"))
                    fileContent.append(flightLog.getArrivalAirport()).append(",");
                if (selectedItems.contains("Arrival Runway"))
                    fileContent.append(flightLog.getArrivalRunway()).append(",");
                if (selectedItems.contains("Arrival Time")) fileContent.append(flightLog.getArrivalTime()).append(",");
                if (selectedItems.contains("Aircraft Make and Model")) {
                    fileContent.append(flightLog.getAircraftMake()).append(",");
                    fileContent.append(flightLog.getAircraftModel()).append(",");
                }
                if (selectedItems.contains("Aircraft Engine Number and Class")) {
                    fileContent.append(flightLog.getAircraftEngineNumber()).append(",");
                    fileContent.append(flightLog.getAircraftEngineClass()).append(",");
                }
                if (selectedItems.contains("Aircraft ICAO Code"))
                    fileContent.append(flightLog.getAircraftICAOCode()).append(",");
                if (selectedItems.contains("Day Take Offs and Landings")) {
                    fileContent.append(flightLog.getDayTakeOffs()).append(",");
                    fileContent.append(flightLog.getDayLandings()).append(",");
                }
                if (selectedItems.contains("Night Take Offs and Landings")) {
                    fileContent.append(flightLog.getNightTakeOffs()).append(",");
                    fileContent.append(flightLog.getNightLandings()).append(",");
                }
                if (selectedItems.contains("Instrument Landings"))
                    fileContent.append(flightLog.getInstrumentLandings()).append(",");
                if (selectedItems.contains("Instrument Approaches"))
                    fileContent.append(flightLog.getInstrumentApproaches()).append(",");
                if (selectedItems.contains("Pilot Type and Status")) {
                    fileContent.append(flightLog.getPilotType()).append(",");
                    fileContent.append(flightLog.getPilotStatus()).append(",");
                }
                if (selectedItems.contains("Day Flight Time")) fileContent.append(
                        Helper.calculateTime(flightLog.getDayHours(), flightLog.getDayMinutes())).append(",");
                if (selectedItems.contains("Night Flight Time")) fileContent.append(
                        Helper.calculateTime(flightLog.getNightHours(), flightLog.getNightMinutes())).append(",");
                if (selectedItems.contains("Ground Time")) fileContent.append(
                        Helper.calculateTime(flightLog.getGroundHours(), flightLog.getGroundMinutes())).append(",");
                if (selectedItems.contains("Cross Country Flight Time")) fileContent.append(
                        Helper.calculateTime(flightLog.getCrossCountryHours(), flightLog.getCrossCountryMinutes())).append(",");
                if (selectedItems.contains("Instruction Time")) fileContent.append(
                        Helper.calculateTime(flightLog.getInstructionHours(), flightLog.getInstructionMinutes())).append(",");
                if (selectedItems.contains("IFR Flight Time")) fileContent.append(
                        Helper.calculateTime(flightLog.getIfrHours(), flightLog.getIfrMinutes())).append(",");
                if (selectedItems.contains("Under the Hood Flight Time")) fileContent.append(
                        Helper.calculateTime(flightLog.getHoodHours(), flightLog.getHoodMinutes())).append(",");
                if (selectedItems.contains("Simulator Time")) fileContent.append(
                        Helper.calculateTime(flightLog.getSimulatorHours(), flightLog.getSimulatorMinutes())).append(",");
                if (selectedItems.contains("Wind Heading and Speed")) {
                    fileContent.append(flightLog.getWindHeading()).append(",");
                    fileContent.append(flightLog.getWindSpeed()).append(",");
                }
                if (selectedItems.contains("Weather")) fileContent.append(flightLog.getWeather().replaceAll(",", "")).append(",");
                fileContent.append("\n");
            }

            FileManager.saveFile(fileContent.toString(), FileManager.EXT.CSV, pilotComboBox.getValue().toLowerCase() + "-flightlog");

        } else {
            AppAlerts.throwSelectionValidationAlert("You must include at least one column.");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Initialize ComboBoxes
        if (FlightLogger.getCurrentUser().isInstructor()) {
            pilotComboBox.setItems(User.getNonInstructorUsernames(FlightLogger.getCurrentUser()));
            pilotComboBox.setValue(FlightLogger.getCurrentUser().getUsername());
        } else {
            pilotComboBox.setValue(FlightLogger.getCurrentUser().getUsername());
            pilotComboBox.setDisable(true);
        }

        // Initialize the values
        setFieldValues(User.getUserByName(pilotComboBox.getValue()));

        // Initialize ListViews
        columnsListView.setItems(includedColumns);
        columnsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        // Initialize the table
        dateTableColumn.setCellValueFactory(new PropertyValueFactory<FlightLog, LocalDate>("date"));
        flightNumberTableColumn.setCellValueFactory(new PropertyValueFactory<FlightLog, Integer>("flightNumber"));
        fromTableColumn.setCellValueFactory(cellData -> Bindings.concat(
                cellData.getValue().getDepartureAirport().substring(0, cellData.getValue().getDepartureAirport().indexOf(' ')).toUpperCase()
        ));
        toTableColumn.setCellValueFactory(cellData -> Bindings.concat(
                cellData.getValue().getArrivalAirport().substring(0, cellData.getValue().getArrivalAirport().indexOf(' ')).toUpperCase()
        ));
        pilotTableColumn.setCellValueFactory(cellData -> {
            String data = cellData.getValue().getPilotType();
            return Bindings.createStringBinding(
                    () -> {
                        String abbreviation = "";
                        if (data.equals("Flight Instructor")) {
                            abbreviation = "F. Instructor";
                        } else if (data.equals("Ground Instructor")) {
                            abbreviation = "G. Instructor";
                        } else {
                            abbreviation = data;
                        }
                        return abbreviation;
                    }
            );
        });
        statusTableColumn.setCellValueFactory(cellData -> {
            String data = cellData.getValue().getPilotStatus();
            return Bindings.createStringBinding(
                    () -> {
                        String abbreviation = "";
                        switch (data) {
                            case "Pilot In Command":
                                abbreviation = "PIC";
                                break;
                            case "Student Pilot In Command":
                                abbreviation = "SPIC";
                                break;
                            case "Pilot In Command Under Supervision":
                                abbreviation = "PICUS";
                                break;
                            case "Dual Pilot and Instructor Command":
                                abbreviation = "DUAL";
                        }
                        return abbreviation;
                    }
            );
        });
        userFlightLogs.setAll(FlightLog.getFlightLogs(User.getUserByName(pilotComboBox.getValue())));
        flightLogsTableView.setItems(userFlightLogs);
    }
}
