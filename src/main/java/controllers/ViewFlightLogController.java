//*******************************************************************
// ViewFlightLogController
//
// Controls the interactions for ViewFlightLog.fxml.
// This is used to view all of the properties of an existing flight log.
//*******************************************************************

package main.java.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import main.java.FlightLogger;
import main.java.models.FlightLog;
import main.java.util.Helper;

import java.net.URL;
import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ResourceBundle;

public class ViewFlightLogController implements Initializable {

    // Components
    @FXML private Label dateLabel;
    @FXML private Label flightNumberLabel;
    @FXML private Label identifierLabel;
    @FXML private Label departureAirportCodeLabel;
    @FXML private Label departureAirportLabel;
    @FXML private Label departureRunwayLabel;
    @FXML private Label departureTimeLabel;
    @FXML private Label arrivalAirportCodeLabel;
    @FXML private Label arrivalAirportLabel;
    @FXML private Label arrivalRunwayLabel;
    @FXML private Label arrivalTimeLabel;
    @FXML private Label aircraftLabel;
    @FXML private Label engineNumberLabel;
    @FXML private Label engineClassLabel;
    @FXML private Label icaoLabel;
    @FXML private Label dayTakeOffsLabel;
    @FXML private Label dayLandingsLabel;
    @FXML private Label nightTakeOffsLabel;
    @FXML private Label nightLandingsLabel;
    @FXML private Label instrumentLandingsLabel;
    @FXML private TextArea instrumentApproachesTextArea;
    @FXML private Label pilotTypeLabel;
    @FXML private Label pilotStatusLabel;
    @FXML private Label dayTimeLabel;
    @FXML private Label nightTimeLabel;
    @FXML private Label groundTimeLabel;
    @FXML private Label crossCountryLabel;
    @FXML private Label instructionLabel;
    @FXML private Label ifrTimeLabel;
    @FXML private Label hoodTimeLabel;
    @FXML private Label simulatorTimeLabel;
    @FXML private Label totalTimeLabel;
    @FXML private Label windHeadingLabel;
    @FXML private Label windSpeedLabel;
    @FXML private TextArea weatherTextArea;

    // Selected Flight Log
    private static FlightLog selectedFlightLog;

    // Formats
    private final DecimalFormat twoZeroFormat = new DecimalFormat("00");
    private final DecimalFormat threeZeroFormat = new DecimalFormat("000");

    // Get total time
    private String totalTime() {
        int totalHours = selectedFlightLog.getDayHours() + selectedFlightLog.getNightHours() + selectedFlightLog.getGroundHours() +
                selectedFlightLog.getCrossCountryHours() + selectedFlightLog.getInstructionHours() + selectedFlightLog.getIfrHours() +
                selectedFlightLog.getHoodHours() + selectedFlightLog.getSimulatorHours();

        int totalMinutes = (selectedFlightLog.getDayMinutes() + selectedFlightLog.getNightMinutes() + selectedFlightLog.getGroundMinutes() +
                selectedFlightLog.getCrossCountryMinutes() + selectedFlightLog.getInstructionMinutes() + selectedFlightLog.getIfrMinutes() +
                selectedFlightLog.getHoodMinutes() + selectedFlightLog.getSimulatorMinutes());

        return Helper.calculateTime(totalHours, totalMinutes);
    }

    // Set Field Values
    private void setFieldValues() {
        dateLabel.setText(selectedFlightLog.getDate().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)));
        flightNumberLabel.setText(Integer.toString(selectedFlightLog.getFlightNumber()));
        identifierLabel.setText(selectedFlightLog.getAircraftIdent().toUpperCase());
        departureAirportCodeLabel.setText(selectedFlightLog.getDepartureAirport().substring(0, selectedFlightLog.getDepartureAirport().indexOf(' ')).toUpperCase());
        departureAirportLabel.setText(
                selectedFlightLog.getDepartureAirport().charAt(4) == ' '
                        ? selectedFlightLog.getDepartureAirport().substring(5)
                        : selectedFlightLog.getDepartureAirport().substring(6)
        );
        departureRunwayLabel.setText(selectedFlightLog.getDepartureRunway());
        departureTimeLabel.setText(selectedFlightLog.getDepartureTime().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
        arrivalAirportCodeLabel.setText(selectedFlightLog.getArrivalAirport().substring(0, selectedFlightLog.getArrivalAirport().indexOf(' ')).toUpperCase());
        arrivalAirportLabel.setText(
                selectedFlightLog.getArrivalAirport().charAt(4) == ' '
                        ? selectedFlightLog.getArrivalAirport().substring(5)
                        : selectedFlightLog.getArrivalAirport().substring(6)
        );
        arrivalRunwayLabel.setText(selectedFlightLog.getArrivalRunway());
        arrivalTimeLabel.setText(selectedFlightLog.getArrivalTime().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
        aircraftLabel.setText(selectedFlightLog.getAircraftMake() + selectedFlightLog.getAircraftModel());
        engineNumberLabel.setText(Integer.toString(selectedFlightLog.getAircraftEngineNumber()));
        engineClassLabel.setText(selectedFlightLog.getAircraftEngineClass());
        icaoLabel.setText(selectedFlightLog.getAircraftICAOCode());
        dayTakeOffsLabel.setText(Integer.toString(selectedFlightLog.getDayTakeOffs()));
        dayLandingsLabel.setText(Integer.toString(selectedFlightLog.getDayLandings()));
        nightTakeOffsLabel.setText(Integer.toString(selectedFlightLog.getNightTakeOffs()));
        nightLandingsLabel.setText(Integer.toString(selectedFlightLog.getNightLandings()));
        instrumentLandingsLabel.setText(Integer.toString(selectedFlightLog.getInstrumentLandings()));
        instrumentApproachesTextArea.setText(selectedFlightLog.getInstrumentApproaches());
        pilotTypeLabel.setText(selectedFlightLog.getPilotType());
        pilotStatusLabel.setText(selectedFlightLog.getPilotStatus());
        dayTimeLabel.setText(Helper.calculateTime(selectedFlightLog.getDayHours(), selectedFlightLog.getDayMinutes()));
        nightTimeLabel.setText(Helper.calculateTime(selectedFlightLog.getNightHours(), selectedFlightLog.getNightMinutes()));
        groundTimeLabel.setText(Helper.calculateTime(selectedFlightLog.getGroundHours(), selectedFlightLog.getGroundMinutes()));
        crossCountryLabel.setText(Helper.calculateTime(selectedFlightLog.getCrossCountryHours(), selectedFlightLog.getCrossCountryMinutes()));
        instructionLabel.setText(Helper.calculateTime(selectedFlightLog.getInstructionHours(), selectedFlightLog.getInstructionMinutes()));
        ifrTimeLabel.setText(Helper.calculateTime(selectedFlightLog.getIfrHours(), selectedFlightLog.getIfrMinutes()));
        hoodTimeLabel.setText(Helper.calculateTime(selectedFlightLog.getHoodHours(), selectedFlightLog.getHoodMinutes()));
        simulatorTimeLabel.setText(Helper.calculateTime(selectedFlightLog.getSimulatorHours(), selectedFlightLog.getSimulatorMinutes()));
        totalTimeLabel.setText(totalTime());
        windHeadingLabel.setText(threeZeroFormat.format(selectedFlightLog.getWindHeading()));
        windSpeedLabel.setText(Integer.toString(selectedFlightLog.getWindSpeed()));
        weatherTextArea.setText(selectedFlightLog.getWeather());
    }

    // Navigation Controls
    @FXML private void goToFlightLogs(ActionEvent event) { FlightLogger.changeScenes("FlightLogs"); }
    @FXML private void goToReports(ActionEvent event) { FlightLogger.changeScenes("Reports"); }
    @FXML private void goToAirports(ActionEvent event) { FlightLogger.changeScenes("Airports"); }
    @FXML private void goToAircraft(ActionEvent event) { FlightLogger.changeScenes("Aircraft"); }
    @FXML private void goToAccount(ActionEvent event) { FlightLogger.changeScenes("Account"); }
    @FXML private void logOut(ActionEvent event) { FlightLogger.changeScenes("LogIn"); }

    // Cancel the Form
    @FXML private void cancelForm(ActionEvent event) { FlightLogger.changeScenes("FlightLogs"); }

    // Edit the Flight Log
    @FXML private void editFlightLog(ActionEvent event) { FlightLogger.changeScenesWithConfirmation("LogFlight-1", selectedFlightLog);}

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Selected FLight Log
        selectedFlightLog = FlightLogsController.getSelectedFlightLog();
        setFieldValues();

    }
}
