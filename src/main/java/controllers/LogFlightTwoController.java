//*******************************************************************
// LogFlightTwoController
//
// Controls the interactions for LogFlight-2.fxml.
// This is the second screen for creating or editing a flight log.
//*******************************************************************

package main.java.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import main.java.FlightLogger;
import main.java.middleware.FieldFormatter;
import main.java.middleware.FieldValidator;
import main.java.models.Field;
import main.java.models.FlightLog;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

public class LogFlightTwoController implements Initializable {

    // Components
    @FXML private TextField dayTakeOffsTextField;
    @FXML private TextField dayLandingsTextField;
    @FXML private TextField nightTakeOffsTextField;
    @FXML private TextField nightLandingsTextField;
    @FXML private TextField instrumentLandingsTextField;
    @FXML private TextField instrumentApproachesTextField;

    // List of fields for main.java.middleware
    private Field dayTakeOffsField, dayLandingsField, nightTakeOffsField, nightLandingsField, instrumentLandingsField,
            instrumentApproachesField;
    private final ArrayList<Field> fields = new ArrayList<>();

    // Populate the fields if values exist
    private void populateFields() {
        FlightLog log = FlightLog.getCurrentFlightLog();
        if (log.getDayTakeOffs() != 0) dayTakeOffsTextField.setText(Integer.toString(log.getDayTakeOffs()));
        if (log.getDayLandings() != 0) dayLandingsTextField.setText(Integer.toString(log.getDayLandings()));
        if (log.getNightTakeOffs() != 0) nightTakeOffsTextField.setText(Integer.toString(log.getNightTakeOffs()));
        if (log.getNightLandings() != 0) nightLandingsTextField.setText(Integer.toString(log.getNightLandings()));
        if (log.getInstrumentLandings() != 0) instrumentLandingsTextField.setText(Integer.toString(log.getInstrumentLandings()));
        if (log.getInstrumentApproaches() != null) instrumentApproachesTextField.setText(log.getInstrumentApproaches());
    }

    // Navigation Controls
    @FXML private void goToFlightLogs(ActionEvent event) { FlightLogger.changeScenesWithSave("FlightLogs"); }
    @FXML private void goToReports(ActionEvent event) { FlightLogger.changeScenesWithSave("Reports"); }
    @FXML private void goToAirports(ActionEvent event) { FlightLogger.changeScenesWithSave("Airports"); }
    @FXML private void goToAircraft(ActionEvent event) { FlightLogger.changeScenesWithSave("Aircraft"); }
    @FXML private void goToAccount(ActionEvent event) { FlightLogger.changeScenesWithSave("Account"); }
    @FXML private void logOut(ActionEvent event) { FlightLogger.changeScenesWithConfirmation("LogIn", null); }

    // Go to the previous scene
    @FXML private void goToPreviousScene(ActionEvent event) { FlightLogger.changeScenes("LogFlight-1");}

    // Go to the next scene
    @FXML private void goToNextScene(ActionEvent event) {
        if (FieldValidator.validate(fields)) {
            FlightLog flightLog = FlightLog.getCurrentFlightLog();
            flightLog.setDayTakeOffs(!dayTakeOffsField.getValue().equals("") ? Integer.parseInt(dayTakeOffsField.getValue()) : 0);
            flightLog.setDayLandings(!dayLandingsField.getValue().equals("") ? Integer.parseInt(dayLandingsField.getValue()) : 0);
            flightLog.setNightTakeOffs(!nightTakeOffsField.getValue().equals("") ? Integer.parseInt(nightTakeOffsField.getValue()) : 0);
            flightLog.setNightLandings(!nightLandingsField.getValue().equals("") ? Integer.parseInt(nightLandingsField.getValue()) : 0);
            flightLog.setInstrumentLandings(!instrumentLandingsField.getValue().equals("") ? Integer.parseInt(instrumentLandingsField.getValue()) : 0);
            flightLog.setInstrumentApproaches(instrumentApproachesField.getValue().toUpperCase());
            FlightLogger.changeScenes("LogFlight-3");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Initialize the fields
        populateFields();
        dayTakeOffsField = new Field("Daytime Take Offs", dayTakeOffsTextField, null, "number", 2, false);
        dayLandingsField = new Field("Daytime Landings", dayLandingsTextField, null, "number", 2, false);
        nightTakeOffsField = new Field("Nighttime Take Offs", nightTakeOffsTextField, null, "number", 2, false);
        nightLandingsField = new Field("Nighttime Landings", nightLandingsTextField, null, "number", 2, false);
        instrumentLandingsField = new Field("Instrument Landings", instrumentLandingsTextField, null, "number", 2, false);
        instrumentApproachesField = new Field("Instrument Approaches", instrumentApproachesTextField, null, "general", 256, false);

        // Initialize the list of fields for validation
        fields.addAll(Arrays.asList(dayTakeOffsField, dayLandingsField, nightTakeOffsField, nightLandingsField,
                instrumentLandingsField, instrumentApproachesField));

        // Add field restrictions and formatting
        FieldFormatter.restrictFields(fields);
    }
}
