//*******************************************************************
// AircraftController
//
// Controls the interactions for Aircraft.fxml.
// This is used to view and select all Aircraft in the database for inspection and manipulation.
//*******************************************************************

package main.java.controllers;

import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import main.java.FlightLogger;
import main.java.models.Aircraft;
import main.java.util.AppAlerts;
import main.java.util.Helper;

import java.net.URL;
import java.util.ResourceBundle;

public class AircraftController implements Initializable {

    // Components
    @FXML private CheckBox customAircraftCheckBox;
    @FXML private TextField searchTextField;
    @FXML private Button searchButton;
    @FXML private Button deleteButton;
    @FXML private Button viewEditButton;
    
    // Aircraft Table
    @FXML private TableView<Aircraft> aircraftTableView;
    @FXML private TableColumn<Aircraft, String> manufacturerTableColumn;
    @FXML private TableColumn<Aircraft, String> modelTableColumn;
    @FXML private TableColumn<Aircraft, Integer> engineNumberTableColumn;
    @FXML private TableColumn<Aircraft, String> engineClassTableColumn;
    @FXML private TableColumn<Aircraft, String> wakeCategoryTableColumn;
    @FXML private TableColumn<Aircraft, String> icaoCodeTableColumn;

    // State Variables
    private static Aircraft selectedAircraft;
    private static int selectedAircraftIndex;
    private static String initializeAlert = "";
    private static ObservableList<Aircraft> aircraftTableItems = FXCollections.observableArrayList();

    // Getters & Setters
    public static Aircraft getSelectedAircraft() {
        return selectedAircraft;
    }
    public static void setInitializeAlert(String initializeAlert) {
        AircraftController.initializeAlert = initializeAlert;
    }

    // Refresh Screen
    public static void refreshScreen() {
        FlightLogger.changeScenes("Aircraft");
    }

    // Search TextField KeyPressed Listener
    @FXML private void searchTextFieldKeyPressed(KeyEvent event) {
        if (event.getCode().toString().equals("ENTER")) {
            searchButton.fire();
        }
    }

    // Navigation Controls
    @FXML private void goToFlightLogs(ActionEvent event) { FlightLogger.changeScenes("FlightLogs"); }
    @FXML private void goToReports(ActionEvent event) { FlightLogger.changeScenes("Reports"); }
    @FXML private void goToAirports(ActionEvent event) { FlightLogger.changeScenes("Airports"); }
    @FXML private void goToAccount(ActionEvent event) { FlightLogger.changeScenes("Account"); }
    @FXML private void logOut(ActionEvent event) { FlightLogger.changeScenes("LogIn"); }

    // Filter the Aircraft Table
    @FXML private void filterAircraft(ActionEvent event) {
        viewEditButton.setDisable(true);
        deleteButton.setDisable(true);
        aircraftTableItems.setAll(Aircraft.getAircraft(null, 100, customAircraftCheckBox.isSelected()));
        aircraftTableView.setItems(aircraftTableItems);
    }

    // Search for Aircraft
    @FXML private void searchAircraft(ActionEvent event) {
        aircraftTableItems.setAll(Aircraft.getAircraft(searchTextField.getText(), 100, customAircraftCheckBox.isSelected()));
        aircraftTableView.setItems(aircraftTableItems);
    }

    // Delete an Aircraft
    @FXML private void deleteAircraft(ActionEvent event) {
        if (AppAlerts.throwDeleteConfirmationAlert("Delete Aircraft") == ButtonType.YES) {
            Aircraft.deleteAircraft(selectedAircraft.getId());
            aircraftTableItems.remove(selectedAircraftIndex);
        }
    }
    
    // View or Edit an Aircraft
    @FXML private void viewEditAircraft(ActionEvent event) {

        // Determine if the form is editable
        if (selectedAircraft.isCustom()) {
            AircraftSingleController.setSceneMode(AircraftSingleController.Mode.EDIT);
        } else {
            AircraftSingleController.setSceneMode(AircraftSingleController.Mode.VIEW);
        }

        // Go to Aircraft Single
        FlightLogger.addScene("AircraftSingle", "Aircraft");
    }

    // Add an Aircraft
    @FXML private void addAircraft(ActionEvent event) {
        AircraftSingleController.setSceneMode(AircraftSingleController.Mode.ADD);
        FlightLogger.addScene("AircraftSingle", "Aircraft");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Throw alert if one is present
        if (!initializeAlert.isEmpty()) {
            AppAlerts.throwSuccessAlert(initializeAlert);
            initializeAlert = "";
        }

        // Initialize the buttons
        deleteButton.setVisible(false);
        viewEditButton.setDisable(true);

        // Initialize the table
        manufacturerTableColumn.setCellValueFactory(cellData -> Bindings.createStringBinding(
                () -> Helper.toTitleCase(cellData.getValue().getMake())
        ));
        modelTableColumn.setCellValueFactory(cellData -> Bindings.createStringBinding(
                () -> Helper.toTitleCase(cellData.getValue().getModel())
        ));
        engineNumberTableColumn.setCellValueFactory(new PropertyValueFactory<Aircraft, Integer>("engineNumber"));
        engineClassTableColumn.setCellValueFactory(cellData -> Bindings.createStringBinding(
                () -> Helper.toTitleCase(cellData.getValue().getEngineClass())
        ));
        wakeCategoryTableColumn.setCellValueFactory(new PropertyValueFactory<Aircraft, String>("wakeCategory"));
        icaoCodeTableColumn.setCellValueFactory(new PropertyValueFactory<Aircraft, String>("icaoCode"));
        aircraftTableItems = Aircraft.getAircraft(null, 100, false);
        aircraftTableView.setItems(aircraftTableItems);

        // Aircraft Table Selection Listener
        aircraftTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Aircraft>() {
            @Override
            public void changed(ObservableValue<? extends Aircraft> observable, Aircraft oldValue, Aircraft newValue) {
                if (aircraftTableView.getSelectionModel().getSelectedItem() != null) {
                    selectedAircraft = newValue;
                    selectedAircraftIndex = aircraftTableView.getSelectionModel().getSelectedIndex();
                    if (newValue.isCustom()) {
                        viewEditButton.setText("Edit");
                        deleteButton.setDisable(false);
                        deleteButton.setVisible(true);
                    } else {
                        viewEditButton.setText("View");
                        deleteButton.setVisible(false);
                    }
                    viewEditButton.setDisable(false);
                }
            }
        });
    }
    

    

    

    


}
