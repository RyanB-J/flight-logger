//*******************************************************************
// LogFlightThreeController
//
// Controls the interactions for LogFlight-3.fxml.
// This is the third screen for creating or editing a flight log.
//*******************************************************************

package main.java.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import main.java.FlightLogger;
import main.java.middleware.FieldFormatter;
import main.java.middleware.FieldValidator;
import main.java.models.Field;
import main.java.models.FlightLog;
import main.java.util.AppAlerts;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;


public class LogFlightThreeController implements Initializable {

    // Components
    @FXML private ComboBox<String> pilotTypeComboBox;
    @FXML private ComboBox<String> pilotStatusComboBox;
    @FXML private TextField dayTimeHoursTextField;
    @FXML private TextField dayTimeMinutesTextField;
    @FXML private TextField nightTimeHoursTextField;
    @FXML private TextField nightTimeMinutesTextField;
    @FXML private TextField groundTimeHoursTextField;
    @FXML private TextField groundTimeMinutesTextField;
    @FXML private TextField crossCountryHoursTextField;
    @FXML private TextField crossCountryMinutesTextField;
    @FXML private TextField instructionTimeHoursTextField;
    @FXML private TextField instructionTimeMinutesTextField;
    @FXML private TextField ifrHoursTextField;
    @FXML private TextField ifrMinutesTextField;
    @FXML private TextField underHoodHoursTextField;
    @FXML private TextField underHoodMinutesTextField;
    @FXML private TextField simulatorHoursTextField;
    @FXML private TextField simulatorMinutesTextField;

    // List of fields for main.java.middleware
    private Field dayTimeHoursField, dayTimeMinutesField, nightTimeHoursField, nightTimeMinutesField, groundTimeHoursField,
    groundTimeMinutesField, crossCountryHoursField, crossCountryMinutesField, instructionTimeHoursField,
    instructionTimeMinutesField, ifrHoursField, ifrMinutesField, underHoodHoursField, underHoodMinutesField,
    simulatorHoursField, simulatorMinutesField;
    private final ArrayList<Field> fields = new ArrayList<>();
    private final ArrayList<ComboBox<String>> comboBoxes = new ArrayList<>();

    // Populate the fields is values exist
    private void populateFields() {
        FlightLog log = FlightLog.getCurrentFlightLog();
        if (log.getPilotType() != null) pilotTypeComboBox.setValue(log.getPilotType());
        if (log.getPilotStatus() != null) pilotStatusComboBox.setValue(log.getPilotStatus());
        if (log.getDayHours() != 0) dayTimeHoursTextField.setText(Integer.toString(log.getDayHours()));
        if (log.getDayMinutes() != 0) dayTimeMinutesTextField.setText(Integer.toString(log.getDayMinutes()));
        if (log.getNightHours() != 0) nightTimeHoursTextField.setText(Integer.toString(log.getNightHours()));
        if (log.getNightMinutes() != 0) nightTimeMinutesTextField.setText(Integer.toString(log.getNightMinutes()));
        if (log.getGroundHours() != 0) groundTimeHoursTextField.setText(Integer.toString(log.getGroundHours()));
        if (log.getGroundMinutes() != 0) groundTimeMinutesTextField.setText(Integer.toString(log.getGroundMinutes()));
        if (log.getCrossCountryHours() != 0) crossCountryHoursTextField.setText(Integer.toString(log.getCrossCountryHours()));
        if (log.getCrossCountryMinutes() != 0) crossCountryMinutesTextField.setText(Integer.toString(log.getCrossCountryMinutes()));
        if (log.getInstructionHours() != 0) instructionTimeHoursTextField.setText(Integer.toString(log.getInstructionHours()));
        if (log.getInstructionMinutes() != 0) instructionTimeMinutesTextField.setText(Integer.toString(log.getInstructionMinutes()));
        if (log.getIfrHours() != 0) ifrHoursTextField.setText(Integer.toString(log.getIfrHours()));
        if (log.getIfrMinutes() != 0) ifrMinutesTextField.setText(Integer.toString(log.getIfrMinutes()));
        if (log.getHoodHours() != 0) underHoodHoursTextField.setText(Integer.toString(log.getHoodHours()));
        if (log.getHoodMinutes() != 0) underHoodMinutesTextField.setText(Integer.toString(log.getHoodMinutes()));
        if (log.getSimulatorHours() != 0) simulatorHoursTextField.setText(Integer.toString(log.getSimulatorHours()));
        if (log.getSimulatorMinutes() != 0) simulatorMinutesTextField.setText(Integer.toString(log.getSimulatorMinutes()));
    }

    // Validate the fields
    private boolean validateInput() {

        boolean validated = true;

        // Update objects from the text fields
        for (Field field : fields) {
            field.setValue(field.getTextField().getText());
        }

        StringBuilder errorMessage = new StringBuilder();

        // If text fields are validated
        errorMessage.append(FieldValidator.validateText(fields));

        // If combo boxes are validated
        errorMessage.append(FieldValidator.requireComboBoxes(comboBoxes));

        // If the errorMessage is not empty
        if (!errorMessage.toString().isEmpty()) {
            validated = false;
            AppAlerts.throwFieldValidationAlert(errorMessage.toString());
        }

        return validated;
    }

    // Navigation Controls
    @FXML private void goToFlightLogs(ActionEvent event) { FlightLogger.changeScenesWithSave("FlightLogs"); }
    @FXML private void goToReports(ActionEvent event) { FlightLogger.changeScenesWithSave("Reports"); }
    @FXML private void goToAirports(ActionEvent event) { FlightLogger.changeScenesWithSave("Airports"); }
    @FXML private void goToAircraft(ActionEvent event) { FlightLogger.changeScenesWithSave("Aircraft"); }
    @FXML private void goToAccount(ActionEvent event) { FlightLogger.changeScenesWithSave("Account"); }
    @FXML private void logOut(ActionEvent event) { FlightLogger.changeScenesWithConfirmation("LogIn", null); }

    // Go to the previous scene
    @FXML private void goToPreviousScene(ActionEvent event) { FlightLogger.changeScenes("LogFlight-2");}
    
    // Go to the next scene
    @FXML private void goToNextScene(ActionEvent event) {
        if (validateInput()) {
            FlightLog flightLog = FlightLog.getCurrentFlightLog();
            flightLog.setPilotType(pilotTypeComboBox.getValue());
            flightLog.setPilotStatus(pilotStatusComboBox.getValue());
            flightLog.setDayHours(!dayTimeHoursField.getValue().equals("") ? Integer.parseInt(dayTimeHoursField.getValue()) : 0);
            flightLog.setDayMinutes(!dayTimeMinutesField.getValue().equals("") ? Integer.parseInt(dayTimeMinutesField.getValue()) : 0);
            flightLog.setNightHours(!nightTimeHoursField.getValue().equals("") ? Integer.parseInt(nightTimeHoursField.getValue()) : 0);
            flightLog.setNightMinutes(!nightTimeMinutesField.getValue().equals("") ? Integer.parseInt(nightTimeMinutesField.getValue()) : 0);
            flightLog.setGroundHours(!groundTimeHoursField.getValue().equals("") ? Integer.parseInt(groundTimeHoursField.getValue()) : 0);
            flightLog.setGroundMinutes(!groundTimeMinutesField.getValue().equals("") ? Integer.parseInt(groundTimeMinutesField.getValue()) : 0);
            flightLog.setCrossCountryHours(!crossCountryHoursField.getValue().equals("") ? Integer.parseInt(crossCountryHoursField.getValue()) : 0);
            flightLog.setCrossCountryMinutes(!crossCountryMinutesField.getValue().equals("") ? Integer.parseInt(crossCountryMinutesField.getValue()) : 0);
            flightLog.setInstructionHours(!instructionTimeHoursField.getValue().equals("") ? Integer.parseInt(instructionTimeHoursField.getValue()) : 0);
            flightLog.setInstructionMinutes(!instructionTimeMinutesField.getValue().equals("") ? Integer.parseInt(instructionTimeMinutesField.getValue()) : 0);
            flightLog.setIfrHours(!ifrHoursField.getValue().equals("") ? Integer.parseInt(ifrHoursField.getValue()) : 0);
            flightLog.setIfrMinutes(!ifrMinutesField.getValue().equals("") ? Integer.parseInt(ifrMinutesField.getValue()) : 0);
            flightLog.setHoodHours(!underHoodHoursField.getValue().equals("") ? Integer.parseInt(underHoodHoursField.getValue()) : 0);
            flightLog.setHoodMinutes(!underHoodMinutesField.getValue().equals("") ? Integer.parseInt(underHoodMinutesField.getValue()) : 0);
            flightLog.setSimulatorHours(!simulatorHoursField.getValue().equals("") ? Integer.parseInt(simulatorHoursField.getValue()) : 0);
            flightLog.setSimulatorMinutes(!simulatorMinutesField.getValue().equals("") ? Integer.parseInt(simulatorMinutesField.getValue()) : 0);
            FlightLogger.changeScenes("LogFlight-4");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Initialize the fields
        populateFields();
        dayTimeHoursField = new Field("Day Time Flying Hours", dayTimeHoursTextField, null, "integer", 2, false);
        dayTimeMinutesField = new Field("Day Time Flying Minutes", dayTimeMinutesTextField, null, "integer", 2, false);
        nightTimeHoursField = new Field("Night Time Flying Hours", nightTimeHoursTextField, null, "integer", 2, false);
        nightTimeMinutesField = new Field("Night Time Flying Minutes", nightTimeMinutesTextField, null, "integer", 2, false);
        groundTimeHoursField = new Field("Ground Time Hours", groundTimeHoursTextField, null, "integer", 2, false);
        groundTimeMinutesField = new Field("Ground Time Minutes", groundTimeMinutesTextField, null, "integer", 2, false);
        crossCountryHoursField = new Field("Cross Country Hours", crossCountryHoursTextField, null, "integer", 2, false);
        crossCountryMinutesField = new Field("Cross Country Minutes", crossCountryMinutesTextField, null, "integer", 2, false);
        instructionTimeHoursField = new Field("Instruction Time Hours", instructionTimeHoursTextField, null, "integer", 2, false);
        instructionTimeMinutesField = new Field("Instruction Time Minutes", instructionTimeMinutesTextField, null, "integer", 2, false);
        ifrHoursField = new Field("IFR Flight Time Hours", ifrHoursTextField, null, "integer", 2, false);
        ifrMinutesField = new Field("IFR Flight Time Minutes", ifrMinutesTextField, null, "integer", 2, false);
        underHoodHoursField = new Field("Under the Hood Hours", underHoodHoursTextField, null, "integer", 2, false);
        underHoodMinutesField = new Field("Under the Hood Minutes", underHoodMinutesTextField, null, "integer", 2, false);
        simulatorHoursField = new Field("Simulator Hours", simulatorHoursTextField, null, "integer", 2, false);
        simulatorMinutesField = new Field("Simulator Minutes", simulatorMinutesTextField, null, "integer", 2, false);

        // Initialize the list of fields for validation
        fields.addAll(Arrays.asList(dayTimeHoursField, dayTimeMinutesField, nightTimeHoursField, nightTimeMinutesField,
                groundTimeHoursField, groundTimeMinutesField, crossCountryHoursField, crossCountryMinutesField,
                instructionTimeHoursField, instructionTimeMinutesField, ifrHoursField, ifrMinutesField,
                underHoodHoursField, underHoodMinutesField, simulatorHoursField, simulatorMinutesField));
        comboBoxes.addAll(Arrays.asList(pilotTypeComboBox, pilotStatusComboBox));

        // Add field restrictions and formatting
        FieldFormatter.restrictFields(fields);

        // Configure Combo Boxes
        pilotTypeComboBox.setItems(FlightLog.pilotTypes);
        pilotStatusComboBox.setItems(FlightLog.pilotStatuses);

    }
}


