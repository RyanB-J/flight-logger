//*******************************************************************
// LogInController
//
// Controls the interactions for LogIn.fxml.
// This is used log into the application as a specific user.
//*******************************************************************

package main.java.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import main.java.FlightLogger;
import main.java.middleware.FieldFormatter;
import main.java.middleware.FieldValidator;
import main.java.middleware.SHAHasher;
import main.java.models.Field;
import main.java.models.PassField;
import main.java.models.User;
import main.java.util.AppAlerts;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class LogInController implements Initializable {

    // Components
    @FXML private TextField usernameTextField;
    @FXML private PasswordField passwordPassField;
    @FXML private Button loginButton;

    // Create objects from the text fields
    private Field usernameField;
    private PassField passwordField;

    // List of fields for main.java.middleware
    private final ArrayList<Field> fields = new ArrayList<>();
    private final ArrayList<PassField> passFields = new ArrayList<>();

    // Validation
    private boolean validateInput() {

        boolean validated = true;

        // Create objects from the text fields
        usernameField.setValue(usernameTextField.getText());
        passwordField.setValue(passwordPassField.getText());

        StringBuilder errorMessage = new StringBuilder();

        // If fields are validated
        errorMessage.append(FieldValidator.validateText(fields));

        // Hash the password
        String hashedPassword = SHAHasher.hash(passwordPassField.getText());
        passwordField.setValue(hashedPassword);

        // If the username and password are authentic
        errorMessage.append(FieldValidator.authenticateUser(usernameField.getValue(), passwordField.getValue()));

        // If the errorMessage is not empty
        if (!errorMessage.toString().isEmpty()) {
            validated = false;
            AppAlerts.throwLogInAlert(errorMessage.toString());
        }

        return validated;
    }

    @FXML private void passwordKeyPressed(KeyEvent event) {
        if (event.getCode().toString().equals("ENTER")) {
            loginButton.fire();
        }
    }

    // Log In
    @FXML private void logIn(ActionEvent event) {

        if (validateInput()) {

            // Log In
            FlightLogger.setCurrentUser(User.getUserByName(usernameField.getValue()));

            // Verify a successful Login
            if (FlightLogger.getCurrentUser() != null) {

                // Go to FlightLogs
                FlightLogger.changeScenes("FlightLogs");
            } else {
                AppAlerts.throwLogInAlert("A problem has occurred during the login process.");
            }
        }
    }

    // Go to Register view
    @FXML private void goToRegister(ActionEvent event) { FlightLogger.changeScenes("Register"); }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Initialize the fields
        usernameField = new Field("Username", usernameTextField, null, "name", 10, true);
        passwordField = new PassField("Password", passwordPassField, null, 50);

        // Initialize the lists of fields
        fields.add(usernameField);
        passFields.add(passwordField);

        // Add field length restrictions
        FieldFormatter.restrictFields(fields);
        FieldFormatter.restrictPasswords(passFields);

        // Change Button to disabled if username field is empty
        usernameTextField.textProperty().addListener((ov, oldValue, newValue) -> {

            // Set Button Inactive if no value is present
            if (usernameTextField.getText().length() < 1) {
                loginButton.getStyleClass().remove("button-primary");
                loginButton.getStyleClass().add("button-disabled");
            } else {
                loginButton.getStyleClass().remove("button-disabled");
                loginButton.getStyleClass().add("button-primary");
            }

        });
    }
}
