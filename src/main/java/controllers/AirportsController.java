//*******************************************************************
// AirportsController
//
// Controls the interactions for Airports.fxml.
// This is used to view and select all Airports in the database for inspection and manipulation.
//*******************************************************************

package main.java.controllers;

import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import main.java.FlightLogger;
import main.java.models.Airport;
import main.java.util.AppAlerts;
import main.java.util.Helper;

import java.net.URL;
import java.util.ResourceBundle;

public class AirportsController implements Initializable {

    // Components
    @FXML private CheckBox customAirportCheckBox;
    @FXML private TextField searchTextField;
    @FXML private Button searchButton;
    @FXML private Button deleteButton;
    @FXML private Button viewEditButton;

    // Airport Table
    @FXML private TableView<Airport> airportTableView;
    @FXML private TableColumn<Airport, String> typeTableColumn;
    @FXML private TableColumn<Airport, String> locIdTableColumn;
    @FXML private TableColumn<Airport, String> nameTableColumn;
    @FXML private TableColumn<Airport, String> stateTableColumn;
    @FXML private TableColumn<Airport, String> cityTableColumn;
    @FXML private TableColumn<Airport, String> latitudeTableColumn;
    @FXML private TableColumn<Airport, String> longitudeTableColumn;

    // State Variables
    private static Airport selectedAirport;
    private static int selectedAirportIndex;
    private static String initializeAlert = "";
    private static ObservableList<Airport> airportTableItems = FXCollections.observableArrayList();

    // Getters & Setters
    public static Airport getSelectedAirport() {
        return selectedAirport;
    }
    public static void setSelectedAirport(Airport selectedAirport) {
        AirportsController.selectedAirport = selectedAirport;
    }
    public static void setInitializeAlert(String initializeAlert) {
        AirportsController.initializeAlert = initializeAlert;
    }

    // Search TextField KeyPressed Listener
    @FXML private void searchTextFieldKeyPressed(KeyEvent event) {
        if (event.getCode().toString().equals("ENTER")) {
            searchButton.fire();
        }
    }

    // Navigation Controls
    @FXML private void goToFlightLogs(ActionEvent event) { FlightLogger.changeScenes("FlightLogs"); }
    @FXML private void goToReports(ActionEvent event) { FlightLogger.changeScenes("Reports"); }
    @FXML private void goToAircraft(ActionEvent event) { FlightLogger.changeScenes("Aircraft"); }
    @FXML private void goToAccount(ActionEvent event) { FlightLogger.changeScenes("Account"); }
    @FXML private void logOut(ActionEvent event) { FlightLogger.changeScenes("LogIn"); }

    // Filter the Airport Table
    @FXML private void filterAirports(ActionEvent event) {
        viewEditButton.setDisable(true);
        deleteButton.setDisable(true);
        airportTableItems.setAll(Airport.getAirports(null, 100, (customAirportCheckBox.isSelected())));
        airportTableView.setItems(airportTableItems);
    }

    // Search for Aircraft
    @FXML private void searchAirports(ActionEvent event) {
        airportTableItems.setAll(Airport.getAirports(searchTextField.getText(), 100, (customAirportCheckBox.isSelected())));
        airportTableView.setItems(airportTableItems);
    }

    // Delete an Airport
    @FXML private void deleteAirport(ActionEvent event) {
        if (AppAlerts.throwDeleteConfirmationAlert("Delete Airport") == ButtonType.YES) {
            Airport.deleteAirport(selectedAirport.getId(), selectedAirport.getRelationalId());
            airportTableItems.remove(selectedAirportIndex);
        }
    }

    // View or Edit an Airport
    @FXML private void viewEditAirport(ActionEvent event) {

        // Determine if the form is editable
        if (selectedAirport.isCustom()) {
            AirportSingleController.setSceneMode(AirportSingleController.Mode.EDIT);
        } else {
            AirportSingleController.setSceneMode(AirportSingleController.Mode.VIEW);
        }

        // Go to Airport Single
        FlightLogger.changeScenes("AirportSingle");
    }

    // Add an Airport
    @FXML private void addAirport(ActionEvent event) {
        AirportSingleController.setSceneMode(AirportSingleController.Mode.ADD);
        FlightLogger.changeScenes("AirportSingle");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Throw alert if one is present
        if (!initializeAlert.isEmpty()) {
            AppAlerts.throwSuccessAlert(initializeAlert);
            initializeAlert = "";
        }

        // Initialize the buttons
        deleteButton.setVisible(false);
        viewEditButton.setDisable(true);

        // Initialize the table
        typeTableColumn.setCellValueFactory(cellData -> Bindings.createStringBinding(
                () -> Helper.toTitleCase(cellData.getValue().getType())
        ));
        locIdTableColumn.setCellValueFactory(new PropertyValueFactory<Airport, String>("locId"));
        nameTableColumn.setCellValueFactory(cellData -> Bindings.createStringBinding(
                () -> Helper.toTitleCase(cellData.getValue().getName())
        ));
        stateTableColumn.setCellValueFactory(cellData -> Bindings.createStringBinding(
                () -> Helper.toTitleCase(cellData.getValue().getState())
        ));
        cityTableColumn.setCellValueFactory(cellData -> Bindings.createStringBinding(
                () -> Helper.toTitleCase(cellData.getValue().getCity())
        ));
        latitudeTableColumn.setCellValueFactory(new PropertyValueFactory<Airport, String>("latitude"));
        longitudeTableColumn.setCellValueFactory(new PropertyValueFactory<Airport, String>("longitude"));
        airportTableItems = Airport.getAirports(null, 100, false);
        airportTableView.setItems(airportTableItems);

        // Aircraft Table Selection Listener
        airportTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Airport>() {
            @Override
            public void changed(ObservableValue<? extends Airport> observable, Airport oldValue, Airport newValue) {
                if (airportTableView.getSelectionModel().getSelectedItem() != null) {
                    setSelectedAirport(newValue);
                    selectedAirportIndex = airportTableView.getSelectionModel().getSelectedIndex();
                    if (newValue.isCustom()) {
                        viewEditButton.setText("Edit");
                        deleteButton.setDisable(false);
                        deleteButton.setVisible(true);
                    } else {
                        viewEditButton.setText("View");
                        deleteButton.setVisible(false);
                    }
                    viewEditButton.setDisable(false);
                }
            }
        });
    }
    




}
