//*******************************************************************
// AccountController
//
// Controls the interactions for Account.fxml.
// This is used to change existing account properties such as username, email, and password.
//*******************************************************************

package main.java.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import main.java.FlightLogger;
import main.java.middleware.FieldFormatter;
import main.java.middleware.FieldValidator;
import main.java.middleware.SHAHasher;
import main.java.models.Field;
import main.java.models.PassField;
import main.java.models.User;
import main.java.util.AppAlerts;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

public class AccountController implements Initializable {

    // Components
    @FXML private TextField usernameTextField;
    @FXML private TextField emailTextField;
    @FXML private PasswordField currentPasswordPassField;
    @FXML private PasswordField newPasswordPassField;
    @FXML private PasswordField confirmPasswordPassField;
    @FXML private CheckBox instructorCheckBox;

    // Current User
    private final User currentUser = FlightLogger.getCurrentUser();

    // Create objects from the text fields
    private Field usernameField, emailField;
    private PassField currentPasswordField, newPasswordField, confirmPasswordField;

    // List of fields for main.java.middleware
    private final ArrayList<Field> fields = new ArrayList<>();
    private final ArrayList<PassField> passFields = new ArrayList<>();

    // State Variables
    private boolean passwordChanged = false;

    // Navigation Controls
    @FXML private void goToFlightLogs(ActionEvent event) { FlightLogger.changeScenes("FlightLogs"); }
    @FXML private void goToReports(ActionEvent event) { FlightLogger.changeScenes("Reports"); }
    @FXML private void goToAircraft(ActionEvent event) { FlightLogger.changeScenes("Aircraft"); }
    @FXML private void goToAirports(ActionEvent event) { FlightLogger.changeScenes("Airports"); }
    @FXML private void logOut(ActionEvent event) { FlightLogger.changeScenes("LogIn"); }

    private boolean validateInput() {

        boolean validated = true;

        // Update objects from the text fields
        usernameField.setValue(usernameTextField.getText());
        emailField.setValue(emailTextField.getText());
        currentPasswordField.setValue(currentPasswordPassField.getText());
        newPasswordField.setValue(confirmPasswordPassField.getText());
        confirmPasswordField.setValue(confirmPasswordPassField.getText());

        StringBuilder errorMessage = new StringBuilder();

        // If text fields are validated
        errorMessage.append(FieldValidator.validateText(fields));

        // If any password field has a value
        if (!currentPasswordField.getValue().isEmpty() && newPasswordField.getValue().isEmpty() &&
                confirmPasswordField.getValue().isEmpty()) {

            passwordChanged = true;

            // Hash the current password
            String hashedPassword = SHAHasher.hash(currentPasswordField.getValue());
            currentPasswordField.setValue(hashedPassword);

            // If the username and password are authentic
            errorMessage.append(FieldValidator.authenticateUser(currentUser.getUsername(),
                    currentPasswordField.getValue()));

            // If the password field(s) are validated
            errorMessage.append(FieldValidator.validatePasswords(passFields));

            // If the passwords match
            errorMessage.append(FieldValidator.passwordMatch(newPasswordField.getValue(),
                    confirmPasswordField.getValue()));
        }

        // If the username field was changed
        if (!usernameTextField.getText().equals(usernameField.getValue())) {

            // If username is unique
            errorMessage.append(FieldValidator.uniqueUsername(usernameField.getValue()));
        }

        // If the errorMessage is not empty
        if (!errorMessage.toString().isEmpty()) {
            validated = false;
            AppAlerts.throwAccountChangeAlert(errorMessage.toString());
        }

        return validated;
    }

    // Save Changes
    @FXML private void saveChanges(ActionEvent event) {

        if (validateInput()) {

            if (passwordChanged) {

                // Hash the password
                String hashedPassword = SHAHasher.hash(newPasswordField.getValue());
                newPasswordField.setValue(hashedPassword);
            }

            // Update the currently logged in user
            FlightLogger.setCurrentUser(
                    User.updateUser(
                            currentUser,
                            usernameField.getValue(),
                            (passwordChanged ? newPasswordField.getValue() : currentUser.getPassword()),
                            emailField.getValue(),
                            instructorCheckBox.isSelected()
                    )
            );

            // Verify a successful Account Change
            if (FlightLogger.getCurrentUser() != null) {

                // Go to FlightLogs
                FlightLogsController.setInitializeAlert("Your account has been updated.");
                FlightLogger.changeScenes("FlightLogs");
            } else {
                AppAlerts.throwAccountChangeAlert("A problem has occurred during the registration process.");
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Initialize the fields
        usernameField = new Field("Username", usernameTextField, currentUser.getUsername(), "name", 24, true);
        emailField = new Field("Email", emailTextField, currentUser.getEmail(), "email", 48, true);
        currentPasswordField = new PassField("Current Password", currentPasswordPassField, null, 48);
        newPasswordField = new PassField("Password", newPasswordPassField, null, 48);
        confirmPasswordField = new PassField("Password", confirmPasswordPassField, null, 48);

        // Initialize the lists of fields
        fields.addAll(Arrays.asList(usernameField, emailField));
        passFields.add(newPasswordField);

        // Add field length restrictions
        FieldFormatter.restrictFields(fields);
        FieldFormatter.restrictPasswords(passFields);

        // Set the fields to the current user
        usernameTextField.setText(usernameField.getValue());
        emailTextField.setText(emailField.getValue());
        if (currentUser.isInstructor()) instructorCheckBox.fire();
    }
}
