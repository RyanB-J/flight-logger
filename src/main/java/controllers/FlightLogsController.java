//*******************************************************************
// FlightLogsController
//
// Controls the interactions for FlightLogs.fxml.
// This is used to view and select all Flight Logs in the database for inspection and manipulation.
//*******************************************************************

package main.java.controllers;

import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import main.java.FlightLogger;
import main.java.models.FlightLog;
import main.java.models.User;
import main.java.util.AppAlerts;

import java.net.URL;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class FlightLogsController implements Initializable {

    // Components
    @FXML private ComboBox<String> pilotComboBox;
    @FXML private Button viewButton;
    @FXML private Button deleteButton;
    @FXML private Button editButton;
    @FXML private Button addButton;

    // Log Table
    @FXML private TableView<FlightLog> logTableView;
    @FXML private TableColumn<FlightLog, LocalDate> dateTableColumn;
    @FXML private TableColumn<FlightLog, Integer> flightNumberTableColumn;
    @FXML private TableColumn<FlightLog, String> aircraftTableColumn;
    @FXML private TableColumn<FlightLog, String> fromTableColumn;
    @FXML private TableColumn<FlightLog, String> toTableColumn;
    @FXML private TableColumn<FlightLog, Number> landingsTableColumn;
    @FXML private TableColumn<FlightLog, String> pilotTableColumn;
    @FXML private TableColumn<FlightLog, String> dayTimeTableColumn;
    @FXML private TableColumn<FlightLog, String> nightTimeTableColumn;
    @FXML private TableColumn<FlightLog, String> weatherTableColumn;

    // State Variables
    private static String initializeAlert = "";
    private static FlightLog selectedFlightLog;
    private static int selectedFlightLogIndex;
    private static ObservableList<FlightLog> logTableItems = FXCollections.observableArrayList();

    // Getters & Setters
    public static void setInitializeAlert(String incomingString) { initializeAlert = incomingString; }
    public static FlightLog getSelectedFlightLog() { return selectedFlightLog; }
    public static void setSelectedFlightLog(FlightLog selectedFlightLog) { FlightLogsController.selectedFlightLog = selectedFlightLog; }

    // Formats
    private final DecimalFormat twoZeroFormat = new DecimalFormat("00");

    // Navigation Controls
    @FXML private void goToReports(ActionEvent event) { FlightLogger.changeScenes("Reports"); }
    @FXML private void goToAircraft(ActionEvent event) { FlightLogger.changeScenes("Aircraft"); }
    @FXML private void goToAirports(ActionEvent event) { FlightLogger.changeScenes("Airports"); }
    @FXML private void goToAccount(ActionEvent event) { FlightLogger.changeScenes("Account"); }
    @FXML private void logOut(ActionEvent event) { FlightLogger.changeScenes("LogIn"); }

    // If the Pilot is changed
    @FXML private void pilotChanged(ActionEvent event) {
        logTableItems = FlightLog.getFlightLogs(User.getUserByName(pilotComboBox.getValue()));
        logTableView.setItems(logTableItems);
    }

    // View Log
    @FXML private void viewLog(ActionEvent event) {
        FlightLogger.changeScenes("ViewFlightLog");
    }

    // Delete Log
    @FXML private void deleteLog(ActionEvent event) {
        if (AppAlerts.throwDeleteConfirmationAlert("Delete Flight Log") == ButtonType.YES) {
            FlightLog.deleteFlightLog(selectedFlightLog.getId());
            logTableItems.remove(selectedFlightLogIndex);
        }
    }

    // Edit Log
    @FXML private void editLog(ActionEvent event) { FlightLogger.changeScenesWithConfirmation("LogFlight-1", selectedFlightLog);}

    // Add Log
    @FXML private void addLog(ActionEvent event) {
        if (FlightLog.getCurrentFlightLog() != null) {
            ButtonType alertResult = AppAlerts.throwResumeChangesConfirmationAlert();
            if (alertResult == ButtonType.YES) {
                FlightLog.setCurrentFlightLog(selectedFlightLog);
                selectedFlightLog.setId(0);
                FlightLogger.changeScenes("LogFlight-1");
            }
            else if (alertResult == ButtonType.NO) {
                FlightLog.setCurrentFlightLog(null);
                selectedFlightLog.setId(0);
                FlightLogger.changeScenes("LogFlight-1");
            }
        } else {
            FlightLogger.changeScenes("LogFlight-1");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Throw alert if one is present
        if (!initializeAlert.isEmpty()) {
            AppAlerts.throwSuccessAlert(initializeAlert);
            initializeAlert = "";
        }

        // Initialize ComboBoxes
        if (FlightLogger.getCurrentUser().isInstructor()) {
            pilotComboBox.setItems(User.getNonInstructorUsernames(FlightLogger.getCurrentUser()));
            pilotComboBox.setValue(FlightLogger.getCurrentUser().getUsername());
        } else {
            pilotComboBox.setValue(FlightLogger.getCurrentUser().getUsername());
            pilotComboBox.setDisable(true);
        }
        
        // Initialize the buttons
        viewButton.setDisable(true);
        editButton.setDisable(true);
        deleteButton.setDisable(true);

        // Initialize the table
        dateTableColumn.setCellValueFactory(new PropertyValueFactory<FlightLog, LocalDate>("date"));
        flightNumberTableColumn.setCellValueFactory(new PropertyValueFactory<FlightLog, Integer>("flightNumber"));
        aircraftTableColumn.setCellValueFactory(cellData -> Bindings.concat(
                cellData.getValue().getAircraftMake(), " ", cellData.getValue().getAircraftModel()
        ));
        fromTableColumn.setCellValueFactory(cellData -> Bindings.concat(
                cellData.getValue().getDepartureAirport().substring(0, cellData.getValue().getDepartureAirport().indexOf(' ')).toUpperCase()
        ));
        toTableColumn.setCellValueFactory(cellData -> Bindings.concat(
                cellData.getValue().getArrivalAirport().substring(0, cellData.getValue().getArrivalAirport().indexOf(' ')).toUpperCase()
        ));
        landingsTableColumn.setCellValueFactory(cellData -> {
            FlightLog data = cellData.getValue();
            return Bindings.createIntegerBinding(
                    () -> {
                        try {
                            return data.getDayLandings() + data.getNightLandings() + data.getInstrumentLandings();
                        } catch (NumberFormatException e) {
                            return 0;
                        }
                    }
            );
        });
        pilotTableColumn.setCellValueFactory(cellData -> {
            String data = cellData.getValue().getPilotStatus();
            return Bindings.createStringBinding(
                    () -> {
                        String abbreviation = "";
                        switch (data) {
                            case "Pilot In Command":
                                abbreviation = "PIC";
                                break;
                            case "Student Pilot In Command":
                                abbreviation = "SPIC";
                                break;
                            case "Pilot In Command Under Supervision":
                                abbreviation = "PICUS";
                                break;
                            case "Dual Pilot and Instructor Command":
                                abbreviation = "DUAL";
                        }
                        return abbreviation;
                    }
            );
        });
        dayTimeTableColumn.setCellValueFactory(cellData -> Bindings.concat(
                cellData.getValue().getDayHours(), ":", twoZeroFormat.format(cellData.getValue().getDayMinutes())
        ));
        nightTimeTableColumn.setCellValueFactory(cellData -> Bindings.concat(
                cellData.getValue().getNightHours(), ":", twoZeroFormat.format(cellData.getValue().getNightMinutes())
        ));
        weatherTableColumn.setCellValueFactory(new PropertyValueFactory<FlightLog, String>("weather"));
        logTableItems = FlightLog.getFlightLogs(User.getUserByName(pilotComboBox.getValue()));
        logTableView.setItems(logTableItems);

        // FlightLog Table Selection Listener
        logTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<FlightLog>() {
            @Override
            public void changed(ObservableValue<? extends FlightLog> observable, FlightLog oldValue, FlightLog newValue) {
                if (logTableView.getSelectionModel().getSelectedItem() != null) {
                    selectedFlightLog = newValue;
                    selectedFlightLogIndex = logTableView.getSelectionModel().getSelectedIndex();
                    viewButton.setDisable(false);
                    editButton.setDisable(false);
                    deleteButton.setDisable(false);
                }
            }
        });
    }
}
