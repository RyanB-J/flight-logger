//*******************************************************************
// LogFlightFourController
//
// Controls the interactions for LogFlight-4.fxml.
// This is the fourth and final screen for creating or editing a flight log.
//*******************************************************************

package main.java.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import main.java.FlightLogger;
import main.java.middleware.FieldFormatter;
import main.java.middleware.FieldValidator;
import main.java.models.Field;
import main.java.models.FlightLog;
import main.java.util.Helper;

import java.net.URL;
import java.util.*;

public class LogFlightFourController implements Initializable {

    // Components
    @FXML private TextField windHeadingTextField;
    @FXML private TextField windSpeedTextField;
    @FXML private CheckBox sunnyCheckBox;
    @FXML private CheckBox cloudyCheckBox;
    @FXML private CheckBox rainyCheckBox;
    @FXML private CheckBox stormyCheckBox;
    @FXML private CheckBox hurricaneCheckBox;
    @FXML private CheckBox lightningCheckBox;
    @FXML private CheckBox typhoonCheckBox;
    @FXML private CheckBox blizzardCheckBox;
    @FXML private CheckBox foggyCheckBox;
    @FXML private CheckBox hotCheckBox;
    @FXML private CheckBox coldCheckBox;
    @FXML private CheckBox windyCheckBox;

    // List of fields for main.java.middleware
    private Field windHeadingField, windSpeedField;
    private final ArrayList<Field> fields = new ArrayList<>();
    private final HashMap<String, Boolean> weatherHashMap = new HashMap<>();

    // Populate the fields is values exist
    private void populateFields() {
        FlightLog log = FlightLog.getCurrentFlightLog();
        if (log.getWindHeading() != 0) windHeadingTextField.setText(Integer.toString(log.getWindHeading()));
        if (log.getWindSpeed() != 0) windSpeedTextField.setText(Integer.toString(log.getWindSpeed()));
        if (log.isSunny()) sunnyCheckBox.setSelected(true);
        if (log.isCloudy()) cloudyCheckBox.setSelected(true);
        if (log.isRainy()) rainyCheckBox.setSelected(true);
        if (log.isStormy()) stormyCheckBox.setSelected(true);
        if (log.isHurricane()) hurricaneCheckBox.setSelected(true);
        if (log.isLightning()) lightningCheckBox.setSelected(true);
        if (log.isTyphoon()) typhoonCheckBox.setSelected(true);
        if (log.isBlizzard()) blizzardCheckBox.setSelected(true);
        if (log.isFoggy()) foggyCheckBox.setSelected(true);
        if (log.isHot()) hotCheckBox.setSelected(true);
        if (log.isCold()) coldCheckBox.setSelected(true);
        if (log.isWindy()) windyCheckBox.setSelected(true);
    }

    // Navigation Controls
    @FXML private void goToFlightLogs(ActionEvent event) { FlightLogger.changeScenesWithSave("FlightLogs"); }
    @FXML private void goToReports(ActionEvent event) { FlightLogger.changeScenesWithSave("Reports"); }
    @FXML private void goToAirports(ActionEvent event) { FlightLogger.changeScenesWithSave("Airports"); }
    @FXML private void goToAircraft(ActionEvent event) { FlightLogger.changeScenesWithSave("Aircraft"); }
    @FXML private void goToAccount(ActionEvent event) { FlightLogger.changeScenesWithSave("Account"); }
    @FXML private void logOut(ActionEvent event) { FlightLogger.changeScenesWithConfirmation("LogIn", null); }

    // Go to the previous scene
    @FXML private void goToPreviousScene(ActionEvent event) { FlightLogger.changeScenes("LogFlight-3");}

    // Finish the Flight Log
    @FXML private void finishForm(ActionEvent event) {
        if (FieldValidator.validate(fields)) {
            FlightLog flightLog = FlightLog.getCurrentFlightLog();
            flightLog.setWindHeading(!windHeadingField.getValue().equals("") ? Integer.parseInt(windHeadingField.getValue()) : 0);
            flightLog.setWindSpeed(!windSpeedField.getValue().equals("") ? Integer.parseInt(windSpeedField.getValue()) : 0);
            flightLog.setSunny(sunnyCheckBox.isSelected());
            flightLog.setCloudy(cloudyCheckBox.isSelected());
            flightLog.setRainy(rainyCheckBox.isSelected());
            flightLog.setStormy(stormyCheckBox.isSelected());
            flightLog.setHurricane(hurricaneCheckBox.isSelected());
            flightLog.setLightning(lightningCheckBox.isSelected());
            flightLog.setTyphoon(typhoonCheckBox.isSelected());
            flightLog.setBlizzard(blizzardCheckBox.isSelected());
            flightLog.setFoggy(foggyCheckBox.isSelected());
            flightLog.setHot(hotCheckBox.isSelected());
            flightLog.setCold(coldCheckBox.isSelected());
            flightLog.setWindy(windyCheckBox.isSelected());

            // Weather String
            weatherHashMap.put("Sunny", sunnyCheckBox.isSelected());
            weatherHashMap.put("Cloudy", cloudyCheckBox.isSelected());
            weatherHashMap.put("Rainy", rainyCheckBox.isSelected());
            weatherHashMap.put("Stormy", stormyCheckBox.isSelected());
            weatherHashMap.put("Hurricane", hurricaneCheckBox.isSelected());
            weatherHashMap.put("Lightning", lightningCheckBox.isSelected());
            weatherHashMap.put("Typhoon", typhoonCheckBox.isSelected());
            weatherHashMap.put("Blizzard", blizzardCheckBox.isSelected());
            weatherHashMap.put("Foggy", foggyCheckBox.isSelected());
            weatherHashMap.put("Hot", hotCheckBox.isSelected());
            weatherHashMap.put("Cold", coldCheckBox.isSelected());
            weatherHashMap.put("Windy", windyCheckBox.isSelected());
            flightLog.setWeather(Helper.booleansToString(weatherHashMap));

            // Add the log to the database
            if (FlightLog.getCurrentFlightLog().getId() != 0) {
                if (FlightLog.updateFlightLog(flightLog)) {
                    FlightLog.setCurrentFlightLog(null);
                    FlightLogsController.setInitializeAlert("Log Updated.");
                    FlightLogger.changeScenes("FlightLogs");
                }
            } else {
                if (FlightLog.createFlightLog(flightLog, FlightLogger.getCurrentUser())) {
                    FlightLog.setCurrentFlightLog(null);
                    FlightLogsController.setInitializeAlert("Log Created.");
                    FlightLogger.changeScenes("FlightLogs");
                }
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Initialize the fields
        populateFields();
        windHeadingField = new Field("Wind Heading", windHeadingTextField, null, "heading", 3, false);
        windSpeedField = new Field("Wind Speed", windSpeedTextField, null, "integer", 3, false);

        // Initialize the list of fields for validation
        fields.addAll(Arrays.asList(windHeadingField, windSpeedField));

        // Add field restrictions and formatting
        FieldFormatter.restrictFields(fields);

    }
}
