//*******************************************************************
// RegisterController
//
// Controls the interactions for Register.fxml.
// This is used to create a user account for the application.
//*******************************************************************

package main.java.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import main.java.FlightLogger;
import main.java.middleware.FieldFormatter;
import main.java.middleware.FieldValidator;
import main.java.middleware.SHAHasher;
import main.java.models.Field;
import main.java.models.PassField;
import main.java.models.User;
import main.java.util.AppAlerts;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

public class RegisterController implements Initializable {

    // Components
    @FXML private TextField usernameTextField;
    @FXML private TextField emailTextField;
    @FXML private PasswordField passwordPassField;
    @FXML private PasswordField confirmPassField;
    @FXML private CheckBox instructorCheckBox;
    @FXML private Button registerButton;

    // Create objects from the text fields
    private Field usernameField, emailField;
    private PassField passwordField, confirmField;

    // List of fields for main.java.middleware
    private final ArrayList<Field> fields = new ArrayList<>();
    private final ArrayList<PassField> passFields = new ArrayList<>();

    // Validation
    private boolean validateInput() {

        boolean validated = true;

        // Create objects from the text fields
        usernameField.setValue(usernameTextField.getText());
        emailField.setValue(emailTextField.getText());
        passwordField.setValue(passwordPassField.getText());
        confirmField.setValue(confirmPassField.getText());

        StringBuilder errorMessage = new StringBuilder();

        // If fields are validated
        errorMessage.append(FieldValidator.validateText(fields));

        // If password field(s) are validated
        errorMessage.append(FieldValidator.validatePasswords(passFields));

        // If passwords match
        errorMessage.append(FieldValidator.passwordMatch(passwordField.getValue(), confirmField.getValue()));

        // If username is unique
        errorMessage.append(FieldValidator.uniqueUsername(usernameField.getValue()));

        // If the errorMessage is not empty
        if (!errorMessage.toString().isEmpty()) {
            validated = false;
            AppAlerts.throwRegistrationAlert(errorMessage.toString());
        }

        return validated;
    }

    // Register
    @FXML private void register(ActionEvent event) {

        if (validateInput()) {

            // Hash the password
            String hashedPassword = SHAHasher.hash(passwordField.getValue());
            passwordField.setValue(hashedPassword);

            // Create and Log In as the user
            FlightLogger.setCurrentUser(
                    User.createUser(
                        usernameField.getValue(),
                        passwordField.getValue(),
                        emailField.getValue(),
                        instructorCheckBox.isSelected()
                    )
            );

            // Verify a successful Registration
            if (FlightLogger.getCurrentUser() != null) {

                // Go to FlightLogs
                FlightLogsController.setInitializeAlert("The Registration was successful and you are now logged in.\n" +
                        "Click Add to create a new flight log.");
                FlightLogger.changeScenes("FlightLogs");
            } else {
                AppAlerts.throwRegistrationAlert("A problem has occurred during the registration process.");
            }
        }
    }

    // Go to LogIn
    @FXML private void goToLogIn(ActionEvent event) { FlightLogger.changeScenes("LogIn"); }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Initialize the fields
        usernameField = new Field("Username", usernameTextField, null, "name", 24, true);
        emailField = new Field("Email", emailTextField, null, "email", 48, true);
        passwordField = new PassField("Password", passwordPassField, null, 48);
        confirmField = new PassField("Password", confirmPassField, null, 48);

        // Initialize the lists of fields
        fields.addAll(Arrays.asList(usernameField, emailField));
        passFields.add(passwordField);

        // Add field length restrictions
        FieldFormatter.restrictFields(fields);
        FieldFormatter.restrictPasswords(passFields);

        // Change Button to disabled if username field is empty
        usernameTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(final ObservableValue<? extends String> ov, final String oldValue, final String newValue) {

                // Set Button Inactive if no value is present
                if (usernameTextField.getText().length() < 1) {
                    registerButton.getStyleClass().remove("button-primary");
                    registerButton.getStyleClass().add("button-disabled");
                } else {
                    registerButton.getStyleClass().remove("button-disabled");
                    registerButton.getStyleClass().add("button-primary");
                }
            }
        });
    }
}
