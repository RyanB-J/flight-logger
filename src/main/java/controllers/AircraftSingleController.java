//*******************************************************************
// AircraftSingleController
//
// Controls the interactions for AircraftSingle.fxml.
// This is used for adding, editing, and viewing aircraft properties.
//*******************************************************************

package main.java.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import main.java.FlightLogger;
import main.java.middleware.FieldFormatter;
import main.java.middleware.FieldValidator;
import main.java.models.Aircraft;
import main.java.models.Field;
import main.java.util.Helper;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

public class AircraftSingleController implements Initializable {

    // Components
    @FXML private Label titleLabel;
    @FXML private TextField manufacturerTextField;
    @FXML private TextField icaoCodeTextField;
    @FXML private TextField modelTextField;
    @FXML private TextField engineNumberTextField;
    @FXML private ComboBox<String> engineClassComboBox;
    @FXML private TextField wingspanTextField;
    @FXML private TextField lengthTextField;
    @FXML private TextField tailHeightTextField;
    @FXML private TextField wheelBaseTextField;
    @FXML private ComboBox<String> wakeCategoryComboBox;
    @FXML private Button addSaveButton;
    @FXML private Button cancelButton;

    // State Variables
    public enum Mode {ADD, EDIT, VIEW}
    private static Mode sceneMode = Mode.VIEW;

    // Create objects from the text fields
    private Field manufacturerField, icaoField, modelField, engineNumberField, wingspanField, lengthField,
            tailHeightField, wheelBaseField;
    private final ArrayList<Field> fields = new ArrayList<>();

    // Getters & Setters
    public static Mode getSceneMode() {
        return sceneMode;
    }
    public static void setSceneMode(Mode sceneMode) {
        AircraftSingleController.sceneMode = sceneMode;
    }

    // Set the values of the fields
    private void setFieldValues() {
        Aircraft aircraft = AircraftController.getSelectedAircraft();
        manufacturerTextField.setText(Helper.toTitleCase(aircraft.getMake()));
        icaoCodeTextField.setText(aircraft.getIcaoCode() != null ? aircraft.getIcaoCode() : "");
        modelTextField.setText(Helper.toTitleCase(aircraft.getModel()));
        engineNumberTextField.setText(Integer.toString(aircraft.getEngineNumber()));
        engineClassComboBox.setValue(Helper.toTitleCase(aircraft.getEngineClass()));
        wingspanTextField.setText(Double.toString(aircraft.getWingspan()));
        lengthTextField.setText(Double.toString(aircraft.getLength()));
        tailHeightTextField.setText(Double.toString(aircraft.getTailHeight()));
        wheelBaseTextField.setText(Double.toString(aircraft.getWheelbase()));
        wakeCategoryComboBox.setValue(aircraft.getWakeCategory());
    }

    // Disable fields for Read-Only
    private void disableFields() {
        for (TextField textField : Arrays.asList(manufacturerTextField, icaoCodeTextField, modelTextField,
                engineNumberTextField, wingspanTextField, lengthTextField, tailHeightTextField, wheelBaseTextField)) {
            textField.setDisable(true);
        }
        engineClassComboBox.setDisable(true);
        wakeCategoryComboBox.setDisable(true);
    }

    // Cancel the Form
    @FXML private void cancelForm(ActionEvent event) {
        FlightLogger.getModalStage().close();
    }

    // Save the Form
    @FXML private void addSaveAircraft(ActionEvent event) {

        // Control the addSave button depending on the sceneMode
        switch (sceneMode) {
            case ADD:
                if (FieldValidator.validate(fields)) {
                    if (Aircraft.createAircraft(
                            manufacturerField.getValue(),
                            modelField.getValue(),
                            icaoField.getValue(),
                            engineNumberField.getValue(),
                            engineClassComboBox.getValue(),
                            wingspanField.getValue(),
                            lengthField.getValue(),
                            tailHeightField.getValue(),
                            wheelBaseField.getValue(),
                            wakeCategoryComboBox.getValue()
                    )) {
                        // Close the window
                        AircraftController.setInitializeAlert("Aircraft Created.");
                        FlightLogger.getModalStage().close();
                        AircraftController.refreshScreen();
                    }
                }
                break;
            case EDIT:
                if (FieldValidator.validate(fields)) {
                    if (Aircraft.updateAircraft(
                            AircraftController.getSelectedAircraft().getId(),
                            manufacturerField.getValue(),
                            modelField.getValue(),
                            icaoField.getValue(),
                            engineNumberField.getValue(),
                            engineClassComboBox.getValue(),
                            wingspanField.getValue(),
                            lengthField.getValue(),
                            tailHeightField.getValue(),
                            wheelBaseField.getValue(),
                            wakeCategoryComboBox.getValue()
                    )) {
                        // Close the window
                        AircraftController.setInitializeAlert("Aircraft Updated.");
                        FlightLogger.getModalStage().close();
                        AircraftController.refreshScreen();
                    }
                }
                break;
            case VIEW:
                FlightLogger.getModalStage().close();
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Initialize the fields
        manufacturerField = new Field("Manufacturer", manufacturerTextField, null, "general", 48, true);
        icaoField = new Field("IACO Code", icaoCodeTextField, null, "character", 4, false);
        modelField = new Field("Model", modelTextField, null, "general", 48, true);
        engineNumberField = new Field("Engine Number", engineNumberTextField, null, "integer", 1, true);
        wingspanField = new Field("Wingspan", wingspanTextField, null, "decimal", 8, false);
        lengthField = new Field("Length", lengthTextField, null, "decimal", 8, false);
        tailHeightField = new Field("Tail Height", tailHeightTextField, null, "decimal", 8, false);
        wheelBaseField = new Field("Wheel Base", wheelBaseTextField, null, "decimal", 8, false);

        // Initialize the list of fields
        fields.addAll(Arrays.asList(manufacturerField, icaoField, modelField, engineNumberField,
                wingspanField, lengthField, tailHeightField, wheelBaseField));

        // Add field restrictions
        FieldFormatter.restrictFields(fields);

        // Configure Combo Boxes
        engineClassComboBox.setItems(Aircraft.engineClasses);
        wakeCategoryComboBox.setItems(Aircraft.wakeCategories);

        // Configure form depending on sceneMode
        switch (sceneMode) {
            case ADD:
                titleLabel.setText("Add Aircraft");
                engineClassComboBox.setValue(engineClassComboBox.getItems().get(1));
                wakeCategoryComboBox.setValue(wakeCategoryComboBox.getItems().get(1));
                addSaveButton.setText("Add");
                break;
            case EDIT:
                titleLabel.setText(AircraftController.getSelectedAircraft().getMake() + " " +AircraftController.getSelectedAircraft().getModel());
                setFieldValues();
                addSaveButton.setText("Save");
                break;
            case VIEW:
                titleLabel.setText(AircraftController.getSelectedAircraft().getMake() + " " +AircraftController.getSelectedAircraft().getModel());
                //titleLabel.setText("View Aircraft");
                setFieldValues();
                disableFields();
                addSaveButton.setText("Done");
                cancelButton.setVisible(false);
        }
    }
}
