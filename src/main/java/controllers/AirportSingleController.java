//*******************************************************************
// AirportSingleController
//
// Controls the interactions for AirportSingle.fxml.
// This is used for adding, editing, and viewing airport properties as well as selecting runways for inspection and
// manipulation.
//*******************************************************************

package main.java.controllers;

import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import main.java.FlightLogger;
import main.java.middleware.FieldFormatter;
import main.java.middleware.FieldValidator;
import main.java.models.Airport;
import main.java.models.Field;
import main.java.models.Runway;
import main.java.util.Helper;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

public class AirportSingleController implements Initializable {

    // Components
    @FXML private Label titleLabel;
    @FXML private TextField nameTextField;
    @FXML private TextField locIdTextField;
    @FXML private ComboBox<String> airportTypeComboBox;
    @FXML private TextField cityTextField;
    @FXML private ComboBox<String> stateComboBox;
    @FXML private TextField latitudeTextField;
    @FXML private TextField longitudeTextField;
    @FXML private TextField elevationTextField;
    @FXML private TextField ctafTextField;
    @FXML private ComboBox<String> unicomComboBox;
    @FXML private CheckBox towerCheckBox;
    @FXML private Button addViewRunwayButton;
    @FXML private Button editRunwayButton;
    @FXML private Button deleteRunwayButton;
    @FXML private Button cancelButton;
    @FXML private Button addSaveButton;

    // Runway Table
    @FXML private TableView<Runway> runwayTableView;
    @FXML private TableColumn<Runway, String> runwayTableColumn;
    @FXML private TableColumn<Runway, Integer> lengthTableColumn;
    @FXML private TableColumn<Runway, Integer> widthTableColumn;
    @FXML private TableColumn<Runway, String> surfaceTableColumn;
    @FXML private TableColumn<Runway, String> baseNumberTableColumn;
    @FXML private TableColumn<Runway, Integer> baseHeadingTableColumn;
    @FXML private TableColumn<Runway, Double> baseElevationTableColumn;
    @FXML private TableColumn<Runway, String> reciprocalNumberTableColumn;
    @FXML private TableColumn<Runway, Integer> reciprocalHeadingTableColumn;
    @FXML private TableColumn<Runway, Double> reciprocalElevationTableColumn;

    // State Variables
    public enum Mode {ADD, EDIT, VIEW}
    private static Mode sceneMode = Mode.VIEW;
    private static Runway selectedRunway;
    private static int selectedRunwayIndex;
    public static ObservableList<Runway> runwayTableItems = FXCollections.observableArrayList();
    public static boolean runwayTableItemsEdited = false;
    public static ArrayList<Integer> deletedRunways = new ArrayList<>();

    // Create objects from the text fields
    private Field nameField, locIdField, cityField, latitudeField, longitudeField, elevationField, ctafField;
    private final ArrayList<Field> fields = new ArrayList<>();

    // Getters & Setters
    public static Mode getSceneMode() {
        return sceneMode;
    }
    public static void setSceneMode(Mode sceneMode) {
        AirportSingleController.sceneMode = sceneMode;
    }
    public static Runway getSelectedRunway() {
        return selectedRunway;
    }
    public static int getSelectedRunwayIndex() {
        return selectedRunwayIndex;
    }
    public static void setSelectedRunwayIndex(int selectedRunwayIndex) {
        AirportSingleController.selectedRunwayIndex = selectedRunwayIndex;
    }

    // Set the values of the fields
    private void setFieldValues() {
        Airport airport = AirportsController.getSelectedAirport();
        nameTextField.setText(Helper.toTitleCase(airport.getName()));
        locIdTextField.setText(airport.getLocId());
        airportTypeComboBox.setValue(Helper.toTitleCase(airport.getType()));
        cityTextField.setText(Helper.toTitleCase(airport.getCity()));
        stateComboBox.setValue(Helper.toTitleCase(airport.getState()));
        latitudeTextField.setText(airport.getLatitude());
        longitudeTextField.setText(airport.getLongitude());
        elevationTextField.setText(Double.toString(airport.getElevation()));
        ctafTextField.setText((airport.getCtaf() != null ? airport.getCtaf() : ""));
        unicomComboBox.setValue(airport.getUnicom());
        towerCheckBox.setSelected(airport.isTower());
        runwayTableItems = Runway.getRunways(AirportsController.getSelectedAirport().getRelationalId());
        runwayTableView.setItems(runwayTableItems);
    }

    // Disable fields for Read-Only
    private void disableFields() {
        for (TextField textField : Arrays.asList(nameTextField, locIdTextField, cityTextField, latitudeTextField,
                longitudeTextField, elevationTextField, ctafTextField)) {
            textField.setDisable(true);
        }
        for (ComboBox<String> comboBox : Arrays.asList(airportTypeComboBox, stateComboBox, unicomComboBox)) {
            comboBox.setDisable(true);
        }
        towerCheckBox.setDisable(true);
    }

    // Navigation Controls
    @FXML private void goToFlightLogs(ActionEvent event) { FlightLogger.changeScenes("FlightLogs"); }
    @FXML private void goToReports(ActionEvent event) { FlightLogger.changeScenes("Reports"); }
    @FXML private void goToAirports(ActionEvent event) { FlightLogger.changeScenes("Airports"); }
    @FXML private void goToAircraft(ActionEvent event) { FlightLogger.changeScenes("Aircraft"); }
    @FXML private void goToAccount(ActionEvent event) { FlightLogger.changeScenes("Account"); }
    @FXML private void logOut(ActionEvent event) { FlightLogger.changeScenes("LogIn"); }

    // Add a Runway
    @FXML private void addViewRunway(ActionEvent event) {
        if (sceneMode == Mode.VIEW) {
            RunwayController.setSceneMode(RunwayController.Mode.VIEW);
        } else {
            RunwayController.setSceneMode(RunwayController.Mode.ADD);
        }

        try {
            FlightLogger.addSceneHandler("Runway", "Runway");
        } catch (IOException e) {
            System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
        }
    }

    // Edit the selected Runway
    @FXML private void editRunway(ActionEvent event) {
        RunwayController.setSceneMode(RunwayController.Mode.EDIT);
        try {
            FlightLogger.addSceneHandler("Runway", "Runway");
        } catch (IOException e) {
            System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
        }
    }

    // Delete the selected Runway
    @FXML private void deleteRunway(ActionEvent event) {
        deletedRunways.add(selectedRunway.getId());
        runwayTableItems.remove(selectedRunwayIndex);
    }

    @FXML private void cancelForm(ActionEvent event) {
        runwayTableItems.removeAll();
        runwayTableView.setItems(runwayTableItems);
        FlightLogger.changeScenes("Airports");
    }

    @FXML private void addSaveAirport(ActionEvent event) {

        // Control the addSave button depending on the sceneMode
        switch (sceneMode) {
            case ADD:
                if (FieldValidator.validate(fields)) {
                    String relationalID = "CUST*" + (Airport.getLastRelationalId() + 1);

                    // Create an airport from field values
                    if (Airport.createAirport(
                            relationalID,
                            nameField.getValue(),
                            locIdField.getValue(),
                            airportTypeComboBox.getValue(),
                            cityField.getValue(),
                            stateComboBox.getValue(),
                            latitudeField.getValue(),
                            longitudeField.getValue(),
                            elevationField.getValue(),
                            ctafField.getValue(),
                            unicomComboBox.getValue(),
                            towerCheckBox.isSelected()
                    )) {
                        // Loop through the list of runways and add them to the database
                        if (!runwayTableItems.isEmpty()) {
                            System.out.println(Helper.changeColor("\tCreating Runway(s)...", Helper.Color.YELLOW));
                            for (Runway runway : runwayTableItems) {
                                Runway.createRunway(
                                        relationalID,
                                        runway.getName(),
                                        runway.getLength(),
                                        runway.getWidth(),
                                        runway.getSurface(),
                                        runway.getBaseNumber(),
                                        runway.getBaseHeading(),
                                        runway.getBaseElevation(),
                                        runway.getReciprocalNumber(),
                                        runway.getReciprocalHeading(),
                                        runway.getReciprocalElevation()
                                );
                            }
                        }
                        System.out.println(Helper.changeColor("Airport Created", Helper.Color.GREEN) + "\n");

                        // Go to Airports
                        AirportsController.setInitializeAlert("Airport Created.");
                        FlightLogger.changeScenes("Airports");
                    }
                }
                break;
            case EDIT:
                if (FieldValidator.validate(fields)) {
                    String relationalID = AirportsController.getSelectedAirport().getRelationalId();

                    // Update the airport from the field values
                    if (Airport.updateAirport(
                            AirportsController.getSelectedAirport().getId(),
                            relationalID,
                            nameField.getValue(),
                            locIdField.getValue(),
                            airportTypeComboBox.getValue(),
                            cityField.getValue(),
                            stateComboBox.getValue(),
                            latitudeField.getValue(),
                            longitudeField.getValue(),
                            elevationField.getValue(),
                            ctafField.getValue(),
                            unicomComboBox.getValue(),
                            towerCheckBox.isSelected()
                    )) {
                        // Loop through the list of runways and update the associated runway(s) in the database
                        System.out.println(Helper.changeColor("\tUpdating Runway(s)...", Helper.Color.YELLOW));
                        for (Runway runway : runwayTableItems) {

                            // If the runway was an existing runway
                            if (runway.getId() != 0) {
                                Runway.updateRunway(
                                        runway.getId(),
                                        runway.getRelationalId(),
                                        runway.getName(),
                                        runway.getLength(),
                                        runway.getWidth(),
                                        runway.getSurface(),
                                        runway.getBaseNumber(),
                                        runway.getBaseHeading(),
                                        runway.getBaseElevation(),
                                        runway.getReciprocalNumber(),
                                        runway.getReciprocalHeading(),
                                        runway.getReciprocalElevation()
                                );
                            }

                            // If the runway is a new runway
                            else {
                                Runway.createRunway(
                                        relationalID,
                                        runway.getName(),
                                        runway.getLength(),
                                        runway.getWidth(),
                                        runway.getSurface(),
                                        runway.getBaseNumber(),
                                        runway.getBaseHeading(),
                                        runway.getBaseElevation(),
                                        runway.getReciprocalNumber(),
                                        runway.getReciprocalHeading(),
                                        runway.getReciprocalElevation()
                                );
                            }
                        }

                        // Remove runways from the database that were deleted
                        if (!deletedRunways.isEmpty()) {
                            for (Integer runwayID : deletedRunways) {
                                Runway.deleteRunway(runwayID);
                            }
                        }
                        System.out.println(Helper.changeColor("Airport Updated", Helper.Color.GREEN) + "\n");

                        // Go to Airports
                        AirportsController.setInitializeAlert("Airport Updated.");
                        FlightLogger.changeScenes("Airports");
                    }
                }
                break;
            case VIEW:

                // Go to Airports
                FlightLogger.changeScenes("Airports");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Initialize the fields
        nameField = new Field("Airport Name", nameTextField, null, "general", 48, true);
        locIdField = new Field("Local Identification", locIdTextField, null, "character", 4, true);
        cityField = new Field("City", cityTextField, null, "alphabetic", 48, true);
        latitudeField = new Field("Latitude", latitudeTextField, null, "latitude", 15, true);
        longitudeField = new Field("Longitude", longitudeTextField, null, "longitude", 15, true);
        elevationField = new Field("Elevation", elevationTextField, null, "decimal", 8, false);
        ctafField = new Field("CTAF Frequency", ctafTextField, null, "frequency", 7, false);

        // Initialize the list of fields
        fields.addAll(Arrays.asList(nameField, locIdField, cityField, latitudeField, longitudeField, elevationField, ctafField));

        // Add field restrictions and formatting
        FieldFormatter.restrictFields(fields);

        // Configure Combo Boxes
        airportTypeComboBox.setItems(Airport.airportTypes);
        stateComboBox.setItems(Airport.states);
        unicomComboBox.setItems(Airport.unicomFrequencies);

        // Configure the Runway table
        runwayTableColumn.setCellValueFactory(cellData -> Bindings.createStringBinding(
                () -> Helper.toTitleCase(cellData.getValue().getName())
        ));
        lengthTableColumn.setCellValueFactory(new PropertyValueFactory<Runway, Integer>("length"));
        widthTableColumn.setCellValueFactory(new PropertyValueFactory<Runway, Integer>("width"));
        surfaceTableColumn.setCellValueFactory(cellData -> Bindings.createStringBinding(
                () -> Helper.toTitleCase(cellData.getValue().getSurface())
        ));
        baseNumberTableColumn.setCellValueFactory(new PropertyValueFactory<Runway, String>("baseNumber"));
        baseHeadingTableColumn.setCellValueFactory(new PropertyValueFactory<Runway, Integer>("baseHeading"));
        baseElevationTableColumn.setCellValueFactory(new PropertyValueFactory<Runway, Double>("baseElevation"));
        reciprocalNumberTableColumn.setCellValueFactory(new PropertyValueFactory<Runway, String>("reciprocalNumber"));
        reciprocalHeadingTableColumn.setCellValueFactory(new PropertyValueFactory<Runway, Integer>("reciprocalHeading"));
        reciprocalElevationTableColumn.setCellValueFactory(new PropertyValueFactory<Runway, Double>("reciprocalElevation"));

        // Configure form depending on sceneMode
        switch (sceneMode) {
            case ADD:
                titleLabel.setText("Add Airport");
                if (!runwayTableItemsEdited) {
                    runwayTableItems.removeAll();
                }
                runwayTableView.setItems(runwayTableItems);
                airportTypeComboBox.setValue(airportTypeComboBox.getItems().get(1));
                stateComboBox.setValue(stateComboBox.getItems().get(0));
                unicomComboBox.setValue(unicomComboBox.getItems().get(1));
                editRunwayButton.setDisable(true);
                deleteRunwayButton.setDisable(true);
                addSaveButton.setText("Add Airport");
                break;
            case EDIT:
                titleLabel.setText(AirportsController.getSelectedAirport().getLocId() + " - " +
                        Helper.toTitleCase(AirportsController.getSelectedAirport().getName()));
                setFieldValues();
                editRunwayButton.setDisable(true);
                deleteRunwayButton.setDisable(true);
                addSaveButton.setText("Save Airport");
                break;
            case VIEW:
                titleLabel.setText(AirportsController.getSelectedAirport().getLocId() + " - " +
                        Helper.toTitleCase(AirportsController.getSelectedAirport().getName()));
                setFieldValues();
                disableFields();
                addViewRunwayButton.setText("View");
                addViewRunwayButton.setDisable(true);
                editRunwayButton.setVisible(false);
                deleteRunwayButton.setVisible(false);
                cancelButton.setVisible(false);
                addSaveButton.setText("Done");
                addSaveButton.getStyleClass().add("button-block");
        }

        // Runway Table Selection Listener
        runwayTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Runway>() {
            @Override
            public void changed(ObservableValue<? extends Runway> observable, Runway oldValue, Runway newValue) {
                if (runwayTableView.getSelectionModel().getSelectedItem() != null) {
                    selectedRunway = newValue;
                    selectedRunwayIndex = runwayTableView.getSelectionModel().getSelectedIndex();
                    if (sceneMode == Mode.ADD || sceneMode == Mode.EDIT) {
                        editRunwayButton.setDisable(false);
                        deleteRunwayButton.setDisable(false);
                    } else {
                        addViewRunwayButton.setDisable(false);
                    }
                }
            }
        });

    }

}
