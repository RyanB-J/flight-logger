//*******************************************************************
// RunwayController
//
// Controls the interactions for Runway.fxml.
// This is used for adding, editing, and viewing runway properties.
//*******************************************************************

package main.java.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import main.java.FlightLogger;
import main.java.middleware.FieldFormatter;
import main.java.middleware.FieldValidator;
import main.java.models.Field;
import main.java.models.Runway;
import main.java.util.Helper;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

public class RunwayController implements Initializable {

    // Components
    @FXML private Label titleLabel;
    @FXML private TextField nameTextField;
    @FXML private TextField lengthTextField;
    @FXML private TextField widthTextField;
    @FXML private ComboBox<String> surfaceComboBox;
    @FXML private TextField baseNumberTextField;
    @FXML private TextField baseHeadingTextField;
    @FXML private TextField baseElevationTextField;
    @FXML private TextField reciprocalNumberTextField;
    @FXML private TextField reciprocalHeadingTextField;
    @FXML private TextField reciprocalElevationTextField;
    @FXML private Button cancelButton;
    @FXML private Button addSaveButton;

    // State Variables
    public enum Mode {ADD, EDIT, VIEW}
    private static Mode sceneMode = Mode.VIEW;

    // Create objects from the text fields
    private Field nameField, lengthField, widthField, baseNumField, baseHeadField, baseElevField, recipNumField,
            recipHeadField, recipElevField;
    private final ArrayList<Field> fields = new ArrayList<>();

    // Getters & Setters
    public static Mode getSceneMode() {
        return sceneMode;
    }
    public static void setSceneMode(Mode sceneMode) {
        RunwayController.sceneMode = sceneMode;
    }

    // Set the values of the fields
    private void setFieldValues() {
        Runway runway = AirportSingleController.getSelectedRunway();
        nameTextField.setText(Helper.toTitleCase(runway.getName()));
        lengthTextField.setText(Integer.toString(runway.getLength()));
        widthTextField.setText(Integer.toString(runway.getWidth()));
        surfaceComboBox.setValue(runway.getSurface());
        baseNumberTextField.setText((runway.getBaseNumber() != null ? runway.getBaseNumber() : ""));
        baseHeadingTextField.setText(Integer.toString(runway.getBaseHeading()));
        baseElevationTextField.setText(Double.toString(runway.getBaseElevation()));
        reciprocalNumberTextField.setText((runway.getReciprocalNumber() != null ? runway.getReciprocalNumber() : ""));
        reciprocalHeadingTextField.setText(Integer.toString(runway.getReciprocalHeading()));
        reciprocalElevationTextField.setText(Double.toString(runway.getReciprocalElevation()));
    }

    // Disable the fields for Read-Only
    private void disableFields() {
        for (TextField textField : Arrays.asList(nameTextField, lengthTextField, widthTextField, baseNumberTextField,
                baseHeadingTextField, baseElevationTextField, reciprocalNumberTextField, reciprocalHeadingTextField,
                reciprocalElevationTextField)) {
            textField.setDisable(true);
        }
        surfaceComboBox.setDisable(true);
    }

    @FXML private void cancelForm(ActionEvent event) {
        FlightLogger.getModalStage().close();
    }

    @FXML private void addSaveRunway(ActionEvent event) {

        // Control the addSave button depending on the sceneMode
        switch (sceneMode) {
            case ADD:
                if (FieldValidator.validate(fields)) {
                    AirportSingleController.runwayTableItems.add(new Runway(
                            0,
                            "",
                            nameField.getValue(),
                            (!lengthField.getValue().equals("") ? Integer.parseInt(lengthField.getValue()) : 0),
                            (!widthField.getValue().equals("") ? Integer.parseInt(widthField.getValue()) : 0),
                            surfaceComboBox.getValue(),
                            baseNumField.getValue(),
                            (!baseHeadField.getValue().equals("") ? Integer.parseInt(baseHeadField.getValue()) : 0),
                            (!baseElevField.getValue().equals("") ? Double.parseDouble(baseElevField.getValue()) : 0.0),
                            recipNumField.getValue(),
                            (!recipHeadField.getValue().equals("") ? Integer.parseInt(recipHeadField.getValue()) : 0),
                            (!recipElevField.getValue().equals("") ? Double.parseDouble(recipElevField.getValue()) : 0.0)
                    ));
                    AirportSingleController.runwayTableItemsEdited = true;
                    FlightLogger.getModalStage().close();
                }
                break;
            case EDIT:
                if (FieldValidator.validate(fields)) {
                    AirportSingleController.runwayTableItems.set(AirportSingleController.getSelectedRunwayIndex(), new Runway(
                            AirportSingleController.getSelectedRunway().getId(),
                            AirportSingleController.getSelectedRunway().getRelationalId(),
                            nameField.getValue(),
                            (!lengthField.getValue().equals("") ? Integer.parseInt(lengthField.getValue()) : 0),
                            (!widthField.getValue().equals("") ? Integer.parseInt(widthField.getValue()) : 0),
                            surfaceComboBox.getValue(),
                            baseNumField.getValue(),
                            (!baseHeadField.getValue().equals("") ? Integer.parseInt(baseHeadField.getValue()) : 0),
                            (!baseElevField.getValue().equals("") ? Double.parseDouble(baseElevField.getValue()) : 0.0),
                            recipNumField.getValue(),
                            (!recipHeadField.getValue().equals("") ? Integer.parseInt(recipHeadField.getValue()) : 0),
                            (!recipElevField.getValue().equals("") ? Double.parseDouble(recipElevField.getValue()) : 0.0)
                    ));
                    AirportSingleController.runwayTableItemsEdited = true;
                    FlightLogger.getModalStage().close();
                }
                break;
            case VIEW:
                FlightLogger.getModalStage().close();
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Initialize the fields
        nameField = new Field("Runway Name", nameTextField, null, "general", 48, true);
        lengthField = new Field("Length", lengthTextField, null, "integer", 8, false);
        widthField = new Field("Width", widthTextField, null, "integer", 8, false);
        baseNumField = new Field("Base Number", baseNumberTextField, null, "character", 3, false);
        baseHeadField = new Field("Base Heading", baseHeadingTextField, null, "integer", 3, false);
        baseElevField = new Field("Base Elevation", baseElevationTextField, null, "decimal", 8, false);
        recipNumField = new Field("Base Number", reciprocalNumberTextField, null, "character", 3, false);
        recipHeadField = new Field("Base Heading", reciprocalHeadingTextField, null, "integer", 3, false);
        recipElevField = new Field("Base Elevation", reciprocalElevationTextField, null, "decimal", 8, false);

        // Initialize the list of fields
        fields.addAll(Arrays.asList(nameField, lengthField, widthField, baseNumField, baseHeadField,
                baseElevField, recipNumField, recipHeadField, recipElevField));

        // Add field restrictions
        FieldFormatter.restrictFields(fields);

        // Configure Combo Box
        surfaceComboBox.setItems(Runway.surfaces);

        // Configure form depending on sceneMode
        switch (sceneMode) {
            case ADD:
                titleLabel.setText("Add Runway");
                surfaceComboBox.setValue(surfaceComboBox.getItems().get(1));
                addSaveButton.setText("Add");
                break;
            case EDIT:
                titleLabel.setText(AirportSingleController.getSelectedRunway().getName());
                setFieldValues();
                addSaveButton.setText("Save");
                break;
            case VIEW:
                titleLabel.setText(AirportSingleController.getSelectedRunway().getName());
                setFieldValues();
                disableFields();
                addSaveButton.setText("Done");
                cancelButton.setVisible(false);
        }
    }
}
