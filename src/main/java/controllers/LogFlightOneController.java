//*******************************************************************
// LogFlightOneController
//
// Controls the interactions for LogFlight-1.fxml.
// This is the first screen for creating or editing a flight log.
//*******************************************************************

package main.java.controllers;

import impl.org.controlsfx.autocompletion.SuggestionProvider;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import main.java.FlightLogger;
import main.java.middleware.FieldFormatter;
import main.java.middleware.FieldValidator;
import main.java.models.*;
import org.controlsfx.control.textfield.TextFields;
import main.java.util.AppAlerts;
import main.java.util.Helper;

import java.net.URL;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

public class LogFlightOneController implements Initializable {

    // Components
    @FXML private DatePicker dateDatePicker;
    @FXML private TextField flightNumberTextField;
    @FXML private TextField aircraftIdentTextField;
    @FXML private TextField departureTextField;
    @FXML private ComboBox<String> departureRunwayComboBox;
    @FXML private TextField departureTimeTextField;
    @FXML private TextField arrivalTextField;
    @FXML private ComboBox<String> arrivalRunwayComboBox;
    @FXML private TextField arrivalTimeTextField;
    @FXML private TextField makeTextField;
    @FXML private ComboBox<String> modelComboBox;
    @FXML private TextField engineNumberTextField;
    @FXML private ComboBox<String> engineClassComboBox;
    @FXML private TextField icaoTextField;

    // State Variables
    private static final ObservableList<String>
            departureAirportList = FXCollections.observableArrayList(),
            arrivalAirportList = FXCollections.observableArrayList(),
            departureRunwayList = FXCollections.observableArrayList(),
            arrivalRunwayList = FXCollections.observableArrayList(),
            aircraftList = FXCollections.observableArrayList(),
            aircraftModelList = FXCollections.observableArrayList();

    private static final ArrayList<Integer>
            departureAirportIDList = new ArrayList<>(),
            arrivalAirportIDList = new ArrayList<>(),
            aircraftModelIDList = new ArrayList<>();

    private final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("hh:mm a");

    // List of fields for main.java.middleware
    private Field flightNumberField, aircraftIdentField, departureField, departureTimeField,
            arrivalField, arrivalTimeField, makeField, engineNumberField, icaoField;
    private final ArrayList<Field> fields = new ArrayList<>();
    private final ArrayList<ComboBox<String>> comboBoxes = new ArrayList<>();
    private final ArrayList<DatePicker> datePickers = new ArrayList<>();

    // Helper method to build the airport autocompletion text fields
    private static void buildAutocompleteTextFieldListener(TextField textField, ObservableList<String> objectList,
                                                           ArrayList<Integer> objectIDList, ObservableList<String> associatedList,
                                                           ComboBox<String> associatedComboBox, SuggestionProvider<String> provider,
                                                           boolean isAirport) {

        textField.textProperty().addListener((ov, oldValue, newValue) -> {

            // If new value is a selection
            for (String objectName : objectList) {
                if (newValue.equals(objectName)) {

                    if (isAirport) {

                        // Get the correct airport from the database
                        int objectID = objectIDList.get(objectList.indexOf(objectName));
                        Airport airport = Airport.getSingleAirport(objectID);

                        // Get the associated runways for that airport
                        associatedList.clear();
                        for (Runway runway : Runway.getRunways(airport.getRelationalId())) {
                            associatedList.add(runway.getName());
                        }
                    } else {

                        // Get the associated main.java.models for that aircraft
                        associatedList.clear();
                        objectIDList.clear();
                        for (Aircraft aircraft : Aircraft.getModels(newValue)) {
                            associatedList.add(Helper.toTitleCase(aircraft.getModel()));
                            objectIDList.add(aircraft.getId());
                        }
                    }
                    if (!associatedList.isEmpty()) {
                        associatedComboBox.setValue(associatedComboBox.getItems().get(0));
                    }
                }
            }

            // Clear lists
            objectList.clear();
            if (isAirport) objectIDList.clear();
            provider.clearSuggestions();

            // Populate the airport list from a search query
            if (newValue.length() != 0) {
                if (isAirport) {
                    for (Airport airport : Airport.getAirports(newValue, 10, false)) {
                        objectList.add(airport.getLocId() + " - " + Helper.toTitleCase(airport.getName()));
                        objectIDList.add(airport.getId());
                    }
                } else {
                    for (String make : Aircraft.getMakes(newValue, 10)) {
                        objectList.add(Helper.toTitleCase(make));
                    }
                }
                provider.addPossibleSuggestions(objectList);
            }
        });
    }

    private void populateFields() {
        FlightLog log = FlightLog.getCurrentFlightLog();
        if (log.getDate() != null) dateDatePicker.setValue(log.getDate());
        if (log.getFlightNumber() != 0) flightNumberTextField.setText(Integer.toString(log.getFlightNumber()));
        if (log.getAircraftIdent() != null) aircraftIdentTextField.setText(log.getAircraftIdent());
        if (log.getDepartureAirport() != null) departureTextField.setText(log.getDepartureAirport());
        if (log.getDepartureRunway() != null) departureRunwayComboBox.setValue(log.getDepartureRunway());
        if (log.getDepartureTime() != null) departureTimeTextField.setText(log.getDepartureTime().format(timeFormatter));
        if (log.getArrivalAirport() != null) arrivalTextField.setText(log.getArrivalAirport());
        if (log.getArrivalRunway() != null) arrivalRunwayComboBox.setValue(log.getArrivalRunway());
        if (log.getArrivalTime() != null) arrivalTimeTextField.setText(log.getArrivalTime().format(timeFormatter));
        if (log.getAircraftMake() != null) makeTextField.setText(log.getAircraftMake());
        if (log.getAircraftModel() != null) modelComboBox.setValue(log.getAircraftModel());
        if (log.getAircraftEngineNumber() != 0) engineNumberTextField.setText(Integer.toString(log.getAircraftEngineNumber()));
        if (log.getAircraftEngineClass() != null) engineClassComboBox.setValue(log.getAircraftEngineClass());
        if (log.getAircraftICAOCode() != null) icaoTextField.setText(log.getAircraftICAOCode());
    }

    private boolean validateInput() {

        boolean validated = true;

        // Update objects from the text fields
        for (Field field : fields) {
            field.setValue(field.getTextField().getText());
        }

        StringBuilder errorMessage = new StringBuilder();

        // If text fields are validated
        errorMessage.append(FieldValidator.validateText(fields));

        // If combo boxes are validated
        errorMessage.append(FieldValidator.requireComboBoxes(comboBoxes));

        // If date pickers are validated
        errorMessage.append(FieldValidator.requireDates(datePickers));

        // If the errorMessage is not empty
        if (!errorMessage.toString().isEmpty()) {
            validated = false;
            AppAlerts.throwFieldValidationAlert(errorMessage.toString());
        }

        return validated;
    }

    // Navigation Controls
    @FXML private void goToFlightLogs(ActionEvent event) {
        if (FlightLog.getCurrentFlightLog().getId() != 0) {
            FlightLogger.changeScenesWithSave("FlightLogs");
        } else {
            FlightLogger.changeScenesWithConfirmation("FlightLogs", null);
        }
    }
    @FXML private void goToReports(ActionEvent event) {
        if (FlightLog.getCurrentFlightLog().getId() != 0) {
            FlightLogger.changeScenesWithSave("Reports");
        } else {
            FlightLogger.changeScenesWithConfirmation("Reports", null);
        }
    }
    @FXML private void goToAirports(ActionEvent event) {
        if (FlightLog.getCurrentFlightLog().getId() != 0) {
            FlightLogger.changeScenesWithSave("Airports");
        } else {
            FlightLogger.changeScenesWithConfirmation("Airports", null);
        }
    }
    @FXML private void goToAircraft(ActionEvent event) {
        if (FlightLog.getCurrentFlightLog().getId() != 0) {
            FlightLogger.changeScenesWithSave("Aircraft");
        } else {
            FlightLogger.changeScenesWithConfirmation("Aircraft", null);
        }
    }
    @FXML private void goToAccount(ActionEvent event) {
        if (FlightLog.getCurrentFlightLog().getId() != 0) {
            FlightLogger.changeScenesWithSave("Account");
        } else {
            FlightLogger.changeScenesWithConfirmation("Account", null);
        }
    }
    @FXML private void logOut(ActionEvent event) { FlightLogger.changeScenesWithConfirmation("LogIn", null); }

    // Cancel the new Flight Log
    @FXML private void cancelForm(ActionEvent event) { FlightLogger.changeScenesWithConfirmation("FlightLogs", null); }

    // Save the data and go to the next scene
    @FXML private void goToNextScene(ActionEvent event) {
        if (validateInput()) {
            FlightLog flightLog = FlightLog.getCurrentFlightLog();
            flightLog.setDate(dateDatePicker.getValue());
            flightLog.setFlightNumber(!flightNumberField.getValue().equals("") ? Integer.parseInt(flightNumberField.getValue()) : 0);
            flightLog.setAircraftIdent(aircraftIdentField.getValue().toUpperCase());
            flightLog.setDepartureAirport(departureField.getValue());
            flightLog.setDepartureRunway(departureRunwayComboBox.getValue() != null ? departureRunwayComboBox.getValue() : "");
            flightLog.setDepartureTime(LocalTime.parse(departureTimeField.getValue(), timeFormatter));
            flightLog.setArrivalAirport(arrivalField.getValue());
            flightLog.setArrivalRunway(arrivalRunwayComboBox.getValue() != null ? arrivalRunwayComboBox.getValue() : "");
            flightLog.setArrivalTime(LocalTime.parse(arrivalTimeField.getValue(), timeFormatter));
            flightLog.setAircraftMake(makeField.getValue());
            flightLog.setAircraftModel(modelComboBox.getValue());
            flightLog.setAircraftEngineNumber(Integer.parseInt(engineNumberField.getValue()));
            flightLog.setAircraftEngineClass(engineClassComboBox.getValue());
            flightLog.setAircraftICAOCode(icaoField.getValue());
            FlightLogger.changeScenes("LogFlight-2");
        }
    }

    // When aircraft model is changed
    @FXML private void modelChanged(ActionEvent event) {
        if (modelComboBox.getValue() != null) {

            // Get aircraft data for selected model
            int aircraftID = aircraftModelIDList.get(aircraftModelList.indexOf(modelComboBox.getValue()));
            Aircraft aircraft = Aircraft.getSingleAircraft(aircraftID);

            // Set aircraft data to respective fields
            engineNumberTextField.setText(Integer.toString(aircraft.getEngineNumber()));
            engineClassComboBox.setValue(Helper.toTitleCase(aircraft.getEngineClass()));
            icaoTextField.setText(aircraft.getIcaoCode());
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Initialize a new flight log
        if (FlightLog.getCurrentFlightLog() == null) {
            FlightLog.setCurrentFlightLog(new FlightLog());
        }

        // Initialize the fields
        populateFields();
        flightNumberField = new Field("Flight Number", flightNumberTextField, null, "integer", 4, false);
        aircraftIdentField = new Field("Aircraft Identification", aircraftIdentTextField, null, "character", 5, false);
        departureField = new Field("Departure Airport", departureTextField, null, "airport", 48, true);
        departureTimeField = new Field("Departure Time", departureTimeTextField, null, "time", 8, true);
        arrivalField = new Field("Arrival Airport", arrivalTextField, null, "airport", 48, true);
        arrivalTimeField = new Field("Arrival Time", arrivalTimeTextField, null, "time", 8, true);
        makeField = new Field("Aircraft Manufacturer", makeTextField, null, "alpha", 48, true);
        engineNumberField = new Field("Engine Number", engineNumberTextField, null, "integer", 1, false);
        icaoField = new Field("ICAO Code", icaoTextField, null, "character", 4, false);

        // Initialize the lists of controls for validation
        fields.addAll(Arrays.asList(flightNumberField, aircraftIdentField, departureField, departureTimeField,
                arrivalField, arrivalTimeField, makeField, engineNumberField, icaoField));
        comboBoxes.add(modelComboBox);
        datePickers.add(dateDatePicker);

        // Add field restrictions and formatting
        FieldFormatter.restrictFields(fields);

        // Configure Combo Boxes
        departureRunwayComboBox.setItems(departureRunwayList);
        arrivalRunwayComboBox.setItems(arrivalRunwayList);
        modelComboBox.setItems(aircraftModelList);
        engineClassComboBox.setItems(Aircraft.engineClasses);

        // Initialize Auto-Completion Text Fields
        SuggestionProvider<String> departureAirportProvider = SuggestionProvider.create(departureAirportList);
        SuggestionProvider<String> arrivalAirportProvider = SuggestionProvider.create(arrivalAirportList);
        SuggestionProvider<String> aircraftProvider = SuggestionProvider.create(aircraftList);
        TextFields.bindAutoCompletion(departureTextField, departureAirportProvider);
        TextFields.bindAutoCompletion(arrivalTextField, arrivalAirportProvider);
        TextFields.bindAutoCompletion(makeTextField, aircraftProvider);

        // Departure Airport Text Field Listener
        buildAutocompleteTextFieldListener(departureTextField, departureAirportList, departureAirportIDList,
                departureRunwayList, departureRunwayComboBox, departureAirportProvider, true);

        // Arrival Airport Text Field Listener
        buildAutocompleteTextFieldListener(arrivalTextField, arrivalAirportList, arrivalAirportIDList,
                arrivalRunwayList, arrivalRunwayComboBox, arrivalAirportProvider, true);

        // Aircraft Text Field Listener
        buildAutocompleteTextFieldListener(makeTextField, aircraftList, aircraftModelIDList, aircraftModelList,
                modelComboBox, aircraftProvider, false);

    }
}
